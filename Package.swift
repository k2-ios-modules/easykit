// swift-tools-version: 5.7

import PackageDescription

let package = Package(
    name: "EasyKit",
    platforms: [
        .iOS(.v13)
    ],
    products: [
        .library(
            name: "EasyFoundation",
            targets: [
                "EasyFoundation"
            ]
        ),
        .library(
            name: "EasyUI",
            targets: [
                "EasyUI"
            ]
        ),
        .library(
            name: "EasyMVVM",
            targets: [
                "EasyMVVM"
            ]
        ),
    ],
    dependencies: [
        .package(url: "https://gitlab.com/k2-ios-modules/coordinators.git", exact: "1.0.11"),
        .package(url: "https://gitlab.com/k2-ios-modules/mvvm.git", exact: "1.0.3"),
        .package(url: "https://gitlab.com/k2-ios-modules/formvalidator.git", exact: "1.0.2")
    ],
    targets: [
        .target(
            name: "EasyFoundation",
            dependencies: []
        ),
        .target(
            name: "EasyUI",
            dependencies: [
                "EasyFoundation",
            ]
        ),
        .target(
            name: "EasyMVVM",
            dependencies: [
                "EasyUI",
                .product(name: "MVVM", package: "MVVM"),
                .product(name: "Coordinators", package: "Coordinators"),
                .product(name: "MVVMCoordinators", package: "Coordinators"),
                .product(name: "FormValidator", package: "FormValidator")
            ]
        )
    ]
)
