//
//  Created on 20.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import Coordinators
import EasyUI
import MVVMCoordinators
import UIKit

open class EasyPageViewController<
    VM: MVVMCoordinators.ControllerViewModelable
>: EasyUI.EasyPageViewController,
   EasyControllable {

    // MARK: - subtypes

    public typealias ViewModel = VM

    // MARK: - properties

    public private(set) var closeControllerSubject: CloseControllerSubject = .init()

    public var cancellables: Set<AnyCancellable> = []

    public let viewModel: ViewModel

    // MARK: - init

    required public convenience init(vm: ViewModel) {
        self.init(vm: vm, transitionStyle: .scroll, navigationOrientation: .horizontal)
    }

    public init(
        vm: ViewModel,
        transitionStyle style: UIPageViewController.TransitionStyle,
        navigationOrientation: UIPageViewController.NavigationOrientation
    ) {
        self.viewModel = vm
        super.init(
            transitionStyle: style,
            navigationOrientation: navigationOrientation
        )
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - life cycle

    open override func viewDidLoad() {
        super.viewDidLoad()

        self.setBinding()
    }

    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        self.sendCloseIfNeeded()
    }

    // MARK: - binding

    open func setBinding() {
        self.setBindingNavBarSideItems()
    }

}
