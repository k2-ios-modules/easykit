//
//  Created on 20.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//

import EasyUI
import MVVM
import MVVMCoordinators
import UIKit

public protocol EasyControllable: MVVMCoordinators.MVVMControllable
where Self: EasyUI.EasyViewController {

}

extension EasyControllable {
    @discardableResult
    public func makeNavBarButton(
        vm: BarButtonItem.ViewModel,
        to position: NavBarButtonPosition
    ) -> BarButtonItem {
        let barButtonItem = BarButtonItem.make()
        barButtonItem.setViewModel(vm)
        self.addNavBarButton(barButtonItem, to: position)
        return barButtonItem
    }

    public func setBindingNavBarSideItems() {
        self.viewModel.navBarLeftItems
            .sink { [weak self] items in
                self?.removeNavBarButtons(from: .left)
                items.forEach { vm in
                    self?.makeNavBarButton(vm: vm, to: .left)
                }
            }
            .store(in: &self.cancellables)

        self.viewModel.navBarRightItems
            .sink { [weak self] items in
                guard let self else { return }
                self.removeNavBarButtons(from: .right)
                items.forEach { vm in
                    self.makeNavBarButton(vm: vm, to: .right)
                }
            }
            .store(in: &self.cancellables)
    }
}
