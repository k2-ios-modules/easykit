//
//  Created on 09.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import EasyFoundation
import Foundation

public extension EasyFoundation.TapGestureRecognizerable {
    func sinkTapCompletion(
        _ publisher: Published<(() -> Void)?>.Publisher,
        checkUserInteraction: Bool = false
    ) -> AnyCancellable {
        publisher.sink { [weak self] completion in
            self?.setTapCompletion(completion, checkUserInteraction: checkUserInteraction)
        }
    }
}
