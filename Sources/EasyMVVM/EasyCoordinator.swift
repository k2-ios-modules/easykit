//
//  Created on 15.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Coordinators
import EasyUI
import MVVMCoordinators
import UIKit

open class EasyCoordinator<
    VC: EasyControllable
>: MVVMCoordinator<VC> {
    open override func preparePresentedVC(
        _ vc: UIViewController,
        config: TransitionType.Presenting
    ) {
        super.preparePresentedVC(vc, config: config)

        if config.transitioningDelegate is BottomSheetTransitioningDelegate {
            vc.view.layoutIfNeeded()
        }
    }
}

extension TransitionType.Presenting {
    public static func easyBottomSheet(
        _ config: BottomSheetConfig,
        withNavigation: Bool = false,
        animated: Bool = true
    ) -> Self {
        .init(
            modalPresentationStyle: .custom,
            transitioningDelegate: BottomSheetTransitioningDelegate(
                config: config,
                dismissCompletion: nil
            ),
            withNavigation: withNavigation,
            animated: animated
        )
    }
}
