//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import UIKit
import EasyFoundation
import EasyUI

open class EasyImageView: EasyUI.EasyImageView {

    // MARK: - properties

    private var animationCancellables: Set<AnyCancellable> = []

    // MARK: - setters

    public final func sinkImageForDisplay() -> AnyCancellable {
        self.publisher(for: \.image)
            .removeDuplicates()
            .sink { [weak self] image in
                self?.isHidden = image == nil
            }
    }

    open func setViewModel(_ vm: ViewModel) {
        self.cancellables.removeAll()
        vm.$image
            .sinkOnMain { [weak self] in self?.image = $0 }
            .store(in: &self.cancellables)
        vm.$size
            .sink { [weak self] in self?.imageSize = $0 }
            .store(in: &self.cancellables)
        Publishers.CombineLatest(
            vm.$applyTotalSize,
            vm.$totalSize
        )
        .sink { [weak self] (apply, totalSize) in
            self?.setSizeConstraints(apply ? totalSize: nil)
        }
        .store(in: &self.cancellables)
        vm.$paddingInsets
            .sink { [weak self] in self?.imagePaddingInsets = $0 }
            .store(in: &self.cancellables)
        vm.$animations
            .sink { [weak self] in self?.setAnimations($0) }
            .store(in: &self.cancellables)
        self.sinkTapCompletion(vm.$tapCompletion, checkUserInteraction: true)
            .store(in: &self.cancellables)
    }

    private func setAnimations(_ animations: [ViewModel.Animation]) {
        self.animationCancellables.removeAll()

        animations.forEach { animation in
            switch animation.type {
            case let .transform(config):
                animation.$toggle
                    .removeDuplicates()
                    .sink { [weak self] toggle in
                        guard let self else { return }

                        let duration = self.window == nil ? 0 : config.duration

                        UIView.animate(withDuration: duration) { [weak self] in
                            self?.transform = toggle ? config.toValue : config.fromValue
                        }
                    }
                    .store(in: &self.animationCancellables)
            }
        }
    }
}
