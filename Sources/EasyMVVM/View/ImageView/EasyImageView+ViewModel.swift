//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import UIKit
import MVVM

extension EasyImageView {
    open class ViewModel: MVVM.ViewModel {

        // MARK: - properties

        @Published
        public private(set) var image: UIImage?

        /// total size of image with image size and padding insets
        @Published
        public private(set) var totalSize: CGSize?

        @Published
        public var applyTotalSize: Bool = false

        @Published
        public private(set) var size: CGSize?

        @Published
        public private(set) var paddingInsets: UIEdgeInsets = .zero

        @Published
        public var animations: [Animation] = []

        @Published
        public var tapCompletion: (() -> Void)?

        // MARK: - init

        public init(
            image: UIImage? = nil,
            size: CGSize? = nil,
            paddingInsets: UIEdgeInsets = .zero
        ) {
            super.init()
            self.setImage(image, size: size, paddingInsets: paddingInsets)
        }

        // MARK: - setters

        public final func setImage(
            _ image: UIImage?
        ) {
            self.image = image
            self.setTotalSize()
        }

        public final func setImage(
            size: CGSize?,
            paddingInsets: UIEdgeInsets = .zero
        ) {
            self.size = size
            self.paddingInsets = paddingInsets
            self.setTotalSize()
        }

        public final func setImage(
            _ image: UIImage?,
            size: CGSize?,
            paddingInsets: UIEdgeInsets = .zero
        ) {
            self.image = image
            self.size = size
            self.paddingInsets = paddingInsets
            self.setTotalSize()
        }

        private func setTotalSize() {
            guard var size = self.size ?? self.image?.size
            else {
                self.totalSize = nil
                return
            }

            if self.paddingInsets != .zero {
                size.width += self.paddingInsets.left + self.paddingInsets.right
                size.height += self.paddingInsets.top + self.paddingInsets.bottom
            }

            self.totalSize = size
        }
    }
}

public extension EasyImageView.ViewModel {
    final class Animation {
        public let type: AnimationType

        @Published
        public var toggle: Bool

        public init(
            type: AnimationType, 
            toggle: Bool = false
        ) {
            self.type = type
            self.toggle = toggle
        }
    }

    enum AnimationType {
        case transform(AnimationValue<CGAffineTransform>)
    }

    struct AnimationValue<T> {
        public let fromValue: T
        public let toValue: T
        public let duration: CGFloat

        public init(
            fromValue: T,
            toValue: T,
            duration: CGFloat
        ) {
            self.fromValue = fromValue
            self.toValue = toValue
            self.duration = duration
        }
    }
}
