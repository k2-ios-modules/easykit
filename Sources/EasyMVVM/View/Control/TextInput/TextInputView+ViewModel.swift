//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation
import MVVM

extension TextInputView {
    open class ViewModel<InputVM: InputViewModel>: MVVM.ViewModel {
        public let leftSideImages = HorizontalImagesLineViewModel()

        public let inputVM = InputVM()

        public let rightSideImages = HorizontalImagesLineViewModel()

        @Published
        public var clearRightImageVM: EasyImageView.ViewModel?
    }
}
