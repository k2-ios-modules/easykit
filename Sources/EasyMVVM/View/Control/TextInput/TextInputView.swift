//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import EasyUI
import UIKit

public typealias TextInputable = ViewInputable & ViewPlaceholdable

open class TextInputView<
    Input: TextInputable
>: EasyStackView {

    public typealias GenericViewModel = ViewModel<Input.ViewModel>

    // MARK: - properties

    open override var isFirstResponder: Bool {
        self.input.isFirstResponder
    }

    // MARK: - gui

    public private(set) lazy var leftImagesConstainer: EasyStackView = {
        let stack = EasyStackView()
        stack.applyHorizontalCenter()
        stack.spacing = 14
        return stack
    }()

    public private(set) lazy var input: Input = .init()

    public private(set) lazy var rightImagesConstainer: EasyStackView = {
        let stack = EasyStackView()
        stack.applyHorizontalCenter()
        stack.spacing = 14
        return stack
    }()

    private lazy var clearImageView = EasyImageView()

    // MARK: - init

    open override func initView() {
        super.initView()

        self.axis = .horizontal
        self.alignment = .fill
        self.distribution = .fill
        self.spacing = 8

        self.addArrangedSubviews([
            self.leftImagesConstainer,
            self.input,
            self.rightImagesConstainer
        ])
    }

    // MARK: - layout

    open override func layoutSubviews() {
        super.layoutSubviews()

        self.leftImagesConstainer.applyWidthFitting()
        self.rightImagesConstainer.applyWidthFitting()
    }

    // MARK: - setters

    open func setViewModel(_ vm: GenericViewModel) {
        self.cancellables.removeAll()

        self.leftImagesConstainer
            .sinkImages(from: vm.leftSideImages)
            .store(in: &self.cancellables)
        self.input.setViewModel(vm.inputVM)

        Publishers.CombineLatest(
            vm.$clearRightImageVM,
            vm.rightSideImages.$images
        )
        .sink { [weak self, weak vm] (clearImageVM, images) in
            guard let self else { return }

            self.rightImagesConstainer.setImagesVM(images)

            if let clearImageVM {
                let tapCompletion = clearImageVM.tapCompletion
                clearImageVM.tapCompletion = {
                    vm?.inputVM.text = ""
                    tapCompletion?()
                }
                self.clearImageView.setViewModel(clearImageVM)

                self.rightImagesConstainer.insertArrangedSubview(
                    self.clearImageView,
                    at: 0,
                    applyWidthFitting: true
                )
            } else {
                self.rightImagesConstainer.removeArrangedSubview(self.clearImageView)
                self.rightImagesConstainer.applyWidthFitting()
            }
        }
        .store(in: &self.cancellables)
    }

    // MARK: - responder

    @discardableResult
    open override func becomeFirstResponder() -> Bool {
        return self.input.becomeFirstResponder()
    }

    @discardableResult
    open override func resignFirstResponder() -> Bool {
        return self.input.resignFirstResponder()
    }

    open override func hitTest(
        _ point: CGPoint,
        with event: UIEvent?
    ) -> UIView? {
        if self.bounds.contains(point)
            && self.leftImagesConstainer.frame.contains(point) == false
            && self.input.frame.contains(point) == false
            && self.rightImagesConstainer.frame.contains(point) == false {
            return self.input.isEnabled ? self.input : self.input.overlapView
        } else {
            return super.hitTest(point, with: event)
        }
    }
}
