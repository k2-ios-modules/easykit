//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyFoundation
import EasyUI
import Foundation
import MVVM

extension HTMLTextView {
    open class ViewModel: MVVM.ViewModel {
        @Published
        public var textValue: TextValue?
        public var tapLinkCompletion: ((URL) -> Void)?
    }
}
