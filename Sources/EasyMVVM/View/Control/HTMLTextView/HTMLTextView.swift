//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import EasyUI
import UIKit

open class HTMLTextView: EasyUI.HTMLTextView {

    // MARK: - properties

    private weak var viewModel: ViewModel?

    // MARK: - setters

    public func setViewModel(_ vm: ViewModel) {
        self.cancellables.removeAll()

        self.viewModel = vm
        vm
            .$textValue
            .removeDuplicates()
            .sink { [weak self] in self?.setTextValue($0) }
            .store(in: &self.cancellables)
    }

    // MARK: - UITextViewDelegate

    public override func textView(
        _ textView: UITextView,
        shouldInteractWith URL: URL,
        in characterRange: NSRange,
        interaction: UITextItemInteraction
    ) -> Bool {
        self.viewModel?.tapLinkCompletion?(URL)
        return false
    }
}
