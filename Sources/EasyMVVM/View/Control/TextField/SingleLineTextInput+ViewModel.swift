//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

extension SingleLineTextInput {
    open class ViewModel: InputViewModel {
        public let leftLabelVM = LeftLabelVM()
        public let rightLabelVM = RightLabelVM()

        @Published
        public var isSecureTextEntry: Bool = false
    }
}

// MARK: - left label

extension SingleLineTextInput {
    public final class LeftLabelVM: EasyLabel.ViewModel {
        @Published
        public var visible: Visible = .never
    }
}

extension SingleLineTextInput.LeftLabelVM {
    public enum Visible {
        case never
        case always
        case whenSelfNotEmpty
        case whenFieldNotEmpty
    }
}

// MARK: - right label

extension SingleLineTextInput {
    public final class RightLabelVM: EasyLabel.ViewModel {
        @Published
        public var location: Location = .fixed
        @Published
        public var visible: Visible = .never
        @Published
        public var style: Style = .equalToText
    }
}

extension SingleLineTextInput.RightLabelVM {
    public enum Location {
        case moving
        case fixed
    }

    public enum Visible {
        case never
        case always
        case whenSelfNotEmpty
        case whenFieldNotEmpty
    }

    public enum Style {
        case equalToText
        case equalToPlaceholder
    }
}
