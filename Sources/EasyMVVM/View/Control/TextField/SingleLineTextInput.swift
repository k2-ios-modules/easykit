//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import EasyUI
import UIKit

open class SingleLineTextInput: EasyUI.SingleLineTextInput,
                                ViewInputable {

    // MARK: - variables

    private var leftViewBounds: CGRect {
        self.leftView?.bounds ?? .zero
    }

    public private(set) weak var viewModel: ViewModel?

    // MARK: - properties

    open override var font: UIFont? {
        get { super.font }
        set {
            super.font = newValue
            self.leftLabel.font = newValue
            self.rightLabel.font = newValue
        }
    }

    // MARK: - gui

    public private(set) lazy var leftLabel: EasyLabel = {
        let label = EasyLabel()
        label.numberOfLines = 1
        label.textInsets.right = 4
        return label
    }()

    public private(set) lazy var rightLabel: EasyLabel = {
        let label = EasyLabel()
        label.numberOfLines = 1
        label.textInsets.left = 4
        return label
    }()

    // MARK: - init

    open override func initTextField() {
        super.initTextField()

        self.leftViewMode = .always
        self.rightViewMode = .always
        self.leftView = self.leftLabel
    }

    // MARK: - layout

    open override func caretRect(for position: UITextPosition) -> CGRect {
        let rect = super.caretRect(for: position)
        self.updateVisibilityLeftLabel()
        self.updateVisibilityRightLabel()

        if self.viewModel?.rightLabelVM.location == .moving,
           self.rightLabel.isHidden == false {
            self.rightLabel.sizeToFit()

            self.rightLabel.frame.size.height = rect.size.height
            self.rightLabel.frame.origin.y = rect.origin.y
        }
        return rect
    }

    open override func textRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.textRect(forBounds: bounds)
        if self.viewModel?.rightLabelVM.location == .moving,
           self.rightLabel.isHidden == false {

            rect.size.width -= self.rightLabel.frame.width
            self.rightLabel.frame.origin.y = rect.origin.y
            self.rightLabel.frame.origin.x = min(
                self.textSize.width + self.leftViewBounds.width,
                self.bounds.width - self.rightLabel.frame.width
            )
        }
        return rect
    }

    open override func editingRect(forBounds bounds: CGRect) -> CGRect {
        var rect = super.editingRect(forBounds: bounds)
        if self.viewModel?.rightLabelVM.location == .moving,
           self.rightLabel.isHidden == false {
            self.rightLabel.sizeToFit()
            rect.size.width -= self.rightLabel.frame.width

            self.rightLabel.frame.size.height = rect.size.height
            self.rightLabel.frame.origin.y = rect.origin.y
            self.rightLabel.frame.origin.x = min(
                rect.origin.x + self.textSize.width,
                rect.origin.x + rect.width
            )
        }
        return rect
    }

    // MARK: - setters

    open func setViewModel(_ vm: ViewModel) {
        self.cancellables.removeAll()
        self.viewModel = vm
        vm.$isEnabled
            .sink { [weak self] in self?.isEnabled = $0 }
            .store(in: &self.cancellables)
        vm.$disabledTapCompletion
            .sink { [weak self] in self?.setDisabledTapAction($0) }
            .store(in: &self.cancellables)
        vm.$isSecureTextEntry
            .sink { [weak self] in self?.isSecureTextEntry = $0 }
            .store(in: &self.cancellables)
        vm.$text
            .receive(on: DispatchQueue.main)
            .sink { [weak self] in self?.sinkText($0) }
            .store(in: &self.cancellables)
        vm.$placeholderValue
            .sink { [weak self] in self?.placeholderValue = $0 }
            .store(in: &self.cancellables)

        self.leftLabel.setViewModel(vm.leftLabelVM)
        Publishers.CombineLatest(
            vm.leftLabelVM.$textValue,
            vm.leftLabelVM.$visible
        )
        .receive(on: DispatchQueue.main)
        .sink { [weak self] _ in self?.updateVisibilityLeftLabel() }
        .store(in: &self.cancellables)

        self.rightLabel.setViewModel(vm.rightLabelVM)
        vm.rightLabelVM.$location
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in self?.updateRightLabelLocation() }
            .store(in: &self.cancellables)

        Publishers.CombineLatest(
            vm.rightLabelVM.$textValue,
            vm.rightLabelVM.$visible
        )
        .receive(on: DispatchQueue.main)
        .sink { [weak self] _ in self?.updateVisibilityRightLabel() }
        .store(in: &self.cancellables)
        vm.rightLabelVM
            .$style
            .receive(on: DispatchQueue.main)
            .sink { [weak self] _ in self?.updateStyleRightLabel() }
            .store(in: &self.cancellables)
    }

    private func sinkText(_ text: String?) {
        self.text = text
        self.updateSideLabels()
        self.updatePlaceholder()
    }

    // MARK: - actions

    open override func didBeginEditingAction(_ textFiled: UITextField) {
        self.editingAction(type: .didBeginEditing, inputText: textFiled.text)
    }

    open override func editingChangedAction(_ textFiled: UITextField) {
        self.editingAction(type: .editingChanged, inputText: textFiled.text)
    }

    open override func didEndEditingAction(_ textFiled: UITextField) {
        self.editingAction(type: .didEndEditing, inputText: textFiled.text)
    }

    // MARK: - right label style

    private func updateStyleRightLabel() {
        guard let vm = self.viewModel else { return }

        switch vm.rightLabelVM.style {
        case .equalToText:
            self.rightLabel.textColor = self.textColor

        case .equalToPlaceholder:
            self.rightLabel.textColor = (self.text ?? "").isEmpty
            ? self.uiPlaceholder.color
            : self.textColor
        }
    }

    // MARK: - side labels visibility

    private func updateSideLabels() {
        self.updateVisibilityLeftLabel()
        self.updateVisibilityRightLabel()
        self.updateStyleRightLabel()
    }

    private func updateVisibilityLeftLabel() {
        guard let vm = self.viewModel else {
            self.leftView = nil
            return
        }

        switch vm.leftLabelVM.visible {
        case .never:
            self.leftView = nil

        case .always:
            self.leftView = self.leftLabel

        case .whenSelfNotEmpty:
            let text = self.leftLabel.text ?? ""
            self.leftView = text.isEmpty ? nil : self.leftLabel

        case .whenFieldNotEmpty:
            let text = self.text ?? ""
            self.leftView = text.isEmpty ? nil : self.leftLabel
        }
    }

    private func updateVisibilityRightLabel() {
        guard let vm = self.viewModel else {
            self.rightLabel.isHidden = true
            return
        }

        switch vm.rightLabelVM.visible {
        case .never:
            self.rightLabel.isHidden = true

        case .always:
            self.rightLabel.isHidden = false

        case .whenSelfNotEmpty:
            let text = self.rightLabel.text ?? ""
            self.rightLabel.isHidden = text.isEmpty

        case .whenFieldNotEmpty:
            let text = self.text ?? ""
            self.rightLabel.isHidden = text.isEmpty
        }
    }

    private func updateRightLabelLocation() {
        self.rightLabel.removeFromSuperview()
        self.rightView = nil

        guard let location = self.viewModel?.rightLabelVM.location else { return }

        switch location {
        case .moving:
            self.addSubview(self.rightLabel)

        case .fixed:
            self.rightView = self.rightLabel
        }
    }
}
