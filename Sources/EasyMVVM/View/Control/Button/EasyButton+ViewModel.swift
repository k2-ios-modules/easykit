//
//  Created on 08.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyFoundation
import EasyUI
import UIKit
import MVVM

extension EasyButton {
    open class ViewModel: MVVM.ViewModel {
        @Published
        public var title: TextValue?

        @Published
        public var isEnabled: Bool

        @Published
        public var isSelected: Bool

        @Published
        public var style: Style

        @Published
        public var action: UIControl.Action?

        public init(
            title: TextValue? = nil,
            isEnabled: Bool = true,
            isSelected: Bool = false,
            style: Style = .empty,
            action: Action? = nil
        ) {
            self.title = title
            self.isEnabled = isEnabled
            self.isSelected = isSelected
            self.style = style
            self.action = action
        }
    }
}
