//
//  Created on 08.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import EasyUI
import UIKit
import MVVM

open class EasyButton: EasyUI.EasyButton {

    // MARK: - setters

    open func setViewModel(_ vm: ViewModel) {
        self.cancellables.removeAll()

        vm.$style
            .removeDuplicates()
            .sink { [weak self] in self?.setStyle($0) }
            .store(in: &self.cancellables)

        vm.$isEnabled
            .removeDuplicates()
            .sink { [weak self] in self?.isEnabled = $0 }
            .store(in: &self.cancellables)

        vm.$isSelected
            .removeDuplicates()
            .sink { [weak self] in self?.isSelected = $0 }
            .store(in: &self.cancellables)

        vm.$title
            .removeDuplicates()
            .sink { [weak self] in self?.setTextValue($0) }
            .store(in: &self.cancellables)

        self.sinkAction(vm.$action)
    }

    public final func sinkAction(_ action: Published<Action?>.Publisher) {
        action.sink { [weak self] action in
            guard let self else { return }
            self.cancellables.removeByIdentifier("actionEvent")

            guard let action else { return }
            self.publisher(for: action.event)
                .sink(receiveValue: { _ in action() })
                .setIdentifier("actionEvent")
                .store(in: &self.cancellables)
        }
        .store(in: &self.cancellables)
    }
}
