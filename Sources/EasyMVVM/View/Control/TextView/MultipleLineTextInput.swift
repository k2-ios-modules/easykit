//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import EasyUI
import UIKit

open class MultipleLineTextInput: EasyUI.MultipleLineTextInput,
                                  ViewInputable {

    // MARK: - variables

    public private(set) weak var viewModel: ViewModel?

    // MARK: - properties

    open override var placeholderValue: PlaceholderValue? {
        get { self.viewModel?.placeholderValue }
        set { self.updatePlaceholder() }
    }

    // MARK: - setters

    open func setViewModel(_ vm: ViewModel) {
        self.cancellables.removeAll()
        self.viewModel = vm
        vm.$isEnabled
            .sink { [weak self] in self?.isEnabled = $0 }
            .store(in: &self.cancellables)
        vm.$disabledTapCompletion
            .sink { [weak self] in self?.setDisabledTapAction($0) }
            .store(in: &self.cancellables)
        vm.$placeholderValue
            .sink { [weak self] in self?.placeholderValue = $0 }
            .store(in: &self.cancellables)
        Publishers.CombineLatest3(
            vm.$text.removeDuplicates(),
            vm.$minNumberOfLinesForScrolling.removeDuplicates(),
            vm.$maxNumberOfLinesForScrolling.removeDuplicates()
        )
        .receive(on: DispatchQueue.main)
        .sink { [weak self] (text, minLines, maxLines) in
            self?.sinkText(text, minLines: minLines, maxLines: maxLines)
        }
        .store(in: &self.cancellables)
    }

    private func sinkText(
        _ text: String?,
        minLines: Int,
        maxLines: Int
    ) {
        if self.text != text {
            self.text = text
        }
        self.updatePlaceholder()
        self.setNumberOfLines(min: minLines, max: maxLines)
    }

    // MARK: - UITextViewDelegate

    open override func textViewDidBeginEditing(_ textView: UITextView) {
        self.editingAction(type: .didBeginEditing, inputText: textView.text)
        self.updateHeightConstraint()
    }

    open override func textViewDidChange(_ textView: UITextView) {
        self.editingAction(type: .editingChanged, inputText: textView.text)
        self.updateHeightConstraint()
    }

    open override func textViewDidEndEditing(_ textView: UITextView) {
        self.editingAction(type: .didEndEditing, inputText: textView.text)
        self.updateHeightConstraint()
    }
}
