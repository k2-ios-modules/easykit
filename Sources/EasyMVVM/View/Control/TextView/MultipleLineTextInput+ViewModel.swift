//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

extension MultipleLineTextInput {
    open class ViewModel: InputViewModel {
        @Published
        public var minNumberOfLinesForScrolling: Int = 1
        @Published
        public var maxNumberOfLinesForScrolling: Int = 1
    }
}
