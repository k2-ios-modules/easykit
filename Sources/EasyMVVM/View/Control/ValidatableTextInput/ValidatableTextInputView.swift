//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import EasyUI
import UIKit

protocol ValidatableTextInputProtocol: TextInputProtocol where Self: EasyStackView {
    var textDidBeginEditingCompletion: (() -> Void)? { get set }
    var textDidEndEditingCompletion: (() -> Void)? { get set }
    var placeholder: ViewPlaceholdable { get }
}

public typealias TextValidatable = TextInputable & UITextInput

open class ValidatableTextInputView<
    Input: TextValidatable
>: EasyStackView,
   ValidatableTextInputProtocol,
   TextInputDelegate {

    public typealias InputView = TextInputView<Input>
    public typealias TextInputViewModel = InputView.GenericViewModel

    // MARK: - variables

    private var validateRulesCancallable: AnyCancellable? {
        didSet { oldValue?.cancel() }
    }

    public private(set) var wasFirstResponder: Bool = false

    private weak var viewModel: GenericViewModel?

    open var scrollContentAdditionalBottomInset: CGFloat {
        self.frame.height
        - self.input.frame.height
    }

    // MARK: - properties

    open override var isFirstResponder: Bool {
        self.textInputView.isFirstResponder
    }

    open var isEnabled: Bool {
        get { self.input.isEnabled }
        set { self.input.isEnabled = newValue }
    }

    public var input: Input { self.textInputView.input }

    public var placeholder: ViewPlaceholdable { self.input }

    open var text: String {
        get { self.input.textString ?? "" }
        set { self.input.textString = newValue }
    }

    public var textDidBeginEditingCompletion: (() -> Void)?

    public var textDidEndEditingCompletion: (() -> Void)?

    // MARK: - gui

    public private(set) lazy var textInputView: InputView = {
        let textInput = InputView()
        textInput.input.textInputDelegate = self
        return textInput
    }()

    // MARK: - init

    open override func initView() {
        super.initView()

        self.addArrangedSubviews([
            self.textInputView
        ])
    }

    // MARK: - life cycle

    open override func didMoveToWindow() {
        super.didMoveToWindow()

        if let viewModel {
            self.validateRulesCancallable = viewModel.$validationRules
                .dropFirst()
                .receive(on: DispatchQueue.main)
                .sink { [weak viewModel] rules in
                    viewModel?.silentValidate()
                }
        }
    }

    open override func removeFromSuperview() {
        super.removeFromSuperview()

        self.validateRulesCancallable = nil
    }

    // MARK: - setters

    open func setViewModel(_ vm: GenericViewModel) {
        self.cancellables.removeAll()
        self.viewModel = vm

        self.textInputView.setViewModel(vm.textInputVM)
    }

    // MARK: - responder

    @discardableResult
    open override func becomeFirstResponder() -> Bool {
        self.textInputView.becomeFirstResponder()
    }

    @discardableResult
    open override func resignFirstResponder() -> Bool {
        self.textInputView.resignFirstResponder()
    }

    // MARK: - TextInputDelegate

    open func textDidBeginEditing(_ text: String) {
        self.wasFirstResponder = true
        self.textDidBeginEditingCompletion?()
        self.viewModel?.validateIfNeeded(text: text, when: .didBeginEditing)
    }

    open func textEditingChanged(_ text: String) {
        self.viewModel?.validateIfNeeded(text: text, when: .editingChanged)
    }

    open func text(
        _ text: String,
        shouldChangeCharactersIn range: NSRange,
        replacementString string: String
    ) -> Bool {
        let newString = (text as NSString).replacingCharacters(in: range, with: string)

        guard let vm = self.viewModel
        else { return true }

        guard string.isEmpty || vm.checkRestriction(text: newString)
        else { return false }

        return true
    }

    open func textDidEndEditing(_ text: String) {
        self.textDidEndEditingCompletion?()
        self.viewModel?.validateIfNeeded(text: text, when: .didEndEditing)
    }

    open func getCursorOffset(
        range: NSRange,
        oldString: String,
        newString: String,
        newSymbols: String,
        separators: Set<Character>
    ) -> Int {
        let offset = range.location + newSymbols.count

        guard newString.isEmpty == false, oldString.count != newString.count else { return offset }

        let oldSelfFillingCount = oldString
            .prefix(range.location)
            .filter({ separators.contains($0) })
            .count

        let newSelfFillingCount = newString
            .prefix(max(offset + newString.count - oldString.count, offset))
            .filter({ separators.contains($0) })
            .count

        return offset + newSelfFillingCount - oldSelfFillingCount
    }

    open func moveCursor(to offset: Int) {
        let cursorLocation = input.position(
            from: input.beginningOfDocument,
            offset: offset
        )

        if let position = cursorLocation {
            input.selectedTextRange = input.textRange(
                from: position,
                to: position
            )
        }
    }
}
