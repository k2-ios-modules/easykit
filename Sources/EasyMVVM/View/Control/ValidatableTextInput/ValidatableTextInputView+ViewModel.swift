//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import EasyUI
import Foundation
import FormValidator
import MVVM

extension ValidatableTextInputView {
    open class GenericViewModel: MVVM.ViewModel,
                                 StateValidatable,
                                 RequiredValidatable,
                                 CheckHasChangable,
                                 Restrictable {

        // MARK: - variables

        private weak var requiredRule: ValidatableRule?

        // MARK: - properties

        public let textInputVM = TextInputViewModel()

        public var validationEditingType: ValidationEditingType = .all

        public private(set) var isValid: ValidSubject = .init(true)

        public private(set) var hasChange: ChangeSubject = .init(false)

        @Published
        public private(set) var isRequred: Bool = false

        public var checkedValue: (() -> String?)?

        @Published
        public private(set) var invalidRule: ValidatableRule?

        @Published
        public var validationRules: [ValidatableRule] = []

        public var restrictionRules: [RestrictableRule] = []

        // MARK: - validation

        public final func setRequiredRule(_ rule: ValidatableRule) {
            self.removeRequiredRule()
            self.requiredRule = rule
            self.validationRules.insert(rule, at: 0)
            self.isRequred = true
        }

        public final func removeRequiredRule() {
            if let rule = self.requiredRule {
                self.validationRules.removeAll(where: { $0 === rule })
            }
            self.isRequred = false
        }

        /// validation without showing error
        @discardableResult
        private func silentValidate(text: String) -> ValidatableRule? {
            switch (self.isRequred, text.isEmpty, self.isEnabled) {
            case (true, _, true), (false, false, true):
                let invalidRule = self.validationRules.first(where: {
                    $0.validate(text) == false
                })

                self.checkChanged(value: text)

                self.isValid.send(invalidRule == nil)
                return invalidRule

            default:
                self.isValid.send(true)
                return nil
            }
        }

        /// validation without showing error
        public final func silentValidate() {
            self.silentValidate(text: self.text ?? "")
        }

        @discardableResult
        public final func validate(text: String) -> Bool {
            self.invalidRule = self.silentValidate(text: text)
            return self.invalidRule == nil
        }

        @discardableResult
        public final func validate() -> Bool {
            return self.validate(text: self.text ?? "")
        }

        public final func validateIfNeeded(
            text: String,
            when editingType: InputViewModel.EditingType
        ) {
            guard self.validationEditingType.contains(editingType)
            else { return }
            self.validate(text: text)
        }
    }
}

// MARK: - validation editing

public extension ValidatableTextInputView.GenericViewModel {
    typealias ValidationEditingType = Set<InputViewModel.EditingType>
}

public extension ValidatableTextInputView.GenericViewModel.ValidationEditingType {
    static var all: Self {
        Self(Element.allCases)
    }
}

extension ValidatableTextInputView.GenericViewModel {

    // MARK: - proxy properties

    public var text: String? {
        get { self.textInputVM.inputVM.text }
        set { self.textInputVM.inputVM.text = newValue }
    }

    public var placeholderValue: PlaceholderValue? {
        get { self.textInputVM.inputVM.placeholderValue }
        set { self.textInputVM.inputVM.placeholderValue = newValue }
    }

    public var isEnabled: Bool {
        get { self.textInputVM.inputVM.isEnabled }
        set { self.textInputVM.inputVM.isEnabled = newValue }
    }

    public var disabledTapCompletion: (() -> Void)? {
        get { self.textInputVM.inputVM.disabledTapCompletion }
        set { self.textInputVM.inputVM.disabledTapCompletion = newValue }
    }

    // MARK: - proxy publishers

    public var editingTextPublisher: AnyPublisher<InputViewModel.EditingText, Never> {
        self.textInputVM.inputVM.editingText.eraseToAnyPublisher()
    }

    public var enabledPublisher: AnyPublisher<Bool, Never> {
        self.textInputVM.inputVM.$isEnabled.eraseToAnyPublisher()
    }
}
