//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import EasyUI
import UIKit
import MVVM

open class InputViewModel: MVVM.ViewModel {
    @Published
    public var text: String?

    public private(set) var editingText: PassthroughSubject<EditingText, Never> = .init()

    @Published
    public var placeholderValue: PlaceholderValue?

    @Published
    public var isEnabled: Bool = true

    @Published
    public var disabledTapCompletion: (() -> Void)?

    required public override init() {
        super.init()
    }
}

public extension InputViewModel {
    enum EditingType: Int, CaseIterable {
        case didBeginEditing
        case editingChanged
        case didEndEditing
    }

    struct EditingText {
        public let editingType: EditingType
        public let text: String

        public init(
            editingType: EditingType,
            text: String
        ) {
            self.editingType = editingType
            self.text = text
        }
    }
}
