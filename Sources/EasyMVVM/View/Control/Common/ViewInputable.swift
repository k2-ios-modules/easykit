//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyUI
import Foundation

public protocol ViewInputable: EasyUI.ViewInputable {

    // MARK: - subtypes

    associatedtype ViewModel: InputViewModel

    // MARK: - view model

    var viewModel: ViewModel? { get }
    func setViewModel(_ vm: ViewModel)
}

public extension ViewInputable {
    func editingAction(
        type: InputViewModel.EditingType,
        inputText: String?
    ) {
        let text = inputText ?? ""
        let textVM = self.viewModel?.text ?? ""
        let actualText: String

        if type != .editingChanged,
           text != textVM {
            actualText = textVM
        } else {
            actualText = text
        }

        self.viewModel?.text = actualText
        self.viewModel?.editingText.send(.init(
            editingType: type,
            text: actualText
        ))

        switch type {
        case .didBeginEditing:
            self.textInputDelegate?.textDidBeginEditing?(actualText)

        case .editingChanged:
            self.textInputDelegate?.textEditingChanged?(actualText)

        case .didEndEditing:
            self.textInputDelegate?.textDidEndEditing?(actualText)
        }
    }
}
