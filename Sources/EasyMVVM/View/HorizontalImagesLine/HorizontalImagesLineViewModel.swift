//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyUI
import Foundation
import MVVM

public final class HorizontalImagesLineViewModel: MVVM.ViewModel {
    @Published
    public var images: [EasyImageView.ViewModel] {
        didSet { self.applyTotalSizeImages() }
    }

    public init(images: [EasyImageView.ViewModel] = []) {
        self.images = images
        super.init()
        self.applyTotalSizeImages()
    }

    private func applyTotalSizeImages() {
        self.images.forEach({ $0.applyTotalSize = true })
    }
}
