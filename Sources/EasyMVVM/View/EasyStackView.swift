//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import EasyUI
import Foundation

extension EasyUI.EasyStackView {

    // MARK: - makers

    private func makeSideImageView(vm: EasyImageView.ViewModel) -> EasyImageView {
        let imageView = EasyImageView()
        imageView.setViewModel(vm)
        return imageView
    }

    // MARK: - setters

    public func setImagesVM(_ vmList: [EasyImageView.ViewModel]) {
        self.clearArrangedSubviews()
        self.addArrangedSubviews(
            vmList.map(self.makeSideImageView),
            applyWidthFitting: true
        )
    }

    public func sinkImages(
        from vm: HorizontalImagesLineViewModel
    ) -> AnyCancellable {
        vm.$images.sink { [weak self] in self?.setImagesVM($0) }
    }
}
