//
//  Created on 21.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation
import MVVM

extension EasySlideTabView {
    open class ViewModel: MVVM.ViewModel {
        @Published
        public var tapCompletion: (() -> Void)?
    }
}
