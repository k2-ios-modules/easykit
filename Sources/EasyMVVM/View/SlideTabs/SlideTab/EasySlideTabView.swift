//
//  Created on 21.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyFoundation
import EasyUI
import Foundation

open class EasySlideTabView: EasyUI.EasyView,
                             EasyFoundation.TapGestureRecognizerable {

    // MARK: - variables

    public private(set) lazy var tapGestureRecognizer = TapGestureRecognizer()

    // MARK: - init

    required public override init() {
        super.init()
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - setters

    open func setViewModel(_ vm: ViewModel) {
        self.cancellables.removeAll()
        self.sinkTapCompletion(vm.$tapCompletion)
            .store(in: &self.cancellables)
    }
}
