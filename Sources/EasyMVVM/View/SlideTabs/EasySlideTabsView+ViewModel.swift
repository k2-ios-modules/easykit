//
//  Created on 21.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyUI
import Foundation
import MVVM

extension EasySlideTabsView {
    open class ViewModel: MVVM.ViewModel {

        // MARK: - properties

        @Published
        public private(set) var slideTabsVM: [SlideTabViewModel] = []

        @Published
        public private(set) var selectedIndex: Int = 0

        // MARK: - setters

        public final func setSlideTabs(_ list: [SlideTabViewModel]) {
            list.enumerated().forEach({ item in
                item.element.tapCompletion = { [weak self] in
                    guard let self else { return }
                    let index = item.offset
                    self.didSelectIndexAction(index)
                    self.setSelectedIndex(index)
                }
            })
            self.slideTabsVM = list
        }

        open func setSelectedIndex(_ index: Int) {
            guard self.selectedIndex != index else { return }
            self.selectedIndex = index
        }

        // MARK: - actions

        open func didSelectIndexAction(_ index: Int) { }
    }

    open class GenericViewModel<Item: Equatable>: ViewModel {

        // MARK: - subtypes

        public typealias Item = Item

        // MARK: - variables

        private var items: [Item] = []

        // MARK: - properties

        @Published
        public private(set) var selectedItem: Item

        public var didSelectItemCompletion: ((Item) -> Void)?

        // MARK: - init

        public init(selectedItem: Item) {
            self.selectedItem = selectedItem
            super.init()

            self.$selectedIndex
                .dropFirst()
                .sink { [weak self] index in
                    guard let self, let item = self.items[safe: index] else { return }
                    self.selectedItem = item
                }
                .store(in: &self.cancellables)
        }

        // MARK: - setters

        public func setItems(
            _ list: [Item],
            tabVMCompletion: (Item) -> SlideTabViewModel
        ) {
            guard self.items != list else { return }
            self.items = list
            self.setSlideTabs(list.map(tabVMCompletion))
            self.setSelectedItem(self.selectedItem)
        }

        public func setSelectedItem(_ item: Item) {
            guard let index = self.items.firstIndex(where: { $0 == item }) else { return }
            self.setSelectedIndex(index)
            self.selectedItem = item
        }

        // MARK: - actions

        public final override func didSelectIndexAction(_ index: Int) {
            guard let item = self.items[safe: index] else { return }
            self.didSelectItemCompletion?(item)
        }
    }
}
