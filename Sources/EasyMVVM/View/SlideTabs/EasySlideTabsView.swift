//
//  Created on 21.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyUI
import Foundation

open class EasySlideTabsView<
    SlideTabView: EasySlideTabView,
    SlideTabViewModel: EasySlideTabView.ViewModel
>: EasyUI.EasySlideTabsView<SlideTabView> {

    public typealias SlideTabViewModel = SlideTabViewModel

    // MARK: - variables

    private weak var viewModel: ViewModel?

    // MARK: - life cicle

    open override func layoutSubviews() {
        super.layoutSubviews()

        if let viewModel = self.viewModel {
            self.setSelectedSlideTab(by: viewModel.selectedIndex)
        }
    }

    // MARK: - makers

    open func makeSlideTabView(viewModel: SlideTabViewModel) -> SlideTabView {
        let slideTabView = SlideTabView()
        slideTabView.setViewModel(viewModel)
        return slideTabView
    }

    // MARK: - setters

    open func setViewModel(_ vm: ViewModel) {
        self.cancellables.removeAll()
        self.viewModel = vm

        vm.$slideTabsVM
            .sink { [weak self] vm in
                guard let self else { return }
                self.setTabs(vm.map({ self.makeSlideTabView(viewModel: $0) }))
            }
            .store(in: &self.cancellables)

        vm.$selectedIndex
            .sink { [weak self] in self?.setSelectedSlideTab(by: $0) }
            .store(in: &self.cancellables)
    }
}
