//
//  Created on 03.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyFoundation
import EasyUI
import Combine

open class EasyLabel: EasyUI.EasyLabel {

    // MARK: - setters

    public final func removeTextValuePublisher() {
        self.cancellables.removeByIdentifier("textValue")
    }

    public final func sinkTextValueForDisplay(
        _ publisher: Published<TextValue>.Publisher
    ) -> AnyCancellable {
        publisher
            .removeDuplicates()
            .sink { [weak self] textValue in
                self?.isHidden = textValue.isEmpty
            }
    }

    open func setViewModel(_ vm: ViewModel) {
        self.cancellables.removeAll()

        self.sinkTapCompletion(vm.$tapCompletion, checkUserInteraction: true)
            .store(in: &self.cancellables)
        vm
            .$textValue
            .sink { [weak self] in self?.setTextValue($0) }
            .setIdentifier("textValue")
            .store(in: &self.cancellables)
    }
}
