//
//  Created on 03.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyFoundation
import Foundation
import MVVM

extension EasyLabel {
    open class ViewModel: MVVM.ViewModel {
        @Published
        public var tapCompletion: (() -> Void)?

        @Published
        public var textValue: TextValue

        public init(textValue: TextValue = .text(nil)) {
            self.textValue = textValue
        }
    }
}
