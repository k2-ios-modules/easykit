//
//  Created on 15.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

extension UIViewController {
    func performAlongsideTransition(
        animation: (() -> Void)? = nil,
        completion: (() -> Void)? = nil
    ) {
        guard let transitionCoordinator else {
            animation?()
            completion?()
            return
        }
        transitionCoordinator.animate(
            alongsideTransition: { _ in animation?() },
            completion: { _ in completion?() }
        )
    }
}
