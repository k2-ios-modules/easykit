//
//  Created on 15.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyFoundation
import UIKit

extension UIPanGestureRecognizer {
    func translation() -> CGPoint {
        translation(in: view)
    }

    func location() -> CGPoint {
        translation(in: view)
    }

    func velocity() -> CGPoint {
        velocity(in: view)
    }

    func endLocation(decelerationRate: UIScrollView.DecelerationRate = .fast) -> CGPoint {
        location() + velocity().endPoint(decelerationRate: decelerationRate)
    }

    func endTranslation(decelerationRate: UIScrollView.DecelerationRate = .fast) -> CGPoint {
        translation() + velocity().endPoint(decelerationRate: decelerationRate)
    }
}

// MARK: - CGPoint

private extension CGPoint {
    func endPoint(decelerationRate: UIScrollView.DecelerationRate) -> CGPoint {
        CGPoint(
            x: x.endPoint(decelerationRate: decelerationRate),
            y: y.endPoint(decelerationRate: decelerationRate)
        )
    }
}

// MARK: - CGFloat

private extension CGFloat {
    func endPoint(decelerationRate: UIScrollView.DecelerationRate) -> CGFloat {
        self * (1 / (1 - decelerationRate.rawValue) / 1000)
    }
}
