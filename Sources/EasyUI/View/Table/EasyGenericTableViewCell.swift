//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyFoundation
import UIKit

/// Простая вью ячейки таблицы на основе `EasyTableViewCell` с указанным типом вью `T`.
///
/// Переданый тип вью контента `MainView` располагается на `contentView`.
///
/*
    EasyGenericTableViewCell
    +----------------------------------------------------+
    |   .contentView                                     |
    |   +--------------------------------------------+   |
    |   |   MainView                                 |   |
    |   |   +------------------------------------+   |   |
    |   |   |                                    |   |   |
    |   |   |                                    |   |   |
    |   |   |                                    |   |   |
    |   |   |                                    |   |   |
    |   |   |                                    |   |   |
    |   |   +------------------------------------+   |   |
    |   |                                            |   |
    |   +--------------------------------------------+   |
    |                                                    |
    +----------------------------------------------------+
*/
open class EasyGenericTableViewCell<T: UIView>: EasyTableViewCell,
                                                GenericContentViewable {
    
    public typealias MainView = T

    // MARK: - gui

    public private(set) lazy var mainView = MainView()

    // MARK: - init

    open override func initView() {
        super.initView()

        self.setupLayout()
    }
}
