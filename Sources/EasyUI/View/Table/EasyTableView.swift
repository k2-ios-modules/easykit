//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import EasyFoundation
import UIKit

/// Простая таблица на основе `UITableView` с предустановленным состоянием
/// для программной верстки`translatesAutoresizingMaskIntoConstraints = false`.
///
/// Фон и стиль выбора сброшены по-умолчанию.
/// Для `>= iOS 15.0` сброшен верхний отступ заголовка серкции `sectionHeaderTopPadding`.
///
/// Таблица имеет возможность расчера внутреннего размера контенра по размеру `contentSize`.
open class EasyTableView: UITableView {

    // MARK: - properties

    /// Хранилище подписок.
    public var cancellables: Set<AnyCancellable> = []

    /// Учитывает размер `contentSize` для перерасчета внутреннего размера таблица.
    public var withContentSized: Bool = false

    open override var tableHeaderView: UIView? {
        didSet {
            guard oldValue !== self.tableHeaderView else { return }
            self.setNeedsLayout()
        }
    }

    open override var contentSize: CGSize {
        didSet {
            guard self.withContentSized else { return }
            self.invalidateIntrinsicContentSize()
        }
    }

    open override var intrinsicContentSize: CGSize {
        guard self.withContentSized else { return super.intrinsicContentSize }
        self.layoutIfNeeded()

        let width = UIView.noIntrinsicMetric
        let height = self.contentSize.height + contentInset.top + contentInset.bottom

        return .init(width: width, height: height)
    }

    // MARK: - init

    public init(style: UITableView.Style) {
        super.init(frame: .zero, style: style)
        self.initView()
    }

    public override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        self.initView()
    }

    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// Метод инициализации таблицы, вызывается в конструкторе.
    open func initView() {
        self.prepareForAutolayout()
        self.showsVerticalScrollIndicator = true
        self.showsHorizontalScrollIndicator = false
        self.separatorStyle = .none
        self.backgroundColor = .clear
        self.tableHeaderView = self.tableHeaderView ?? UIView(frame: .init(
            origin: .zero,
            size: .init(width: .zero, height: CGFloat.leastNonzeroMagnitude)
        ))
        self.tableFooterView = self.tableFooterView ?? UIView(frame: .init(
            origin: .zero,
            size: .init(width: .zero, height: CGFloat.leastNonzeroMagnitude)
        ))
        if #available(iOS 15.0, *) {
            self.sectionHeaderTopPadding = .zero
        }
    }

    // MARK: - layout

    open override func layoutSubviews() {
        super.layoutSubviews()

        self.layoutTableHeaderIfNeeded()
    }
}
