//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyFoundation
import UIKit

/// Простая вью заголовка/футера секции таблицы на основе `EasyTableViewHeaderFooterView` с указанным типом вью `T`.
///
/// Переданый тип вью контента `MainView` располагается на `contentView`.
///
/*
    EasyGenericTableViewHeaderFooterView
    +----------------------------------------------------+
    |   .contentView                                     |
    |   +--------------------------------------------+   |
    |   |   MainView                                 |   |
    |   |   +------------------------------------+   |   |
    |   |   |                                    |   |   |
    |   |   |                                    |   |   |
    |   |   |                                    |   |   |
    |   |   |                                    |   |   |
    |   |   |                                    |   |   |
    |   |   +------------------------------------+   |   |
    |   |                                            |   |
    |   +--------------------------------------------+   |
    |                                                    |
    +----------------------------------------------------+
*/
open class EasyGenericTableViewHeaderFooterView<T: UIView>: EasyTableViewHeaderFooterView,
                                                            GenericContentViewable {

    public typealias MainView = T

    // MARK: - gui

    public private(set) lazy var mainView = MainView()

    // MARK: - init

    open override func initView() {
        super.initView()

        self.setupLayout()
    }
}
