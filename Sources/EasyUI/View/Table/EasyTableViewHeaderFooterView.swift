//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyFoundation
import UIKit

/// Простая вью заголовка/футера секции таблицы на основе `UITableViewHeaderFooterView` с предустановленным состоянием
/// для программной верстки`translatesAutoresizingMaskIntoConstraints = false`.
///
/// Фон и стиль выбора сброшены по-умолчанию.
open class EasyTableViewHeaderFooterView: UITableViewHeaderFooterView {

    // MARK: - initialization

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)

        self.initView()
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    open func initView() {
        self.contentView.backgroundColor = .clear

        if #available(iOS 14.0, *) {
            if var backgroundConfiguration {
                backgroundConfiguration.backgroundColor = .clear
                self.backgroundConfiguration = backgroundConfiguration
            }
        } else {
            self.backgroundColor = .clear
        }
    }
}
