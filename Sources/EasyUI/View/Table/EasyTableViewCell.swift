//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyFoundation
import UIKit

/// Простая вью ячейки таблицы на основе `UITableViewCell` с предустановленным состоянием
/// для программной верстки`translatesAutoresizingMaskIntoConstraints = false`.
///
/// Фон, отступы разделительной линии и стиль выбора сброшены по-умолчанию.
open class EasyTableViewCell: UITableViewCell {

    // MARK: - init

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.initView()
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    open func initView() {
        self.selectionStyle = .none
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
    }

    // MARK: - reuse

    open override func prepareForReuse() {
        super.prepareForReuse()

        self.separatorInset = .zero
    }
}

