//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import EasyFoundation
import UIKit

/// Простой стэк на основе `UIStackView` с предустановленным состоянием
/// для программной верстки`translatesAutoresizingMaskIntoConstraints = false`
/// и некоторыми настройками макета.
open class EasyStackView: UIStackView {

    // MARK: - properties

    /// Хранилище подписок.
    public var cancellables: Set<AnyCancellable> = []

    /// Внешние оступы у стэка.
    ///
    /*
         EasyStackView.superview
         +----------------------------------------------------+
         |                        ↑                           |
         |   EasyStackView                                    |
         |   +--------------------------------------------+   |
         |   |   .arrangedSubviews                        |   |
         |   |   +------------------------------------+   |   |
         |   |   |                                    |   |   |
         |   |   |                                    |   |   |
         | ← |   |                                    |   | → |
         |   |   |                                    |   |   |
         |   |   |                                    |   |   |
         |   |   +------------------------------------+   |   |
         |   |                                            |   |
         |   +--------------------------------------------+   |
         |                        ↓                           |
         +----------------------------------------------------+
    */
    public var marginInsets: UIEdgeInsets = .zero {
        didSet {
            self.updateLayoutMargins()
            self.setNeedsLayout()
        }
    }

    /// Внутренние оступы у стэка.
    ///
    /*
        EasySackView.superview
        +----------------------------------------------------+
        |   EasyStackView                                    |
        |   +--------------------------------------------+   |
        |   |                     ↑                      |   |
        |   |   .arrangedSubviews                        |   |
        |   |   +------------------------------------+   |   |
        |   |   |                                    |   |   |
        |   |   |                                    |   |   |
        |   | ← |                                    | → |   |
        |   |   |                                    |   |   |
        |   |   |                                    |   |   |
        |   |   +------------------------------------+   |   |
        |   |                     ↓                      |   |
        |   +--------------------------------------------+   |
        |                                                    |
        +----------------------------------------------------+
    */
    public var paddingInsets: UIEdgeInsets = .zero {
        didSet { self.updateLayoutMargins() }
    }

    open override var alignmentRectInsets: UIEdgeInsets {
        UIEdgeInsets(
            top: -self.marginInsets.top,
            left: -self.marginInsets.left,
            bottom: -self.marginInsets.bottom,
            right: -self.marginInsets.right
        )
    }

    public override var cornerRadius: CGFloat {
        get {
            if #available(iOS 14, *) {
                return super.cornerRadius
            } else {
                return self.backgroundView.cornerRadius
            }
        }
        set {
            if #available(iOS 14, *) {
                super.cornerRadius = newValue
            } else {
                self.backgroundView.cornerRadius = newValue
            }
        }
    }

    public override var backgroundColor: UIColor? {
        get {
            if #available(iOS 14, *) {
                return super.backgroundColor
            } else {
                return self.backgroundView.backgroundColor
            }
        }
        set {
            if #available(iOS 14, *) {
                super.backgroundColor = newValue
            } else {
                self.backgroundView.backgroundColor = newValue
            }

        }
    }

    // MARK: - subviews

    private lazy var backgroundView: UIView = {
        let view = UIView(frame: self.bounds)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.insertSubview(view, at: 0)
        return view
    }()

    // MARK: - init

    public init() {
        super.init(frame: .zero)
        self.initView()
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.initView()
    }

    public required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// Метод инициализации стека, вызывается в конструкторе.
    open func initView() {
        self.prepareForAutolayout()
        self.axis = .vertical
        self.alignment = .fill
        self.distribution = .fill
        self.spacing = 0
    }

    // MARK: - updating

    private func updateLayoutMargins() {
        let insets = UIEdgeInsets(
            top: self.marginInsets.top + self.paddingInsets.top,
            left: self.marginInsets.left + self.paddingInsets.left,
            bottom: self.marginInsets.bottom + self.paddingInsets.bottom,
            right: self.marginInsets.right + self.paddingInsets.right
        )
        self.isLayoutMarginsRelativeArrangement = insets != .zero
        self.layoutMargins = insets
    }
}
