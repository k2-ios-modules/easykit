//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//

import EasyFoundation
import UIKit

/// Вью для отображения HTML текста.
/// При формировании атрибутивной строки так же учитывается HTML, указанный непосредственно внутри строки.
open class HTMLTextView: EasyTextView,
                         UITextViewDelegate {

    // MARK: - subtypes

    typealias Attributes = [NSAttributedString.Key: Any]

    // MARK: - varaibles

    /// Общие атрибуты HTML текста.
    /// Необходимо в случае применения кастомных атрибутов внутри некоторых участков всего текста.
    private var commonAttributes: Attributes {[
        .font: self.commonFont ?? .systemFont(ofSize: 14),
        .foregroundColor: self.textColor ?? .black
    ]}

    /// Общий шрифт текста.
    private var commonFont: UIFont?

    // MARK: - properties

    open override var font: UIFont? {
        get { super.font }
        set {
            super.font = newValue
            self.commonFont = newValue
            self.updateTextValue()
        }
    }

    open override var textColor: UIColor? {
        get { super.textColor }
        set {
            super.textColor = newValue
            self.updateTextValue()
        }
    }

    /// Цвет контента для ссылок `<a>content</a>`.
    public var linkColor: UIColor? = .link {
        didSet { self.updateTextValue() }
    }

    /// Учет отображения линий для ссылок.
    public var linkUnderline: Bool = true

    // MARK: - init

    /// Метод инициализации, вызывается в конструкторе родителя.
    open override func initTextView() {
        super.initTextView()

        self.backgroundColor = .clear
        self.delegate = self
        self.isEditable = false
        self.isScrollEnabled = false
        self.inputAccessoryView = nil
        self.font = super.font
    }

    // MARK: - setters

    /// Установить значение текста для отображения в HTML.
    public func setTextValue(_ textValue: TextValue?) {
        defer {
            self.sizeToFit()
        }

        let attributedString: NSAttributedString?
        switch textValue {
        case let .text(text):
            attributedString = self.makeAttributedStringHTML(text)

        case let .attributed(attributed):
            attributedString = attributed

        case .none:
            attributedString = nil
        }
        self.updateAttributedText(attributedString)
    }

    /// Обновление всего HTML текста при изменении визуальных параметров.
    private func updateTextValue() {
        defer {
            self.sizeToFit()
        }
        self.updateAttributedText(self.attributedText)
    }

    /// Формирование HTML атрибутивной строки.
    /// - Parameter htmlBody: Текствовое представлени HTML.
    /// - Returns: HTML атрибутивной строка.
    private func makeAttributedStringHTML(_ htmlBody: String?) -> NSAttributedString? {
        guard let htmlBody else { return nil }

        var style: String.HTMLStyle = [
            "body": [
                self.textAlignment
            ]
        ]
        if let linkColor = self.linkColor {
            style["a"] = [
                linkColor,
                "text-decoration: \(self.linkUnderline ? "underline" : "none");"
            ]
            self.linkTextAttributes = [
                .foregroundColor: linkColor
            ]
        }

        let html = htmlBody.embedToHTML(style: style)
        guard let htmlAS = html.htmlAttributedString else { return nil }
        let attributedString = NSMutableAttributedString(attributedString: htmlAS)
        let range = NSRange(location: 0, length: attributedString.length)
        self.commonAttributes.keys.forEach({
            attributedString.removeAttribute($0, range: range)
        })
        return attributedString
    }

    /// Обновление атрибутивной HTML строки с учетом общих атрибутов.
    /// - Parameter attributedText: Исходная атрибутивная HTML строка.
    private func updateAttributedText(_ attributedText: NSAttributedString?) {
        self.isUserInteractionEnabled = attributedText?.description.contains(
            NSAttributedString.Key.link.rawValue
        ) == true

        guard let attributedText
        else {
            self.attributedText = attributedText
            return
        }
        let mutableAT = NSMutableAttributedString(attributedString: attributedText)

        var neededRangeAttributes: [NSRange: Attributes] = [:]

        mutableAT.enumerateAttributes(
            in: .init(location: 0, length: mutableAT.length),
            using: { attributes, range, _ in
                let keys = Set(self.commonAttributes.keys).subtracting(Set(attributes.keys))
                guard keys.isEmpty == false else { return }
                var neededAttributes: Attributes = [:]
                keys.forEach({
                    neededAttributes[$0] = self.commonAttributes[$0]
                })
                neededRangeAttributes[range] = neededAttributes
            }
        )

        neededRangeAttributes.forEach({
            mutableAT.addAttributes($0.value, range: $0.key)
        })
        self.attributedText = mutableAT
    }

    // MARK: - UITextViewDelegate

    open func textView(
        _ textView: UITextView,
        shouldInteractWith URL: URL,
        in characterRange: NSRange,
        interaction: UITextItemInteraction
    ) -> Bool {
        return false
    }

    open func textView(
        _ textView: UITextView,
        shouldInteractWith textAttachment: NSTextAttachment,
        in characterRange: NSRange,
        interaction: UITextItemInteraction
    ) -> Bool {
        return false
    }
}
