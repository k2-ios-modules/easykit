//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

/// Базовое многострочное поле ввода на основе `EasyTextView` с плейсхолдером
/// и возмодностью установки минимального и максимального количества строк
/// для строкллинга текста.
open class MultipleLineTextInput: EasyTextView,
                                  ViewPlaceholdable,
                                  ViewInputable,
                                  UITextViewDelegate {

    // MARK: - variables

    private var isPastedText: Bool = false

    private var heightConstraint: NSLayoutConstraint?

    private var minNumberOfLinesForScrolling: Int = 1

    private var maxNumberOfLinesForScrolling: Int = 1

    // MARK: - properties

    public override var isEnabled: Bool {
        get { super.isEnabled }
        set {
            super.isEnabled = newValue
            self.overlapView.isHidden = newValue
        }
    }

    open override var textInsets: UIEdgeInsets {
        get { super.textInsets }
        set {
            super.textInsets = newValue
            self.placeholderLabel.textInsets = newValue
        }
    }

    open var textInputDelegate: TextInputDelegate?

    open override var font: UIFont? {
        get { super.font }
        set {
            super.font = newValue
            self.updatePlaceholder()
        }
    }

    open override var textAlignment: NSTextAlignment {
        get { super.textAlignment }
        set {
            super.textAlignment = newValue
            self.updatePlaceholder()
        }
    }

    // MARK: ViewPlaceholdable

    public var placeholderDisplay: PlaceholderDisplay = .whenTextIsEmpty

    open var uiPlaceholder: UIText = (
        font: nil,
        color: .gray
    )

    open var placeholderValue: PlaceholderValue? {
        didSet { self.updatePlaceholder() }
    }

    // MARK: - gui

    public private(set) lazy var overlapView = InputOverlapView()

    private lazy var placeholderLabel = EasyLabel()

    // MARK: - init

    open override func initTextView() {
        super.initTextView()

        self.isEnabled = true
        self.delegate = self

        self.setupLayout()
    }

    // MARK: - layout

    private func setupLayout() {
        self.addSubview(self.placeholderLabel)
        self.sendSubviewToBack(self.placeholderLabel)

        NSLayoutConstraint.activate([
            self.placeholderLabel.leftAnchor.constraint(equalTo: self.leftAnchor),
            self.placeholderLabel.topAnchor.constraint(equalTo: self.topAnchor),
            self.placeholderLabel.widthAnchor.constraint(equalTo: self.widthAnchor)
        ])
    }

    open override func didMoveToSuperview() {
        super.didMoveToSuperview()

        self.updateDisabledViewLayout()
    }

    open override func didMoveToWindow() {
        super.didMoveToWindow()

        self.updateHeightConstraint()
    }

    // MARK: - constraints

    public func updateHeightConstraint() {
        self.setNumberOfLines(
            min: self.minNumberOfLinesForScrolling,
            max: self.maxNumberOfLinesForScrolling
        )
    }

    public func setNumberOfLines(min minLines: Int, max maxLines: Int) {
        self.minNumberOfLinesForScrolling = max(minLines, 1)
        self.maxNumberOfLinesForScrolling = max(maxLines, 1)
        self.maxNumberOfLinesForScrolling = max(self.minNumberOfLinesForScrolling, self.maxNumberOfLinesForScrolling)
        guard let font = self.font, (self.minNumberOfLinesForScrolling > 0 || self.maxNumberOfLinesForScrolling > 0)
        else {
            self.isScrollEnabled = false
            self.heightConstraint?.isActive = false
            return
        }

        // for correct sizing when textView is not visible
        if self.window == nil {
            self.sizeToFit()
        }

        let currentNumberOfLines = self.currentNumberOfLines
        let numberOfLines = max(
            self.minNumberOfLinesForScrolling,
            min(self.maxNumberOfLinesForScrolling, currentNumberOfLines)
        )
        let height = CGFloat(numberOfLines) * font.lineHeight + self.textInsets.top + self.textInsets.bottom

        self.isScrollEnabled = numberOfLines >= self.minNumberOfLinesForScrolling

        if let heightConstraint = self.heightConstraint {
            guard heightConstraint.constant != height else { return }
            heightConstraint.constant = height
        } else {
            self.heightConstraint = self.heightAnchor.constraint(equalToConstant: height)
        }
        self.heightConstraint?.isActive = true
        self.layoutIfNeeded()

        if self.isPastedText {
            self.isPastedText = false
            self.textContainer.size = self.frame.size
            self.sizeToFit()
        }
    }

    // MARK: - responder

    @discardableResult
    open override func resignFirstResponder() -> Bool {
        defer {
            self.updatePlaceholder()
        }
        return super.resignFirstResponder()
    }

    @discardableResult
    open override func becomeFirstResponder() -> Bool {
        defer {
            self.updatePlaceholder()
        }
        return super.becomeFirstResponder()
    }

    // MARK: - setters

    public func setAttributedPlaceholder(_ attributed: NSAttributedString?) {
        self.placeholderLabel.setTextValue(.attributed(attributed))
    }

    // MARK: - UITextViewDelegate

    open func textViewDidBeginEditing(_ textView: UITextView) {
        self.textInputDelegate?.textDidBeginEditing?(textView.text ?? "")
        self.updateHeightConstraint()
    }

    open func textViewDidChange(_ textView: UITextView) {
        self.textInputDelegate?.textEditingChanged?(textView.text ?? "")
        self.updateHeightConstraint()
    }

    open func textView(
        _ textView: UITextView,
        shouldChangeTextIn range: NSRange,
        replacementText text: String
    ) -> Bool {
        self.isPastedText = text.count > 1
        return self.textInputDelegate?.text?(
            textView.text ?? "",
            shouldChangeCharactersIn: range,
            replacementString: text
        ) ?? true
    }

    open func textViewDidEndEditing(_ textView: UITextView) {
        self.textInputDelegate?.textDidEndEditing?(textView.text ?? "")
        self.updateHeightConstraint()
    }
}
