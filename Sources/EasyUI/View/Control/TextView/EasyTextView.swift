//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import EasyFoundation
import UIKit

/// Базовое многострочное поле ввода на основе `UITextView` с предустановленным состоянием
/// для программной верстки`translatesAutoresizingMaskIntoConstraints = false`
/// и некоторыми настройками макета.
open class EasyTextView: UITextView {

    // MARK: - properties

    /// Хранилище подписок.
    public var cancellables: Set<AnyCancellable> = []

    /// Внутренние оступы текста внутри `UITextView`.
    ///
    /*
         EasyTextView
         +----------------------------------------------------+
         |                        ↑                           |
         |   .UITextLayoutCanvasView                          |
         |   +--------------------------------------------+   |
         | ← |text text text text                         | → |
         |   +--------------------------------------------+   |
         |                        ↓                           |
         +----------------------------------------------------+
    */
    public var textInsets: UIEdgeInsets {
        get { self.textContainerInset }
        set { self.textContainerInset = newValue }
    }

    open override var text: String! {
        get { super.text }
        set {
            super.text = newValue
            self.applyParagraphStyle(for: newValue)
        }
    }

    open override var font: UIFont? {
        get { super.font }
        set {
            super.font = newValue
            self.applyParagraphStyle(for: self.text)
        }
    }

    open override var textColor: UIColor? {
        get { super.textColor }
        set {
            super.textColor = newValue
            self.applyParagraphStyle(for: self.text)
        }
    }

    open override var textAlignment: NSTextAlignment {
        get { super.textAlignment }
        set {
            super.textAlignment = newValue
            self.applyParagraphStyle(for: self.text)
        }
    }

    // MARK: - init

    public required init() {
        super.init(frame: .zero, textContainer: nil)
        self.initTextView()
    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.initTextView()
    }

    /// Метод инициализации поля ввода, вызывается в конструкторе.
    open func initTextView() {
        self.prepareForAutolayout()
        self.sizeToFit()
        self.isScrollEnabled = false
        self.textContainer.lineFragmentPadding = 0

        self.textInsets = .zero
    }
}

