//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import EasyFoundation
import UIKit

open class EasyTextField: UITextField {

    // MARK: - properties

    public var cancellables: Set<AnyCancellable> = []

    public var textInsets: UIEdgeInsets = .zero {
        didSet { self.setNeedsDisplay() }
    }

    // MARK: - init

    public required init() {
        super.init(frame: .zero)

        self.initTextField()
    }

    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func initTextField() {
        self.prepareForAutolayout()
        self.autocorrectionType = .no
        self.leftViewMode = .never
        self.rightViewMode = .never

        self.textInsets = .zero
    }

    // MARK: - layout

    private func makeTextRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: self.textInsets)
    }

    open override func textRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.textRect(forBounds: bounds)
        return self.makeTextRect(forBounds: rect)
    }

    open override func editingRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.editingRect(forBounds: bounds)
        return self.makeTextRect(forBounds: rect)
    }

    open override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        let rect = super.placeholderRect(forBounds: bounds)
        return self.makeTextRect(forBounds: rect)
    }

    open override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: self.textInsets))
    }
}

