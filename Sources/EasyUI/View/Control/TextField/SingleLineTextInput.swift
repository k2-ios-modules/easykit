//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

open class SingleLineTextInput: EasyTextField,
                                ViewPlaceholdable,
                                ViewInputable {

    // MARK: - properties

    public var textInputDelegate: TextInputDelegate?

    public override var isEnabled: Bool {
        get { super.isEnabled }
        set {
            super.isEnabled = newValue
            self.overlapView.isHidden = newValue
        }
    }

    open override var textAlignment: NSTextAlignment {
        get { super.textAlignment }
        set {
            super.textAlignment = newValue
            self.updatePlaceholder()
        }
    }

    open override var font: UIFont? {
        get { super.font }
        set {
            super.font = newValue
            self.updatePlaceholder()
        }
    }

    // MARK: ViewPlaceholdable

    public var placeholderDisplay: PlaceholderDisplay = .whenTextIsEmpty

    open var uiPlaceholder: UIText = (
        font: nil,
        color: .gray
    )

    open var placeholderValue: PlaceholderValue? {
        didSet { self.updatePlaceholder() }
    }

    // MARK: - gui

    public private(set) lazy var overlapView = InputOverlapView()

    // MARK: - init

    open override func initTextField() {
        super.initTextField()

        self.isEnabled = true
        self.delegate = self
        self.addTarget(
            self,
            action: #selector(self.didBeginEditingAction),
            for: .editingDidBegin
        )
        self.addTarget(
            self,
            action: #selector(self.editingChangedAction),
            for: .editingChanged
        )
        self.addTarget(
            self,
            action: #selector(self.didEndEditingAction),
            for: .editingDidEnd
        )
    }

    // MARK: - layout

    open override func didMoveToSuperview() {
        super.didMoveToSuperview()

        self.updateDisabledViewLayout()
    }

    // MARK: - responder

    @discardableResult
    open override func resignFirstResponder() -> Bool {
        defer {
            self.updatePlaceholder()
        }
        return super.resignFirstResponder()
    }

    @discardableResult
    open override func becomeFirstResponder() -> Bool {
        defer {
            self.updatePlaceholder()
        }
        return super.becomeFirstResponder()
    }

    // MARK: - setters

    public func setAttributedPlaceholder(_ attributed: NSAttributedString?) {
        self.attributedPlaceholder = attributed
    }

    // MARK: - actions

    @objc
    open func didBeginEditingAction(_ textFiled: UITextField) {
        self.textInputDelegate?.textDidBeginEditing?(textFiled.text ?? "")
    }

    @objc
    open func editingChangedAction(_ textFiled: UITextField) {
        self.textInputDelegate?.textEditingChanged?(textFiled.text ?? "")
    }

    @objc
    open func didEndEditingAction(_ textFiled: UITextField) {
        self.textInputDelegate?.textDidEndEditing?(textFiled.text ?? "")
    }
}

// MARK: - UITextFieldDelegate

extension SingleLineTextInput: UITextFieldDelegate {
    open func textField(
        _ textFiled: UITextField,
        shouldChangeCharactersIn range: NSRange,
        replacementString string: String
    ) -> Bool {
        self.textInputDelegate?.text?(
            textFiled.text ?? "",
            shouldChangeCharactersIn: range,
            replacementString: string
        ) ?? true
    }
}

