//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import UIKit

public protocol ViewInputable: ViewTextable {
    var textInputDelegate: TextInputDelegate? { get set }

    var overlapView: InputOverlapView { get }

    init()
}

public extension ViewInputable {
    func setDisabledTapAction(_ completion: (() -> Void)?) {
        self.overlapView.setTapCompletion(completion)
    }

    func updateDisabledViewLayout() {
        self.overlapView.removeConstraints(self.overlapView.constraints)
        self.overlapView.removeFromSuperview()

        guard let view = self.superview else { return }

        view.addSubview(self.overlapView)
        view.bringSubviewToFront(self.overlapView)

        NSLayoutConstraint.activate([
            self.overlapView.leftAnchor.constraint(equalTo: view.leftAnchor),
            self.overlapView.topAnchor.constraint(equalTo: view.topAnchor),
            self.overlapView.rightAnchor.constraint(equalTo: view.rightAnchor),
            self.overlapView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}
