//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyFoundation
import Foundation

public final class InputOverlapView: EasyView,
                                     TapGestureRecognizerable {

    // MARK: - variables

    public private(set) lazy var tapGestureRecognizer = TapGestureRecognizer()
}
