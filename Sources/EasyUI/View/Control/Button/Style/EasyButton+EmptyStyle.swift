//
//  Created on 06.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

public extension EasyButton.Style {
    static var empty: Self {
        .init(
            normal: .emptyNormal,
            disabled: .emptyDisabled,
            highlighted: .emptyHighlighted
        )
    }
}

extension EasyButton.Config {
    private static func emptyBase(titleColor: UIColor?) -> Self {
        .init(
            background: .color(.gray),
            border: EasyButton.Border(width: 1, color: .black),
            title: EasyButton.Title(color: titleColor),
            cornerRadius: 10,
            contentHeight: 56
        )
    }

    fileprivate static var emptyNormal: Self {
        .emptyBase(
            titleColor: .black
        )
    }

    fileprivate static var emptyDisabled: Self {
        .emptyBase(
            titleColor: .black.withAlphaComponent(0.7)
        )
    }

    fileprivate static var emptyHighlighted: Self {
        .emptyBase(
            titleColor: .black.withAlphaComponent(0.7)
        )
    }
}
