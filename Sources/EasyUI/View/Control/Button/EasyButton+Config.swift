//
//  Created on 06.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

public extension EasyButton {
    /// Конфигурация стиля кнопки.
    struct Config: Equatable {
        /// Тип фона.
        let background: Background

        /// Параметры оконтовки.
        let border: Border

        /// Параметры тени.
        let shadow: Shadow?

        /// Параметры текста (заголовка).
        let title: Title

        /// Параметры изображения.
        let image: Image

        /// Параметры загругления.
        let cornerRadius: CornerRadius

        /// Значение высоты.
        let contentHeight: CGFloat?

        /// Внешние отступы контента.
        let contentInsets: UIEdgeInsets

        @available(iOS, deprecated, message: "Используйте инициализатор init(background: Background, border: Border, shadow: Shadow?, title: Title, image: Image, cornerRadius: CornerRadius, contentHeight: CGFloat?, contentInsets: UIEdgeInsets)")
        public init(
            background: Background,
            borderWidth: CGFloat = 0,
            borderColor: UIColor? = nil,
            shadow: Shadow? = nil,
            titleColor: UIColor? = nil,
            titleFont: UIFont? = nil,
            titleAlignment: NSTextAlignment = .center,
            numberOfLines: Int = 0,
            image: UIImage? = nil,
            imagePosition: ImagePosition = .leftOfTitle,
            imagePadding: CGFloat = 0,
            cornerRadius: CornerRadius = 0,
            contentHeight: CGFloat? = nil,
            contentInsets: UIEdgeInsets = .zero
            // do not forget to extend builders below
        ) {
            self.background = background
            self.border = Border(width: borderWidth, color: borderColor)
            self.shadow = shadow
            self.title = Title(color: titleColor, font: titleFont, alignment: titleAlignment, numberOfLines: numberOfLines)
            self.image = Image(image: image, position: imagePosition, imagePadding: imagePadding)
            self.cornerRadius = cornerRadius
            self.contentHeight = contentHeight
            self.contentInsets = contentInsets
        }

        public init(
            background: Background,
            border: Border = .default,
            shadow: Shadow? = .default,
            title: Title = .default,
            image: Image = .default,
            cornerRadius: CornerRadius = .default,
            contentHeight: CGFloat? = nil,
            contentInsets: UIEdgeInsets = .zero
            // do not forget to extend builders below
        ) {
            self.background = background
            self.border = border
            self.shadow = shadow
            self.title = title
            self.image = image
            self.cornerRadius = cornerRadius
            self.contentHeight = contentHeight
            self.contentInsets = contentInsets
        }
    }
}

public extension EasyButton.Config {
    typealias Image = EasyButton.Image
    typealias ImagePosition = EasyButton.ImagePosition

    func withImage(_ image: UIImage?, position: ImagePosition?, imagePadding: CGFloat?) -> Self {
        .init(
            background: background,
            border: border,
            shadow: shadow,
            title: title,
            image: Image(
                image: image ?? self.image.image,
                position: position ?? self.image.position,
                imagePadding: imagePadding ?? self.image.imagePadding
            ),
            cornerRadius: cornerRadius,
            contentHeight: contentHeight,
            contentInsets: contentInsets
        )
    }
}

public extension EasyButton.Config {
    var withoutContentHeight: Self {
        .init(
            background: background,
            border: border,
            shadow: shadow,
            title: title,
            image: image,
            cornerRadius: cornerRadius,
            contentHeight: nil,
            contentInsets: contentInsets
        )
    }
}
