//
//  Created on 19.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

extension EasyButton {
    /// Тип конфигурации загругления кнопки.
    public enum CornerRadius: Equatable,
                              ExpressibleByIntegerLiteral,
                              ExpressibleByFloatLiteral {
        public static let rounded: Self = .heightPercent(0.5)

        /// Фиксированное значение.
        case fixed(CGFloat)

        /// Процентное значение от высоты.
        case heightPercent(CGFloat)

        public static var `default`: Self { 0 }

        public init(integerLiteral value: IntegerLiteralType) {
            self = .fixed(CGFloat(value))
        }

        public init(floatLiteral value: FloatLiteralType) {
            self = .fixed(CGFloat(value))
        }
    }

    /// Установить закругление кнопки.
    /// - Parameter value: Тип закругления.
    func setCornerRadius(_ value: CornerRadius) {
        switch value {
        case .fixed(let value):
            self.cornerRadius = value

        case .heightPercent(let value):
            self.cornerRadius = self.bounds.height * value
        }
    }
}
