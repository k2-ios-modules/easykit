//
//  Created on 19.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

extension EasyButton {
    /// Модель тени кнопки.
    public struct Shadow: Equatable {
        /// Цвет тени
        let color: UIColor?

        /// Относительное положение тени.
        let offset: CGSize

        /// Непрозрачность тени.
        let opacity: Float

        /// Радиус размытия по краям тени, отбрасываемое светом.
        let radius: CGFloat

        /// Форма тени.
        let path: CGPath?

        public static var `default`: Self? { nil }

        public init(
            color: UIColor?,
            offset: CGSize,
            opacity: Float,
            radius: CGFloat,
            path: CGPath?
        ) {
            self.color = color
            self.offset = offset
            self.opacity = opacity
            self.radius = radius
            self.path = path
        }
    }
    
    /// Установить тень для кнопки.
    /// - Parameter shadow: Модель тени.
    func setShadow(_ shadow: Shadow?) {
        if let shadow {
            self.layer.shadowColor = shadow.color?.cgColor
            self.layer.shadowOffset = shadow.offset
            self.layer.shadowOpacity = shadow.opacity
            self.layer.shadowRadius = shadow.radius
            self.layer.shadowPath = shadow.path
        } else {
            self.layer.shadowOpacity = 0
        }
    }
}
