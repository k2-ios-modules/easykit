//
//  Created on 19.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyFoundation
import UIKit

extension EasyButton {
    /// Тип конфигурации фона кнопки.
    public enum Background {
        case color(UIColor?)
        case gradient(CAGradientLayer)
    }

    /// Установить цвет фона кнопки.
    /// - Parameter background: Тип фона
    func setBackgroundColor(_ background: Background) {
        if case let .color(color) = background {
            self.backgroundColor = color
        } else {
            self.backgroundColor = nil
        }
    }

    /// Установить градиент фона кнопки.
    /// - Parameter background: Тип фона
    func setBackgroundGradient(_ background: Background) {
        self.removeIdentifierLayers()
        if case let .gradient(gradientLayer) = background {
            self.setIdentifierLayer(gradientLayer)
        }
    }
}

extension EasyButton.Background: Equatable {
    public static func == (lhs: EasyButton.Background, rhs: EasyButton.Background) -> Bool {
        switch (lhs, rhs) {
        case let (.color(lhsColor), .color(rhsColor)):
            return lhsColor == rhsColor

        case let (.gradient(lhsGradient), .gradient(rhsGradient)):
            return lhsGradient.type == rhsGradient.type
            && lhsGradient.startPoint == rhsGradient.startPoint
            && lhsGradient.endPoint == rhsGradient.endPoint
            && (lhsGradient.colors as? [CGColor] ?? []) == (rhsGradient.colors as? [CGColor] ?? [])
            && lhsGradient.locations == rhsGradient.locations

        default:
            return false
        }
    }
}
