//
//  Created on 06.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import EasyFoundation
import UIKit

/// Простая кнопка на основе `UIButton` с предустановленным состоянием
/// для программной верстки`translatesAutoresizingMaskIntoConstraints = false`.
open class EasyButton: UIButton {

    // MARK: - variables

    public private(set) var heightConstraint: NSLayoutConstraint?

    public var cancellables: Set<AnyCancellable> = []

    // MARK: - properties

    private lazy var style: Style = .empty

    /// Текущая конфигурация кнопвки в зависимости от состояний `isEnabled` и `isHighlighted`.
    public var config: Config {
        switch (isEnabled, isHighlighted) {
        case (true, true):
            return self.style.highlighted
        case (true, false):
            return self.style.normal
        case (false, _):
            return self.style.disabled
        }
    }

    open override var isHighlighted: Bool {
        didSet { self.setStateStyle() }
    }

    open override var isSelected: Bool {
        didSet { self.setStateStyle() }
    }

    open override var isEnabled: Bool {
        didSet { self.setStateStyle() }
    }

    // MARK: - initialization

    public init() {
        super.init(frame: .zero)
        self.initButton()
    }

    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// Метод инициализации кнопки, вызывается в конструкторе.
    open func initButton() {
        if #available(iOS 15.0, *) {
            self.configuration = nil
        }

        self.prepareForAutolayout()
        self.heightConstraint = self.heightAnchor.constraint(equalToConstant: 0)
        self.heightConstraint?.priority = .init(999)
        self.heightConstraint?.isActive = false

        self.tintColor = .clear
        self.setStyle(self.style)
    }

    // MARK: - layout

    open override func layoutSubviews() {
        super.layoutSubviews()

        let config = self.config

        self.setBackgroundGradient(config.background)

        if case .heightPercent = config.cornerRadius {
            self.setCornerRadius(config.cornerRadius)
        }

        self.setContentInsets(config.contentInsets, image: config.image)
    }

    // MARK: - setters

    public func setStyle(_ style: Style) {
        self.style = style
        self.setTitleColor(style: style)
        self.setImage(style: style)
        self.setStateStyle()
    }

    private func setStateStyle() {
        let config = self.config

        self.setTitleConfig(config.title)
        self.setBackgroundColor(config.background)
        self.setSemanticContentAttribute(imagePosition: config.image.position)
        self.setContentHorizontalAlignment(imagePosition: config.image.position)
        self.setBorder(config.border)
        self.setShadow(config.shadow)
        self.setCornerRadius(config.cornerRadius)
        self.setContentHeight(config.contentHeight)
        self.setContentInsets(config.contentInsets, image: config.image)
    }
}
