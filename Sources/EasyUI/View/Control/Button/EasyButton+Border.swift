//
//  Created on 19.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

extension EasyButton {
    /// Модель конфигурации оконтовки кнопки.
    public struct Border: Equatable {
        /// Ширина оконтовки.
        let width: CGFloat

        /// Цвет оконтовки.
        let color: UIColor?

        public static var `default`: Self { .init() }

        public init(
            width: CGFloat = 0,
            color: UIColor? = nil
        ) {
            self.width = width
            self.color = color
        }
    }

    /// Установить оконтовку для кнопки
    /// - Parameter border: Модель оконтовки.
    func setBorder(_ border: Border) {
        self.layer.borderWidth = border.width
        self.layer.borderColor = border.color?.cgColor
    }
}
