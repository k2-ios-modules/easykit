//
//  Created on 19.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

extension EasyButton {
    /// Модель текста (заголовка) кнопки.
    public struct Title: Equatable {
        /// Цвет текста.
        let color: UIColor?

        /// Шрифт.
        let font: UIFont?

        /// Выравнивание текста.
        let alignment: NSTextAlignment

        /// Количество отображаемых строк текста.
        let numberOfLines: Int

        public static var `default`: Self { .init() }

        public init(
            color: UIColor? = nil,
            font: UIFont? = nil,
            alignment: NSTextAlignment = .center,
            numberOfLines: Int = 0
        ) {
            self.color = color
            self.font = font
            self.alignment = alignment
            self.numberOfLines = numberOfLines
        }
    }

    /// Установит цвет текста (заголовка) кнопки для всех состояний кнопки.
    /// - Parameter style: Стиль кнопки.
    func setTitleColor(style: Style) {
        self.setTitleColor(style.normal.title.color, for: .normal)
        self.setTitleColor(style.selected.title.color, for: .selected)
        self.setTitleColor(style.disabled.title.color, for: .disabled)
        self.setTitleColor(style.highlighted.title.color, for: .highlighted)
    }

    /// Установить конфигурацию текста (заголовка) кнопки.
    /// - Parameter config: Модель текста (заголовка).
    func setTitleConfig(_ config: Title) {
        self.titleLabel?.font = config.font
        self.titleLabel?.textAlignment = config.alignment
        self.titleLabel?.numberOfLines = config.numberOfLines
    }
}
