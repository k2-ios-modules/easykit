//
//  Created on 19.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

extension EasyButton {

    private var imageWidth: CGFloat {
        self.imageView?.frame.width ?? 0
    }

    private var titleWidth: CGFloat {
        self.titleLabel?.frame.width ?? 0
    }

    private var contentWidth: CGFloat {
        self.imageWidth * 2 + self.titleWidth
    }
    
    /// Установить внешние отступы контента и внешние отступы текста кнопки.
    /// - Parameters:
    ///   - contentInsets: Внешние отступы контента.
    ///   - image: Модель изображения.
    func setContentInsets(
        _ contentInsets: UIEdgeInsets,
        image: Image
    ) {
        var contentEdgeInsets = contentInsets
        var titleEdgeInsets = UIEdgeInsets.zero

        let imagePadding: CGFloat = self.image(for: self.state) != nil ? image.imagePadding : .zero

        switch image.position {
        case .left:
            let availableSpace = self.bounds.inset(by: contentEdgeInsets).width
            let padding = max((availableSpace - self.contentWidth) / 2, imagePadding)
            titleEdgeInsets.left = padding
            titleEdgeInsets.right = padding + (padding < imagePadding * 2 ? self.imageWidth : 0)

        case .right:
            let availableSpace = self.bounds.inset(by: contentEdgeInsets).width
            let padding = max((availableSpace - self.contentWidth) / 2, imagePadding)
            titleEdgeInsets.left = padding + (padding < imagePadding * 2 ? self.imageWidth : 0)
            titleEdgeInsets.right = padding

        case .leftOfTitle:
            contentEdgeInsets.right += imagePadding
            titleEdgeInsets.left = imagePadding
            titleEdgeInsets.right = -imagePadding

        case .rightOfTitle:
            contentEdgeInsets.left += imagePadding
            titleEdgeInsets.left = -imagePadding
            titleEdgeInsets.right = imagePadding
        }

        self.contentEdgeInsets = contentEdgeInsets
        self.titleEdgeInsets = titleEdgeInsets
    }

    /// Установить семантическое описание контента.
    /// - Parameter imagePosition: Положение картинки.
    func setSemanticContentAttribute(
        imagePosition: Config.ImagePosition
    ) {
        switch imagePosition {
        case .left, .leftOfTitle:
            self.semanticContentAttribute = .forceLeftToRight

        case .right, .rightOfTitle:
            self.semanticContentAttribute = .forceRightToLeft
        }
    }

    /// Установить горизонтальное выравнивание контента.
    /// - Parameter imagePosition: Положение картинки.
    func setContentHorizontalAlignment(
        imagePosition: Config.ImagePosition
    ) {
        switch imagePosition {
        case .leftOfTitle, .rightOfTitle:
            self.contentHorizontalAlignment = .center

        case .left:
            self.contentHorizontalAlignment = .left

        case .right:
            self.contentHorizontalAlignment = .right
        }
    }
}
