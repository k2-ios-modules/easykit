//
//  Created on 19.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

extension EasyButton {
    /// Модель конфигурации изображения сборку от текста.
    public struct Image: Equatable {
        /// Изображения.
        let image: UIImage?

        /// Позиция изображения.
        let position: ImagePosition

        /// Отступ изображения сборку от текста кнопки.
        let imagePadding: CGFloat

        public static var `default`: Self { .init() }

        public init(
            image: UIImage? = nil,
            position: ImagePosition = .leftOfTitle,
            imagePadding: CGFloat = 0
        ) {
            self.image = image
            self.position = position
            self.imagePadding = imagePadding
        }
    }
    
    /// Позиция изображения отновительно текста кнопки.
    public enum ImagePosition: Equatable {
        /// Слева от текста, вплотную к левой границе кнопки.
        case left

        /// Справа от текста, вплотную к правой границе кнопки.
        case right

        /// Слева от текста.
        case leftOfTitle

        /// Справа от текста.
        case rightOfTitle
    }

    func setImage(style: Style) {
        self.setImage(style.normal.image.image, for: .normal)
        self.setImage(style.selected.image.image, for: .selected)
        self.setImage(style.disabled.image.image, for: .disabled)
        self.setImage(style.highlighted.image.image, for: .highlighted)
    }
}
