//
//  Created on 19.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

extension EasyButton {
    /// Установить высоту кнопки.
    /// - Parameter value: Значение высоты.
    func setContentHeight(_ value: CGFloat?) {
        if let value = value {
            self.heightConstraint?.constant = value
            self.heightConstraint?.isActive = true
        } else {
            self.heightConstraint?.isActive = false
        }
        self.layoutIfNeeded()
    }
}
