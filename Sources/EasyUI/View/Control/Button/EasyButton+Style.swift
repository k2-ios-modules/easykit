//
//  Created on 06.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

extension EasyButton {
    /// Стиль кнопки для различных состояний кнопки.
    public struct Style: Equatable {
        /// Конфигурация активной кнопки.
        public let normal: Config

        /// Конфигурация выбранной кнопки.
        public let selected: Config

        /// Конфигурация неактивной кнопки.
        public let disabled: Config

        /// Конфигурация выделенной (в фокусе) кнопки.
        public let highlighted: Config

        public init(
            normal: Config,
            selected: Config? = nil,
            disabled: Config? = nil,
            highlighted: Config? = nil
        ) {
            self.normal = normal
            self.selected = selected ?? normal
            self.disabled = disabled ?? normal
            self.highlighted = highlighted ?? normal
        }
    }
}

public extension EasyButton.Style {
    typealias ImagePosition = EasyButton.ImagePosition

    func withImage(_ image: UIImage?, position: ImagePosition? = nil, imagePadding: CGFloat? = nil) -> Self {
        .init(
            normal: normal.withImage(image, position: position, imagePadding: imagePadding),
            selected: selected.withImage(image, position: position, imagePadding: imagePadding),
            disabled: disabled.withImage(image, position: position, imagePadding: imagePadding),
            highlighted: highlighted.withImage(image, position: position, imagePadding: imagePadding)
        )
    }

    func withNormalImage(_ image: UIImage?, position: ImagePosition? = nil, imagePadding: CGFloat? = nil) -> Self {
        .init(
            normal: normal.withImage(image, position: position, imagePadding: imagePadding),
            selected: selected,
            disabled: disabled,
            highlighted: highlighted
        )
    }

    func withSelectedImage(_ image: UIImage?, position: ImagePosition? = nil, imagePadding: CGFloat? = nil) -> Self {
        .init(
            normal: normal,
            selected: selected.withImage(image, position: position, imagePadding: imagePadding),
            disabled: disabled,
            highlighted: highlighted
        )
    }

    func withDisabledImage(_ image: UIImage?, position: ImagePosition? = nil, imagePadding: CGFloat? = nil) -> Self {
        .init(
            normal: normal,
            selected: selected,
            disabled: disabled.withImage(image, position: position, imagePadding: imagePadding),
            highlighted: highlighted
        )
    }

    func withHighlightedImage(_ image: UIImage?, position: ImagePosition? = nil, imagePadding: CGFloat? = nil) -> Self {
        .init(
            normal: normal,
            selected: selected,
            disabled: disabled,
            highlighted: highlighted.withImage(image, position: position, imagePadding: imagePadding)
        )
    }
}

extension EasyButton.Style {
    public var withoutContentHeight: Self {
        .init(
            normal: normal.withoutContentHeight,
            selected: selected.withoutContentHeight,
            disabled: disabled.withoutContentHeight,
            highlighted: highlighted.withoutContentHeight
        )
    }
}
