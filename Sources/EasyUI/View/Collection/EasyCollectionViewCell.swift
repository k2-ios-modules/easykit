//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyFoundation
import UIKit

/// Простая вью ячейки коллекции на основе `UICollectionViewCell` с предустановленным состоянием
/// для программной верстки`translatesAutoresizingMaskIntoConstraints = false`.
///
/// Фон сброшен по-умолчанию.
open class EasyCollectionViewCell: UICollectionViewCell {

    // MARK: - init

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initView()
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    open func initView() {
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
    }
}
