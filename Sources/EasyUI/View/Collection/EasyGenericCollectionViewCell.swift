//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyFoundation
import UIKit

/// Простая вью ячейки коллекции на основе `EasyCollectionViewCell` с указанным типом вью `T`.
///
/// Переданый тип вью контента `MainView` располагается на `contentView`.
///
/*
    EasyGenericCollectionViewCell
    +----------------------------------------------------+
    |   .contentView                                     |
    |   +--------------------------------------------+   |
    |   |   MainView                                 |   |
    |   |   +------------------------------------+   |   |
    |   |   |                                    |   |   |
    |   |   |                                    |   |   |
    |   |   |                                    |   |   |
    |   |   |                                    |   |   |
    |   |   |                                    |   |   |
    |   |   +------------------------------------+   |   |
    |   |                                            |   |
    |   +--------------------------------------------+   |
    |                                                    |
    +----------------------------------------------------+
 */
open class EasyGenericCollectionViewCell<T: UIView>: EasyCollectionViewCell,
                                                     GenericContentViewable {

    public typealias MainView = T

    // MARK: - gui

    public private(set) lazy var mainView = MainView()

    // MARK: - init

    public override func initView() {
        super.initView()

        self.setupLayout()
    }
}
