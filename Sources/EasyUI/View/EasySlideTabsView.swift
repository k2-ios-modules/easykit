//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import EasyFoundation
import UIKit

open class EasySlideTabsView<
    SlideTabView: UIView
>: EasyView {

    // MARK: - subtypes

    public typealias SlideTabView = SlideTabView

    // MARK: - variables

    private var previousIndex: Int = 0

    private var changingBoundsToken: AnyCancellable?

    private var containerStackWidthConstraint: NSLayoutConstraint?

    public var slideTabs: [SlideTabView] {
        self.containerStackView.arrangedSubviews.compactMap({
            $0 as? SlideTabView
        })
    }

    // MARK: - properties

    public var animationDuration: TimeInterval = 0.3

    public var contentInset: UIEdgeInsets = .zero {
        didSet {
            let inset = self.contentInset
            self.scrollView.contentInset = UIEdgeInsets(top: 0, left: inset.left, bottom: 0, right: inset.right)
            self.containerStackView.paddingInsets = UIEdgeInsets(top: inset.top, left: 0, bottom: inset.bottom, right: 0)
            self.containerStackWidthConstraint?.constant = -(inset.left + inset.right)
        }
    }

    public var spacing: CGFloat {
        get { self.containerStackView.spacing }
        set { self.containerStackView.spacing = newValue }
    }

    // MARK: - gui

    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.showsHorizontalScrollIndicator = false
        return scrollView.prepareForAutolayout()
    }()

    private lazy var containerStackView: EasyStackView = {
        let stack = EasyStackView()
        stack.axis = .horizontal
        stack.alignment = .fill
        return stack
    }()

    public private(set) lazy var selectorLayer = CALayer()

    // MARK: - init

    open override func initView() {
        super.initView()

        self.setupLayout()

        self.spacing = 10
        self.setScrollState(.disabled(.fill))
    }

    // MARK: - layout

    private func setupLayout() {
        self.addSubview(self.scrollView)
        self.scrollView.layer.addSublayer(self.selectorLayer)
        self.scrollView.addSubview(self.containerStackView)

        NSLayoutConstraint.activate([
            self.scrollView.leftAnchor.constraint(equalTo: self.leftAnchor),
            self.scrollView.topAnchor.constraint(equalTo: self.topAnchor),
            self.scrollView.rightAnchor.constraint(equalTo: self.rightAnchor),
            self.scrollView.bottomAnchor.constraint(equalTo: self.bottomAnchor),

            self.containerStackView.leftAnchor.constraint(equalTo: self.scrollView.leftAnchor),
            self.containerStackView.topAnchor.constraint(equalTo: self.scrollView.topAnchor),
            self.containerStackView.rightAnchor.constraint(equalTo: self.scrollView.rightAnchor),
            self.containerStackView.bottomAnchor.constraint(equalTo: self.scrollView.bottomAnchor),
            self.containerStackView.heightAnchor.constraint(equalTo: self.scrollView.heightAnchor)
        ])

        self.containerStackWidthConstraint = self.containerStackView.widthAnchor.constraint(equalTo: self.scrollView.widthAnchor)
    }

    public func addSubviewToScrollView(_ subview: UIView) {
        subview.prepareForAutolayout()
        self.scrollView.addSubview(subview)
    }

    // MARK: - setters

    public func setScrollState(_ state: ScrollState) {
        self.scrollView.isScrollEnabled = state.isEnabled
        self.containerStackWidthConstraint?.isActive = state.isEnabled == false
        self.containerStackView.distribution = state.distribution
    }

    public func setTabs(_ list: [SlideTabView]) {
        self.containerStackView.clearArrangedSubviews()
        self.containerStackView.addArrangedSubviews(list)
        self.setNeedsLayout()
    }

    // MARK: - animation

    public func setSelectedSlideTab(by index: Int) {
        guard let targetTab = self.slideTabs[safe: index],
              let previousTab = self.slideTabs[safe: self.previousIndex]
        else { return }

        self.layoutIfNeeded()

        let animationDisabled = self.window == nil || self.selectorLayer.frame.isEmpty

        CATransaction.begin()
        CATransaction.setDisableActions(animationDisabled)

        UIView.animate(
            withDuration: self.animationDuration,
            delay: 0.0,
            usingSpringWithDamping: 1.0,
            initialSpringVelocity: 0.5,
            options: .curveEaseIn,
            animations: {
                let frame = targetTab.frame
                self.setSelectorFrame(from: frame)
                self.scrollView.scrollRectToVisible(frame, animated: false)
            }
        )

        CATransaction.commit()

        self.updateTab(previousIndex: self.previousIndex, selectedIndex: index)
        self.previousIndex = index

        self.changingBoundsToken?.cancel()
        self.changingBoundsToken = targetTab.publisher(for: \.bounds)
            .receive(on: DispatchQueue.main)
            .removeDuplicates()
            .sink { [weak self] _ in
                guard let self else { return }

                UIView.transition(
                    with: targetTab,
                    duration: self.animationDuration / 2,
                    options: [.transitionCrossDissolve, .curveEaseIn],
                    animations: {
                        self.setSelectorFrame(from: targetTab.frame)
                    }
                )
            }
    }

    // MARK: - selection

    private func setSelectorFrame(from targetFrame: CGRect) {
        let selectorFrame = self.getSelectorFrame(from: targetFrame)
        self.selectorLayer.frame = selectorFrame
    }

    open func getSelectorFrame(from targetFrame: CGRect) -> CGRect {
        targetFrame
    }

    open func updateTab(
        previousIndex: Int,
        selectedIndex: Int
    ) { }
}

public extension EasySlideTabsView {
    struct ScrollState {
        let isEnabled: Bool
        let distribution: UIStackView.Distribution

        private init(isEnabled: Bool, distribution: UIStackView.Distribution) {
            self.isEnabled = isEnabled
            self.distribution = distribution
        }

        public static func disabled(_ distribution: UIStackView.Distribution) -> Self {
            Self.init(isEnabled: false, distribution: distribution)
        }

        public static func enabled(_ distribution: UIStackView.Distribution) -> Self {
            Self.init(isEnabled: true, distribution: distribution)
        }

        public static var enabled: Self {
            self.enabled(.fillProportionally)
        }
    }
}

