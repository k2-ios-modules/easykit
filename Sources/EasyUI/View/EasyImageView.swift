//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import EasyFoundation
import UIKit

/// Простая вью картинки на основе `UIImageView` с предустановленным состоянием
/// для программной верстки`translatesAutoresizingMaskIntoConstraints = false`.
/// и некоторыми настройками макета.
///
/// Вью имеет жеста нажатия `TapGestureRecognizerable`, действие устанавливается с помощью метода `setTapCompletion`.
open class EasyImageView: UIImageView,
                          TapGestureRecognizerable {

    // MARK: - variables

    private var widthConstraint: NSLayoutConstraint?
    private var heightConstraint: NSLayoutConstraint?

    /// Жест нажатия на вью, по-умолчанию неактивен и не добавлен на вью.
    public private(set) lazy var tapGestureRecognizer = TapGestureRecognizer()

    /// Хранилище подписок.
    public var cancellables: Set<AnyCancellable> = []

    // MARK: - properties

    /// Внутренние оступы у картинки.
    ///
    /*
         EasyImageView
         +----------------------------------------------------+
         |                        ↑                           |
         |   UIImage                                          |
         |   +--------------------------------------------+   |
         |   |                                            |   |
         |   |                                            |   |
         | ← |                                            | → |
         |   |                                            |   |
         |   +--------------------------------------------+   |
         |                        ↓                           |
         +----------------------------------------------------+
    */
    public var imagePaddingInsets: UIEdgeInsets = .zero {
        didSet { self.updateImage() }
    }

    /// Размер картинки `image`.
    public var imageSize: CGSize? {
        didSet { self.updateImage() }
    }

    // MARK: - init

    public init() {
        super.init(frame: .zero)
        self.initImageView()
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.initImageView()
    }

    public override init(image: UIImage?) {
        super.init(image: image)
        self.initImageView()
    }

    public override init(image: UIImage?, highlightedImage: UIImage?) {
        super.init(image: image, highlightedImage: highlightedImage)
        self.initImageView()
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// Метод инициализации вью, вызывается в конструкторе.
    open func initImageView() {
        self.prepareForAutolayout()
    }

    // MARK: - constraints

    /// Установка ограничение размеро вью.
    /// При передаче `nil`, ограничения деактивируются.
    public func setSizeConstraints(_ size: CGSize?) {
        guard let size else {
            self.widthConstraint?.isActive = false
            self.heightConstraint?.isActive = false
            return
        }
        if let widthConstraint = self.widthConstraint,
           let heightConstraint = self.heightConstraint {
            widthConstraint.constant = size.width
            heightConstraint.constant = size.height
        } else {
            self.widthConstraint = self.widthAnchor.constraint(equalToConstant: size.width)
            self.heightConstraint = self.heightAnchor.constraint(equalToConstant: size.height)
        }
        self.widthConstraint?.isActive = true
        self.heightConstraint?.isActive = true
    }

    // MARK: - makers

    /// Обновление размеров `image` для не нулевого размера.
    public func updateImageSize() {
        guard let imageSize = self.imageSize,
              imageSize != .zero else { return }

        self.image = self.image?.withSize(imageSize)
    }

    /// Обновление отступов `image` для не нулевых отступов.
    public func updateImageInsets() {
        guard self.imagePaddingInsets != .zero else { return }

        self.image = self.image?.withInsets(self.imagePaddingInsets)
    }

    /// Обновление `image`.
    /// Включает обновлени размеров и отступов.
    public func updateImage() {
        self.updateImageSize()
        self.updateImageInsets()
    }
}
