//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import EasyFoundation
import UIKit

/// Простая вью на основе `UIView` с предустановленным состоянием
/// для программной верстки`translatesAutoresizingMaskIntoConstraints = false`.
open class EasyView: UIView {

    // MARK: - properties

    /// Хранилище подписок.
    public var cancellables: Set<AnyCancellable> = []

    // MARK: - init

    public init() {
        super.init(frame: .zero)
        self.initView()
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.initView()
    }

    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// Метод инициализации вью, вызывается в конструкторе.
    open func initView() {
        self.prepareForAutolayout()
    }
}
