//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//

import Combine
import EasyFoundation
import UIKit

/// Простой лейбл на основе `UILabel` с предустановленным состоянием
/// для программной верстки`translatesAutoresizingMaskIntoConstraints = false`
/// и некоторыми настройками макета.
///
/// Лейбл имеет жеста нажатия `TapGestureRecognizerable`, действие устанавливается с помощью метода `setTapCompletion`.
open class EasyLabel: UILabel,
                      TapGestureRecognizerable {

    // MARK: - properties

    /// Жест нажатия на лейбл, по-умолчанию неактивен и не добавлен на лейбл.
    public private(set) lazy var tapGestureRecognizer = TapGestureRecognizer()

    /// Хранилище подписок.
    public var cancellables: Set<AnyCancellable> = []

    /// Внутренние оступы у лейбла.
    ///
    /*
         EasyLabel.superview
         +----------------------------------------------------+
         |                        ↑                           |
         |   EasyLabel                                        |
         |   +--------------------------------------------+   |
         |   |                                            |   |
         |   |     text text text text                    |   |
         | ← |                                            | → |
         |   |                                            |   |
         |   +--------------------------------------------+   |
         |                        ↓                           |
         +----------------------------------------------------+
    */
    public var textInsets: UIEdgeInsets = .zero {
        didSet { self.invalidateIntrinsicContentSize() }
    }

    open override var text: String? {
        get { super.text }
        set {
            super.text = newValue
            self.applyParagraphStyle(for: newValue)
        }
    }

    open override var font: UIFont! {
        get { super.font }
        set {
            super.font = newValue
            self.applyParagraphStyle(for: self.text)
        }
    }

    open override var textColor: UIColor! {
        get { super.textColor }
        set {
            super.textColor = newValue
            self.applyParagraphStyle(for: self.text)
        }
    }

    open override var textAlignment: NSTextAlignment {
        get { super.textAlignment }
        set {
            super.textAlignment = newValue
            self.applyParagraphStyle(for: self.text)
        }
    }

    // MARK: - init

    public init() {
        super.init(frame: .zero)
        self.initLabel()
    }

    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.initLabel()
    }

    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// Метод инициализации лейбла, вызывается в конструкторе.
    open func initLabel() {
        self.prepareForAutolayout()
        self.numberOfLines = 0
    }

    // MARK: - layout

    open override func textRect(
        forBounds bounds: CGRect,
        limitedToNumberOfLines numberOfLines: Int
    ) -> CGRect {
        guard self.textInsets != .zero else {
            return super.textRect(
                forBounds: bounds,
                limitedToNumberOfLines: numberOfLines
            )
        }
        let insetRect = bounds.inset(by: self.textInsets)
        let textRect = super.textRect(
            forBounds: insetRect,
            limitedToNumberOfLines: self.numberOfLines
        )
        let invertedInsets = UIEdgeInsets(
            top: -self.textInsets.top,
            left: -self.textInsets.left,
            bottom: -self.textInsets.bottom,
            right: -self.textInsets.right
        )
        return textRect.inset(by: invertedInsets)
    }

    open override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: self.textInsets))
    }
}
