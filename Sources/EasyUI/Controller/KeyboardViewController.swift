//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import UIKit

open class KeyboardViewController: UIViewController {

    // MARK: - variables

    private var keyboardCancallebles: Set<AnyCancellable> = []

    private lazy var hideKeyboardGesture = UITapGestureRecognizer(
        target: self,
        action: #selector(self.hideKeyboard)
    )

    public var keyboardNotificationsDebounce: DispatchQueue.SchedulerTimeType.Stride = .milliseconds(0)

    public var keyboardOffset: CGFloat {
        self.keyboardWillShown ? self.keyboardFrame.height : 0.0
    }

    public var actualKeyboardOffset: CGFloat {
        let offset = self.keyboardOffset

        if let tabBar = self.tabBarController?.tabBar,
           tabBar.isHidden == false {
            return max(offset - tabBar.bounds.height, 0)
        } else {
            return max(offset, 0)
        }
    }

    public private(set) var keyboardFrame: CGRect = .zero

    public private(set) var keyboardWillShown: Bool = false

    public private(set) var keyboardShown: Bool = false

    // MARK: - init

    open override func viewDidLoad() {
        super.viewDidLoad()

        self.setKeyboardNotifications()
    }

    // MARK: - notifications

    private func setKeyboardNotifications() {
        NotificationCenter.default
            .publisher(for: UIResponder.keyboardWillShowNotification)
            .debounce(for: self.keyboardNotificationsDebounce, scheduler: DispatchQueue.main)
            .sink { [weak self] in self?.keyboardWillShow(notification: $0) }
            .store(in: &self.keyboardCancallebles)

        NotificationCenter.default
            .publisher(for: UIResponder.keyboardDidShowNotification)
            .debounce(for: self.keyboardNotificationsDebounce, scheduler: DispatchQueue.main)
            .sink { [weak self] in self?.keyboardDidShow(notification: $0) }
            .store(in: &self.keyboardCancallebles)

        NotificationCenter.default
            .publisher(for: UIResponder.keyboardWillHideNotification)
            .debounce(for: self.keyboardNotificationsDebounce, scheduler: DispatchQueue.main)
            .sink { [weak self] in self?.keyboardWillHide(notification: $0) }
            .store(in: &self.keyboardCancallebles)

        NotificationCenter.default
            .publisher(for: UIResponder.keyboardDidHideNotification)
            .debounce(for: self.keyboardNotificationsDebounce, scheduler: DispatchQueue.main)
            .sink { [weak self] in self?.keyboardDidHide(notification: $0) }
            .store(in: &self.keyboardCancallebles)
    }

    // MARK: - keyboard visibility actions

    // MARK: will show

    open func keyboardWillShowAction() { }

    open func keyboardWillShowAnimationAction() { }

    private func keyboardWillShow(notification: Notification) {
        guard self.viewIfLoaded?.window != nil,
              let userInfo = notification.userInfo
        else { return }

        self.keyboardFrame = userInfo.keyboardFrameEnd
        guard self.keyboardWillShown == false else {
            self.keyboardWillShowAnimationAction()
            return
        }

        self.view.addGestureRecognizerIfNeeded(self.hideKeyboardGesture)
        self.keyboardWillShown = true
        self.keyboardWillShowAction()
        UIView.animate(withDuration: userInfo.keyboardAnimationDuration) { [weak self] in
            guard let self else { return }
            self.keyboardWillShowAnimationAction()
            self.view.layoutIfNeeded()
        }
    }

    // MARK: did show

    open func keyboardDidShowAction() { }

    private func keyboardDidShow(notification: Notification) {
        self.keyboardShown = true
        self.keyboardDidShowAction()
    }

    // MARK: will hide

    open func keyboardWillHideAction() { }

    open func keyboardWillHideAnimationAction() { }

    private func keyboardWillHide(notification: Notification) {
        guard self.viewIfLoaded?.window != nil,
              let userInfo = notification.userInfo
        else { return }

        self.keyboardWillShown = false
        self.keyboardWillHideAction()
        self.view.removeGestureRecognizer(self.hideKeyboardGesture)
        UIView.animate(withDuration: userInfo.keyboardAnimationDuration) { [weak self] in
            guard let self else { return }
            self.keyboardWillHideAnimationAction()
            self.view.layoutIfNeeded()
        }
    }

    // MARK: did hide

    open func keyboardDidHideAction() { }

    private func keyboardDidHide(notification: Notification) {
        self.keyboardShown = false
        self.keyboardDidHideAction()
    }

    @objc
    open func hideKeyboard() {
        self.view.endEditing(true)
    }
}

private extension Dictionary where Key == AnyHashable,
                                   Value == Any {
    var keyboardFrameBegin: CGRect {
        let nsValue = self[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue
        return nsValue?.cgRectValue ?? .zero
    }    

    var keyboardFrameEnd: CGRect {
        let nsValue = self[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        return nsValue?.cgRectValue ?? .zero
    }

    var keyboardAnimationDuration: TimeInterval {
        let nsNumber = self[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber
        return nsNumber?.doubleValue ?? 0
    }
}
