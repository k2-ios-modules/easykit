//
//  Created on 08.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//

import Combine
import EasyFoundation
import UIKit

/// Контейнер для отображения другого контроллера (`childVC` ) в качестве нижнего листа.
///
/// Контейре является дочерни контроллером для родительского контроллера.
/// Используейте:
/// - метод `setChild` для установки отображаемого контента;
/// - метод `pinToController` для размещения контейнера на родительском контроллере;
/// - метод `show` для показа контейнера (перемещение к нижней границе), `asyncShow` - для асинхронного показа;
/// - метод `moveToLowerBound` для перемещения контейнера к ниженй границе, `asyncMoveToLowerBound` - для асинхронного перещения;
/// - метод `moveToUpperBound` для перемещения контейнера к вержней границе, `asyncMoveToUpperBound` - для асинхронного перещения;
/// - метод `moveToNearestBound` для перемещения контейнера к ближайщей границе, `asyncMoveToNearestBound` - для асинхронного перещения.
///
/*
     ChildBottomSheetController.parentViewController
     +------------------------------------------------+
     |                                                |
     |                                                |
     |                                                |
     |                                                |
     |                                                |
     |   .lowerBoundOffset                            |
     |   ------------------------------------------   |
     |                        ↑                       |
     |                        |                       |
     |                        |  область перемещения  |
     |                        |  контейнера           |
     |                        |                       |
     |                        |                       |
     |                        |                       |
     |                        |                       |
     |   .upperBoundOffset    ↓                       |
     |   ------------------------------------------   |
     |                                                |
     | ChildBottomSheetController                     |
     | +--------------------------------------------+ |
     | | ChildBottomSheetController.childVC         | |
     | | +----------------------------------------+ | |
     | | |                                        | | |
     | | |                                        | | |
     | | |                                        | | |
     | | |                                        | | |
     | | |                                        | | |
     | | |                                        | | |
     | | |                                        | | |
     | | +----------------------------------------+ | |
     | +--------------------------------------------+ |
     +------------------------------------------------+
*/
open class ChildBottomSheetController: KeyboardViewController,
                                       SwipeableViewController,
                                       UIGestureRecognizerDelegate {

    // MARK: - variables

    private var wasShown: Bool = false

    private var scrollCancallable: AnyCancellable? {
        didSet { oldValue?.cancel() }
    }

    /// Продолжительность анимации скрытия/показа контейнера.
    public var animationDuration: CGFloat = 0.3

    private var isTopPosition: Bool {
        self.currentOffsetY == self.safeLowerBoundOffset
    }

    private var isBottomPosition: Bool {
        self.currentOffsetY == self.safeUpperBoundOffset
    }

    // MARK: anchors

    private var customBottomAnchor: NSLayoutYAxisAnchor?

    private var parentTopAnchor: NSLayoutYAxisAnchor? {
        self.view.superview?.topAnchor
    }

    private var parentBottomAnchor: NSLayoutYAxisAnchor? {
        self.customBottomAnchor ?? self.view.superview?.bottomAnchor
    }

    // MARK: constraints

    private var topConstraint: NSLayoutConstraint?
    private var bottomConstraint: NSLayoutConstraint?

    // MARK: offset

    private var topSafeOffset: CGFloat {
        guard let parentVC = self.parentViewController else { return 0 }
        return parentVC.view.safeAreaInsets.top
    }

    /// lower bound from top side with `topSafeOffset`
    private var safeLowerBoundOffset: CGFloat {
        self.topSafeOffset + self.lowerBoundOffset.value
    }

    /// upper bound from top side with `topSafeOffset`
    private var safeUpperBoundOffset: CGFloat {
        self.topSafeOffset + self.upperBoundOffset.value
    }

    private var currentOffsetY: CGFloat = 0

    @Published
    public private(set) var percentOffset: CGFloat = 0

    /// Нижняя граница от верхней части экрана.
    public var lowerBoundOffset: BoundOffset = .constant(0)

    /// Верхняя граница от верхней части экрана.
    public var upperBoundOffset: BoundOffset = .constant(0)

    // MARK: - gesture

    private lazy var panGestureRecognizer: UIPanGestureRecognizer = {
        let gesture = UIPanGestureRecognizer()
        gesture.delegate = self
        gesture.setTarget(self, action: #selector(self.handlePanGestureRecognizer))
        return gesture
    }()

    // MARK: - gui

    private weak var childVC: SwipeableViewController?

    public var swipeableScrollView: UIScrollView {
        self.childVC?.swipeableScrollView ?? UIScrollView()
    }

    // MARK: - life cicle

    open override func viewDidLoad() {
        super.viewDidLoad()

        self.view.addGestureRecognizer(self.panGestureRecognizer)
    }

    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        self.updatePreferredContentSize()
    }

    // MARK: - layout

    private func setLayoutHiddenState() {
        guard let parentBottomAnchor else { return }

        self.currentOffsetY = 0
        self.view.removeConstraints([
            self.topConstraint,
            self.bottomConstraint
        ])

        self.topConstraint = self.view.topAnchor.constraint(equalTo: parentBottomAnchor)
        self.bottomConstraint = nil

        NSLayoutConstraint.activate([
            self.topConstraint
        ])
    }

    private func setLayoutShownState() {
        guard let parentTopAnchor, let parentBottomAnchor else { return }

        self.view.removeConstraints([
            self.topConstraint,
            self.bottomConstraint
        ])

        self.topConstraint = self.view.topAnchor.constraint(equalTo: parentTopAnchor)
        self.bottomConstraint = self.view.bottomAnchor.constraint(equalTo: parentBottomAnchor)

        NSLayoutConstraint.activate([
            self.topConstraint,
            self.bottomConstraint
        ])
    }

    public func calculatePreferredContentSize() -> CGSize? {
        self.childVC?.calculatePreferredContentSize()
    }

    // MARK: pin methods

    /// Установить дочерний контент-контроллер, непосредственно то, что будет отображено в качестве нижнего листа.
    /// - Parameter vc: Дочерний контроллер.
    public final func setChild(_ vc: SwipeableViewController) {
        self.childVC = vc
        self.removeAllChild()
        self.addChild(vc, to: self.view)
    }

    /// Привязать контейнер на указанный контроллер, как дочерний.
    /// - Important: Контейнер при этом скрыт.
    /// - Parameter parentVC: Родительский контроллер.
    public final func pinToController(
        _ parentVC: UIViewController
    ) {
        self.pinToController(parentVC, bottomAnchor: parentVC.view.bottomAnchor)
    }

    /// Привязать контейнер на указанный контроллер, как дочерний.
    /// - Important: Контейнер при этом скрыт.
    /// - Parameters:
    ///   - parentVC: Родительский контроллер.
    ///   - bottomAnchor: Нижний якорь, ограничиваюбщий дочерний контейнер снизу.
    public final func pinToController(
        _ parentVC: UIViewController,
        bottomAnchor: NSLayoutYAxisAnchor
    ) {
        self.customBottomAnchor = bottomAnchor
        parentVC.addChild(self, to: parentVC.view)

        self.view.prepareForAutolayout()

        self.setLayoutHiddenState()
        NSLayoutConstraint.activate([
            self.view.leftAnchor.constraint(equalTo: parentVC.view.leftAnchor),
            self.view.rightAnchor.constraint(equalTo: parentVC.view.rightAnchor)
        ])
    }

    // MARK: - utils

    public final func offsetIsOnBorder() -> Bool {
        return self.isTopPosition || self.isBottomPosition
    }

    public final func bindScrollContentOffset(withBounceVertical: Bool = true) {
        let enableGesture = self.safeUpperBoundOffset > self.safeLowerBoundOffset
        self.panGestureRecognizer.isEnabled = enableGesture
        self.swipeableScrollView.panGestureRecognizer.removeTarget(self, action: #selector(self.handlePanGestureRecognizer))

        guard enableGesture else { return }

        self.swipeableScrollView.panGestureRecognizer.addTarget(self, action: #selector(self.handlePanGestureRecognizer))

        let contentInsetTop = self.swipeableScrollView.contentInset.top
        self.scrollCancallable = self.swipeableScrollView
            .publisher(for: \.contentOffset)
            .removeDuplicates(by: { $0.y == $1.y })
            .sink { [weak self] contentOffset in
                guard let self else { return }

                let contentOffsetY = contentOffset.y

                if contentOffsetY < 0 {
                    let offset = contentOffsetY + contentInsetTop
                    self.calculateScrollOffset(of: -offset)
                    self.updateTopConstraints()
                } else if self.currentOffsetY <= self.safeUpperBoundOffset,
                          self.currentOffsetY > self.safeLowerBoundOffset {
                    self.swipeableScrollView.contentOffset.y = 0
                    self.calculateScrollOffset(of: -contentOffsetY)
                    self.updateTopConstraints()
                }
            }
        self.swipeableScrollView.alwaysBounceVertical = withBounceVertical
    }

    private func updateTopConstraints() {
        self.topConstraint?.constant = self.currentOffsetY
    }

    private func setCurrentOffsetY(_ value: CGFloat) {
        guard self.currentOffsetY != value else { return }
        self.currentOffsetY = value
        self.percentOffset = (self.currentOffsetY - self.safeLowerBoundOffset)
        / (self.safeUpperBoundOffset - self.safeLowerBoundOffset)
    }

    private func calculateScrollOffset(of pointY: CGFloat) {
        let offset = self.currentOffsetY + pointY
        self.setCurrentOffsetY(max(
            self.safeLowerBoundOffset,
            min(
                max(self.safeLowerBoundOffset, offset),
                min(offset, self.safeUpperBoundOffset)
            )
        ))
    }

    private func updateScrollOffset(
        withAnimation: Bool = true,
        completion: (() -> Void)? = nil
    ) {
        self.setCurrentOffsetY(
            self.percentOffset < 0.5
            ? self.safeLowerBoundOffset
            : self.safeUpperBoundOffset
        )
        self.updateTopConstraints()
        guard withAnimation else {
            completion?()
            return
        }

        UIView.animate(
            withDuration: self.animationDuration,
            animations: {
                self.view.superview?.layoutIfNeeded()
            },
            completion: { _ in
                completion?()
            }
        )
    }

    // MARK: - keyboard visibility methods

    open override func keyboardWillShowAction() {
        guard self.view.firstResponder != nil else { return }

        super.keyboardWillShowAction()
        self.setCurrentOffsetY(self.currentOffsetY - self.keyboardOffset)
        self.updateScrollOffset()
    }

    open override func keyboardWillHideAction() {
        guard self.view.firstResponder != nil else { return }

        self.setCurrentOffsetY(self.currentOffsetY + self.keyboardOffset)
        self.swipeableScrollView.contentOffset.y = max(
            0,
            self.swipeableScrollView.contentOffset.y - self.keyboardOffset
        )

        super.keyboardWillHideAction()

        self.updateScrollOffset()
    }
}

// MARK: - move methods

extension ChildBottomSheetController {

    private func setAndUpdateOffset(
        _ offset: CGFloat,
        withAnimation: Bool = true,
        completion: (() -> Void)?
    ) {
        self.setCurrentOffsetY(offset)
        self.updateScrollOffset(withAnimation: withAnimation, completion: completion)
    }

    private func moveToBound(
        offset: CGFloat,
        completion: (() -> Void)?
    ) {
        guard self.topConstraint != nil else {
            completion?()
            return
        }
        self.setAndUpdateOffset(offset, completion: completion)
    }

    @MainActor
    /// Асинхронно показать контейнер, выдвинуть к нижней границе.
    /// - Parameters:
    ///   - withAnimation: Учитывать анимация отображения.
    ///   - withBounceVertical: Учитывать вертикальный отскок отображаемого контента.
    ///   - withAwaitAnimationFinished: Ожидать окончания завершения анимации отображения.
    public func asyncShow(
        withAnimation: Bool = true,
        withBounceVertical: Bool = true,
        withAwaitAnimationFinished: Bool = true
    ) async {
        await withCheckedContinuation { continuation in
            guard self.wasShown == false else {
                continuation.resume()
                return
            }

            defer {
                self.wasShown = true
            }

            self.bindScrollContentOffset(withBounceVertical: withBounceVertical)

            self.setLayoutShownState()

            self.setAndUpdateOffset(
                self.safeUpperBoundOffset,
                withAnimation: withAnimation,
                completion: withAwaitAnimationFinished ? {
                    continuation.resume()
                } : nil
            )

            if withAwaitAnimationFinished == false {
                continuation.resume()
            }
        }
    }

    /// Показать контейнер на главном потоке, выдвинуть к нижней границе.
    /// - Parameters:
    ///   - withAnimation: Учитывать анимация отображения.
    ///   - withBounceVertical: Учитывать вертикальный отскок отображаемого контента.
    ///   - withAwaitAnimationFinished: Ожидать окончания завершения анимации отображения.
    public func show(
        withAnimation: Bool = true,
        withBounceVertical: Bool = true,
        withAwaitAnimationFinished: Bool = true
    ) {
        Task {
            await self.asyncShow(
                withAnimation: withAnimation,
                withBounceVertical: withBounceVertical,
                withAwaitAnimationFinished: withAwaitAnimationFinished
            )
        }
    }

    @MainActor
    /// Асинхронно скрыть контейнер с анимацией.
    public func asyncHide() async {
        await withCheckedContinuation { continuation in
            let height = self.view.bounds.height
            self.topConstraint?.constant += height
            self.bottomConstraint?.constant += height

            UIView.animate(
                withDuration: self.animationDuration,
                animations: {
                    self.view.superview?.layoutIfNeeded()
                },
                completion: { _ in
                    self.wasShown = false
                    self.setLayoutHiddenState()
                    continuation.resume()
                }
            )
        }
    }

    /// Cкрыть контейнер с анимацией.
    public func hide() {
        Task {
            await self.asyncHide()
        }
    }

    @MainActor
    /// Выполнить перемещение контейнера к нижней границе асинхронно с анимацией.
    public func asyncMoveToLowerBound() async {
        await withCheckedContinuation { continuation in
            self.moveToBound(
                offset: self.safeLowerBoundOffset,
                completion: {
                    continuation.resume()
                }
            )
        }
    }

    /// Выполнить перемещение контейнера к нижней границе на главном потоке с анимацией.
    public func moveToLowerBound() {
        Task {
            await self.asyncMoveToLowerBound()
        }
    }

    @MainActor
    /// Выполнить перемещение контейнера к верхней границе асинхронно с анимацией.
    public func asyncMoveToUpperBound() async {
        await withCheckedContinuation { continuation in
            self.moveToBound(
                offset: self.safeUpperBoundOffset,
                completion: {
                    continuation.resume()
                }
            )
        }
    }

    /// Выполнить перемещение контейнера к верхней границе на главном потоке с анимацией.
    public func moveToUpperBound() {
        Task {
            await self.asyncMoveToUpperBound()
        }
    }

    @MainActor
    /// Выполнить перемещение контейнера к ближайщей границе асинхронно с анимацией.
    public func asyncMoveToNearestBound() async {
        let middle = self.safeLowerBoundOffset + (self.safeUpperBoundOffset - self.safeLowerBoundOffset) / 2
        let nearest = self.currentOffsetY > middle ? self.safeUpperBoundOffset : self.safeLowerBoundOffset

        return await withCheckedContinuation { continuation in
            self.moveToBound(
                offset: nearest,
                completion: {
                    continuation.resume()
                }
            )
        }
    }

    /// Выполнить перемещение контейнера к ближайщей границе на главном потоке с анимацией.
    public func moveToNearestBound() {
        Task {
            await self.asyncMoveToNearestBound()
        }
    }
}

// MARK: - UIGestureRecognizerDelegate

public extension ChildBottomSheetController {
    @objc
    private func handlePanGestureRecognizer(_ gesture: UIPanGestureRecognizer) {
        let point = gesture.translation(in: self.view)

        switch gesture.state {
        case .changed:
            guard gesture === self.panGestureRecognizer else { return }
            let needCalculateScrollOffset = !self.isTopPosition && !self.isBottomPosition
            if needCalculateScrollOffset {
                self.calculateScrollOffset(of: point.y)
                self.updateTopConstraints()
            } else {
                self.swipeableScrollView.contentOffset.y -= point.y
            }

        case .ended:
            if self.isTopPosition {
                let scrollBounds = self.swipeableScrollView.bounds
                let scrollContentSize = self.swipeableScrollView.contentSize
                if scrollContentSize.height < scrollBounds.height {
                    UIView.animate(withDuration: self.animationDuration) {
                        self.swipeableScrollView.contentOffset.y = 0
                    }
                }
            } else if self.isBottomPosition {
                UIView.animate(withDuration: self.animationDuration) {
                    self.swipeableScrollView.contentOffset.y = 0
                }
            }
            self.updateScrollOffset()

        default:
            break
        }

        gesture.setTranslation(.zero, in: self.view)
    }

    func gestureRecognizer(
        _ gestureRecognizer: UIGestureRecognizer,
        shouldReceive touch: UITouch
    ) -> Bool {
        return gestureRecognizer === self.panGestureRecognizer
        ? (touch.view?.containsCollectionSuperview == false)
        : touch.view == self.view
    }
}

// MARK: - BoundOffset

extension ChildBottomSheetController {
    public enum BoundOffset {
        case constant(CGFloat)
        case calculate(() -> CGFloat)

        var value: CGFloat {
            switch self {
            case let .constant(value):
                return value
            case let .calculate(completion):
                return completion()
            }
        }
    }
}

// MARK: - private

private extension UIView {
    /// contains UITableView or UICollectionView in the hierarchy above
    var containsCollectionSuperview: Bool {
        if self is UITableView || self is UICollectionView {
            return true
        } else {
            return self.superview?.containsCollectionSuperview ?? false
        }
    }
}
