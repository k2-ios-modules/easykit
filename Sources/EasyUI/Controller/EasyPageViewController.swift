//
//  Created on 20.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

open class EasyPageViewController: EasyViewController,
                                   UIPageViewControllerDelegate,
                                   UIPageViewControllerDataSource {

    // MARK: - properties

    @Published
    public private(set) var selectedPageIndex: Int = 0

    public var selectedPageVC: UIViewController? {
        self.pages[safe: self.selectedPageIndex]
    }

    // MARK: - gui

    private var pages: [UIViewController] = []

    public let pageController: UIPageViewController

    open override var swipeableScrollView: UIScrollView {
        (self.selectedPageVC as? SwipeableViewController)?.swipeableScrollView
        ?? super.swipeableScrollView
    }

    // MARK: - init

    public init(
        transitionStyle style: UIPageViewController.TransitionStyle,
        navigationOrientation: UIPageViewController.NavigationOrientation
    ) {
        self.pageController = UIPageViewController(
            transitionStyle: style,
            navigationOrientation: navigationOrientation
        )
        super.init(nibName: nil, bundle: nil)
    }
    
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - life cicle

    open override func viewDidLoad() {
        super.viewDidLoad()

        self.pageController.delegate = self
        self.pageController.dataSource = self

        self.setupLayout()
    }

    // MARK: - layout

    private func setupLayout() {
        self.scrollContentView.isScrollEnabled = false

        self.addChild(self.pageController, to: self.mainContentView)

        self.pageController.view.prepareForAutolayout()
        NSLayoutConstraint.activate([
            self.pageController.view.leftAnchor.constraint(equalTo: self.mainContentView.leftAnchor),
            self.pageController.view.topAnchor.constraint(equalTo: self.mainContentView.topAnchor),
            self.pageController.view.rightAnchor.constraint(equalTo: self.mainContentView.rightAnchor),
            self.pageController.view.bottomAnchor.constraint(equalTo: self.mainContentView.bottomAnchor)
        ])
    }

    // MARK: - setters

    public func setPages(
        _ list: [UIViewController],
        selectedIndex: Int,
        animated: Bool = true
    ) {
        guard let selectedPage = list[safe: selectedIndex] else { return }
        self.pages = list
        self.selectedPageIndex = selectedIndex

        self.pageController.setViewControllers(
            [selectedPage],
            direction: .forward,
            animated: animated
        )
    }

    public func setSelectedPageIndex(
        _ index: Int,
        animated: Bool = true
    ) {
        guard index != self.selectedPageIndex,
              let nextPage = self.pages[safe: index]
        else { return }

        self.pageController.setViewControllers(
            [nextPage],
            direction: self.selectedPageIndex < index ? .forward : .reverse,
            animated: animated
        )

        self.selectedPageIndex = index
    }

    // MARK: - UIPageViewControllerDelegate

    open func pageViewController(
        _ pageViewController: UIPageViewController,
        didFinishAnimating finished: Bool,
        previousViewControllers: [UIViewController],
        transitionCompleted completed: Bool
    ) {
        if completed {
            if let currentViewController = pageViewController.viewControllers?.first,
               let index = self.pages.firstIndex(of: currentViewController) {
                self.selectedPageIndex = index
            }
        }
    }

    // MARK: - UIPageViewControllerDataSource

    open func pageViewController(
        _ pageViewController: UIPageViewController,
        viewControllerBefore viewController: UIViewController
    ) -> UIViewController? {
        guard let currentIndex = self.pages.firstIndex(of: viewController),
              currentIndex > 0
        else {
            return nil
        }
        return self.pages[safe: currentIndex - 1]
    }

    open func pageViewController(
        _ pageViewController: UIPageViewController,
        viewControllerAfter viewController: UIViewController
    ) -> UIViewController? {
        guard let currentIndex = self.pages.firstIndex(of: viewController),
              currentIndex < self.pages.count - 1
        else {
            return nil
        }
        return self.pages[safe: currentIndex + 1]
    }
}
