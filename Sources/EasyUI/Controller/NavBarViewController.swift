//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//

import UIKit

open class NavBarViewController: KeyboardViewController,
                                 UINavigationControllerDelegate,
                                 UIGestureRecognizerDelegate {

    public enum NavBarButtonPosition {
        case left
        case right
    }

    // MARK: - properties

    public var closeControllerCompletion: (() -> Void)?
    
    /// Признак наличия кнопки "Назад".
    ///
    /// По-умолчанию кнопка показывается, если контроллер явняется не первым в стеке навигации.
    open var availableBackBarButton: Bool {
        (self.navigationController?.viewControllers.count ?? 0) > 1
    }

    // MARK: - gui

    private var backBarButton: UIBarButtonItem?

    // MARK: - init

    open override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.delegate = self
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.navigationItem.setHidesBackButton(true, animated: false)
        self.navigationItem.leftBarButtonItems = []
        self.navigationItem.rightBarButtonItems = []

        self.makeNavBarBackButton()
    }

    // MARK: - setters

    // MARK: back button

    open func makeBackButton() -> UIButton {
        let button = UIButton()
        button.setImage(UIImage(systemName: "chevron.backward"), for: .init())
        return button
    }

    @discardableResult
    public final func makeNavBarBackButton(forced: Bool = false) -> UIBarButtonItem? {
        if let backBarButton = self.backBarButton {
            return backBarButton
        }

        guard self.availableBackBarButton || forced
        else { return nil }

        let button = self.makeBackButton()
        let backBarButton = UIBarButtonItem(customView: button)
        backBarButton.setTarget(self, action: #selector(self.closeControllerAction))
        self.backBarButton = backBarButton
        self.setBackBarButton()
        return backBarButton
    }

    private func setBackBarButton() {
        guard let backBarButton = self.backBarButton else { return }
        self.navigationItem
            .leftBarButtonItems?
            .insert(backBarButton, at: 0)
    }

    public final func hideBarBackButton() {
        guard let control = self.backBarButton else { return }
        self.removeNavBarButton(control, from: .left)
    }

    // MARK: common methods

    @discardableResult
    public final func makeNavBarButton(
        image: UIImage?,
        to position: NavBarButtonPosition
    ) -> UIBarButtonItem {
        let button = UIButton()
        button.setImage(image, for: .init())
        let barButtonItem = UIBarButtonItem(customView: button)
        barButtonItem.imageInsets = .zero
        self.addNavBarButton(barButtonItem, to: position)
        return barButtonItem
    }

    public final func addNavBarButton(
        _ barButtonItem: UIBarButtonItem,
        to position: NavBarButtonPosition
    ) {
        switch position {
        case .left:
            self.navigationItem.leftBarButtonItems?.append(barButtonItem)

        case .right:
            self.navigationItem.rightBarButtonItems?.append(barButtonItem)
        }
    }

    public final func removeNavBarButtons(from position: NavBarButtonPosition) {
        switch position {
        case .left:
            self.navigationItem.leftBarButtonItems = []
            self.setBackBarButton()

        case .right:
            self.navigationItem.rightBarButtonItems = []
        }
    }

    public final func removeNavBarButton(
        _ barButtonItem: UIBarButtonItem,
        from position: NavBarButtonPosition
    ) {
        switch position {
        case .left:
            self.navigationItem
                .leftBarButtonItems?
                .removeAll(where: { $0 === barButtonItem })

        case .right:
            self.navigationItem
                .rightBarButtonItems?
                .removeAll(where: { $0 === barButtonItem })
        }
    }

    // MARK: - actions

    // MARK: back action

    @objc
    open func closeControllerAction() {
        if self.isModal {
            self.dismiss(animated: true) { [weak self] in
                self?.closeControllerCompletion?()
            }
        } else {
            self.navigationController?.popViewController(
                animated: true,
                completion: { [weak self] in
                    self?.closeControllerCompletion?()
                })
        }
    }

    // MARK: - UINavigationControllerDelegate

    open func navigationController(
        _ navigationController: UINavigationController,
        didShow viewController: UIViewController,
        animated: Bool
    ) {
        let enable = navigationController.viewControllers.count > 1
        navigationController.interactivePopGestureRecognizer?.isEnabled = enable
    }

    // MARK: - UIGestureRecognizerDelegate

    open func gestureRecognizer(
        _ gestureRecognizer: UIGestureRecognizer,
        shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer
    ) -> Bool {
        return gestureRecognizer === self.navigationController?.interactivePopGestureRecognizer
    }
}

private extension UINavigationController {
    func popViewController(
        animated: Bool,
        completion: @escaping () -> Void
    ) {
        self.popViewController(animated: animated)
        self.performAlongsideTransition(completion: completion)
    }
}
