//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyFoundation
import UIKit

public protocol SwipeableViewController: UIViewController {
    var swipeableScrollView: UIScrollView { get }

    func calculatePreferredContentSize() -> CGSize?

    func updatePreferredContentSize()
}

extension SwipeableViewController {
    public func updatePreferredContentSize() {
        guard let size = self.calculatePreferredContentSize() else { return }
        self.actualPreferredContentSize = size
    }
}
