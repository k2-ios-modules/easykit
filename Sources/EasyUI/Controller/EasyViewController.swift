//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//

import Combine
import EasyFoundation
import UIKit

/// Базовый вью контроллер для отображения экрана.
///
/// Контроллер имеет следующую структуру:
/// - `headerContentView` - контейнер для отображение верхней не скролящейся части экрана;
/// - `scrollContentView` - контейнер для отображение основной скролящейся части экрана;
///     - содержит `mainContentView` - контейнер для отображение контента с возможностью скролла;
/// - `liftingFooterContentView` - контейнер для отображение нижней не скролящейся части экрана, имеет возможность "подпрыгивания" над клавиатурой;
/// - `footerView` - контейнер для отображение нижней не скролящейся части экрана, снизу не имеет ограничение по `safeAreaGuide`;
///     - содержит `footerContentView` - контейнер для отображение нижней не скролящейся части экрана, снизу имеет ограничение по `safeAreaGuide`.
///
/// Для размещения контента необходимо использовать `headerContentView`, `mainContentView`, `liftingFooterContentView` и `footerContentView`.
///
/*
     EasyViewController.view
     +------------------------------------------------+
     |                        ↑                       |
     |                        | safeAreaLayoutGuide   |  <- useTopSafeArea
     | .headerContentView     ↓                       |
     | +--------------------------------------------+ |
     | |                                            | |
     | |                                            | |
     | |                                            | |
     | |                                            | |
     | +--------------------------------------------+ |
     | .scrollContentView                             |
     | +--------------------------------------------+ |
     | | .mainContentView                           | |
     | | +----------------------------------------+ | |
     | | |                                        | | |
     | | |                                        | | |
     | | |                                        | | |
     | | |                                        | | |
     | | |                                        | | |
     | | |                                        | | |
     | | +----------------------------------------+ | |
     | |                                            | |
     | |                                            | |
     | |                                            | |
     | |                                            | |
     | |                                            | |
     | |                                            | |
     | |                                            | |
     | +--------------------------------------------+ |
     | .liftingFooterContentView                      |
     | +--------------------------------------------+ |
     | |                                            | |
     | |                                            | |
     | |                                            | |
     | |                                            | |
     | +--------------------------------------------+ |
     | .footerView                                    |
     | +--------------------------------------------+ |
     | | .footerContentView                         | |
     | | +----------------------------------------+ | |
     | | |                                        | | |
     | | |                                        | | |
     | | |                                        | | |
     | | |                                        | | |
     | | +----------------------------------------+ | |
     | |                      ↑                     | |
     | |                      | safeAreaLayoutGuide | |  <- useBottomSafeArea
     | |                      ↓                     | |
     + +--------------------------------------------+ +
*/
open class EasyViewController: NavBarViewController,
                               SwipeableViewController {

    // MARK: - variables

    private var previousContentBottomInset: CGFloat = .zero

    private var cancellables: [AnyCancellable] = []

    // MARK: constraints

    private var scrollContentTopConstraint: NSLayoutConstraint?

    private var mainContentHeightConstraint: NSLayoutConstraint?

    private var scrollContentBottomConstraint: NSLayoutConstraint?

    private var liftingFooterContentBottomConstraint: NSLayoutConstraint?

    // MARK: safe area

    /// Признак использования `safeAreaLayoutGuide` от верхней границы экрана.
    ///
    /// Если контроллер является дочерним, `safeAreaLayoutGuide` не будут учитываться при построении макета экрана.
    open var useTopSafeArea: Bool {
        self.isChild ? false : true
    }

    /// Признак использования `safeAreaLayoutGuide` от нижней границы экрана.
    ///
    /// Если контроллер является дочерним, `safeAreaLayoutGuide` не будут учитываться при построении макета экрана.
    /// Значение `true` для дочернего контроллера будет учитывать `safeAreaInsets` отступ снизу для `footerContentView`.
    open var useBottomSafeArea: Bool {
        self.isChild ? false : true
    }

    private var childSafeAreaInsets: UIEdgeInsets {
        self.isChild && self.useBottomSafeArea
        ? UIApplication.shared.delegate?.window??.safeAreaInsets ?? .zero
        : .zero
    }

    // MARK: anchors

    private var topAnchor: NSLayoutYAxisAnchor {
        if self.isChild {
            return self.view.topAnchor
        } else {
            return self.useTopSafeArea ? self.view.safeAreaLayoutGuide.topAnchor : self.view.topAnchor
        }
    }

    private var bottomAnchor: NSLayoutYAxisAnchor {
        if self.isChild {
            return self.view.bottomAnchor
        } else {
            return self.useBottomSafeArea ? self.view.safeAreaLayoutGuide.bottomAnchor : self.view.bottomAnchor
        }
    }

    // MARK: life cicle flags
    
    /// Признак, что экран сейчас будет показан.
    public private(set) var isWillAppeared: Bool = false

    /// Признак, что экран уже показан.
    public private(set) var isDidAppeared: Bool = false

    // MARK: size

    public var actualHeaderHeight: CGFloat {
        self.isHeaderVisible ? self.headerContentView.bounds.height : 0
    }

    public var actualLiftingFooterHeight: CGFloat {
        self.isLiftingFooterVisible ? self.liftingFooterContentView.bounds.height : 0
    }

    public var actualFooterContentHeight: CGFloat {
        self.isFooterVisible ? self.footerContentView.bounds.height : 0.0
    }

    public var actualFooterHeight: CGFloat {
        self.footerView.bounds.height
    }

    // MARK: view

    public var isHeaderVisible: Bool {
        self.getContainerVisiblity(self.headerContentView)
    }

    public var isScrollEnabled: Bool {
        self.scrollContentView.isScrollEnabled
    }

    public var isLiftingFooterVisible: Bool {
        self.getContainerVisiblity(self.liftingFooterContentView)
    }

    public var isFooterVisible: Bool {
        self.getContainerVisiblity(self.footerContentView)
    }

    // MARK: - gui

    // MARK: top views

    public private(set) lazy var headerContentView = UIView().prepareForAutolayout()

    // MARK: middle views

    open var swipeableScrollView: UIScrollView { self.scrollContentView }

    public private(set) lazy var scrollContentView = UIScrollView().prepareForAutolayout()

    public private(set) lazy var mainContentView = UIView().prepareForAutolayout()

    // MARK: bottom views

    public private(set) lazy var liftingFooterContentView = UIView().prepareForAutolayout()

    public private(set) lazy var footerContentView = UIView().prepareForAutolayout()

    public private(set) lazy var footerView = UIView().prepareForAutolayout()

    // MARK: - life cycle

    open override func viewDidLoad() {
        super.viewDidLoad()

        self.setNeedsStatusBarAppearanceUpdate()

        self.setupLayout()

        self.setBinding()
    }

    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if self.isWillAppeared == false {
            self.isWillAppeared = true
            self.singleWillAppear()
        }
    }

    open func singleWillAppear() { }

    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if self.isDidAppeared == false {
            self.isDidAppeared = true
            self.singleDidAppear()
        }
    }

    open func singleDidAppear() { }

    open override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        self.updatePreferredContentSize()
    }

    // MARK: - keyboard visibility

    private func setContentBottomInsetIfNeeded() {
        var inset = self.previousContentBottomInset

        if let responder = self.view.firstResponder as? TextInputProtocol {
            inset += responder.scrollContentAdditionalBottomInset
        }

        self.swipeableScrollView.contentInset.bottom = inset
    }

    open override func keyboardDidShowAction() {
        if self.keyboardShown == false {
            self.previousContentBottomInset = self.swipeableScrollView.contentInset.bottom
        }
        super.keyboardDidShowAction()

        self.setContentBottomInsetIfNeeded()
    }

    open override func keyboardWillHideAction() {
        super.keyboardWillHideAction()

        self.setContentBottomInsetIfNeeded()
    }

    open override func keyboardWillShowAnimationAction() {
        super.keyboardWillShowAnimationAction()

        self.updatePreferredContentSize()
        self.updateScrollBottomOffset()
    }

    open override func keyboardWillHideAnimationAction() {
        super.keyboardWillHideAnimationAction()

        self.updatePreferredContentSize()
        self.updateScrollBottomOffset()
    }

    // MARK: - layout

    private func setupLayout() {
        self.scrollContentView.addSubview(self.mainContentView)
        self.footerView.addSubview(self.footerContentView)

        self.view.addSubviews([
            self.headerContentView,
            self.scrollContentView,
            self.liftingFooterContentView,
            self.footerView
        ])

        // MARK: header

        NSLayoutConstraint.activate([
            self.headerContentView.topAnchor.constraint(equalTo: self.topAnchor),
            self.headerContentView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            self.headerContentView.rightAnchor.constraint(equalTo: self.view.rightAnchor)
        ])

        // MARK: scroll

        self.scrollContentTopConstraint = self.scrollContentView.topAnchor.constraint(equalTo: self.topAnchor)
        self.scrollContentBottomConstraint = self.scrollContentView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        NSLayoutConstraint.activate([
            self.scrollContentTopConstraint,
            self.scrollContentView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            self.scrollContentView.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            self.scrollContentBottomConstraint
        ])

        // MARK: main content

        NSLayoutConstraint.activate([
            self.mainContentView.topAnchor.constraint(equalTo: self.scrollContentView.topAnchor),
            self.mainContentView.leftAnchor.constraint(equalTo: self.scrollContentView.leftAnchor),
            self.mainContentView.rightAnchor.constraint(equalTo: self.scrollContentView.rightAnchor),
            self.mainContentView.bottomAnchor.constraint(equalTo: self.scrollContentView.bottomAnchor),
            self.mainContentView.widthAnchor.constraint(equalTo: self.scrollContentView.widthAnchor)
        ])
        self.updateMainContentHeightConstraint(withScrolling: self.isScrollEnabled)

        // MARK: footer

        self.liftingFooterContentBottomConstraint = self.liftingFooterContentView.bottomAnchor.constraint(equalTo: self.footerView.topAnchor)
        NSLayoutConstraint.activate([
            self.liftingFooterContentView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            self.liftingFooterContentView.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            self.liftingFooterContentBottomConstraint
        ])

        NSLayoutConstraint.activate([
            self.footerView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            self.footerView.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            self.footerView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
        ])

        NSLayoutConstraint.activate([
            self.footerContentView.topAnchor.constraint(equalTo: self.footerView.topAnchor),
            self.footerContentView.leftAnchor.constraint(equalTo: self.footerView.leftAnchor),
            self.footerContentView.rightAnchor.constraint(equalTo: self.footerView.rightAnchor),
            self.footerContentView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -self.childSafeAreaInsets.bottom)
        ])
    }

    private func updateScrollContentTopOffset() {
        self.scrollContentTopConstraint?.constant = self.actualHeaderHeight
    }

    private func updateMainContentHeightConstraint(withScrolling: Bool) {
        let constraint: NSLayoutConstraint
        if let mainContentHeightConstraint {
            constraint = mainContentHeightConstraint
        } else {
            constraint = self.mainContentView.heightAnchor.constraint(equalTo: self.scrollContentView.heightAnchor)
            self.mainContentHeightConstraint = constraint
        }
        constraint.isActive = withScrolling == false
    }

    private func updateScrollBottomOffset() {
        let safeAreaBottomOffset = self.isChild || self.useBottomSafeArea ? .zero : self.view.safeAreaInsets.bottom
        let liftingFooterHeight = self.actualLiftingFooterHeight
        let footerContentHeight = self.actualFooterContentHeight
        let footerHeight = self.actualFooterHeight
        let keyboardOffset = self.actualKeyboardOffset
        let tabBarHeight = self.tabBarController?.tabBar.frame.height ?? 0

        let liftingFooterOffset = -max(keyboardOffset - footerHeight + tabBarHeight, 0.0)
        self.liftingFooterContentBottomConstraint?.constant = liftingFooterOffset

        let scrollContentOffset = -max(liftingFooterHeight - liftingFooterOffset + footerContentHeight - safeAreaBottomOffset, 0.0)
        self.scrollContentBottomConstraint?.constant = scrollContentOffset
    }

    // MARK: - preferred content size

    open func calculatePreferredContentSize() -> CGSize? {
        guard self.isScrollEnabled || self.isChild || self.isModal else { return nil }
        self.headerContentView.layoutIfNeeded()
        self.swipeableScrollView.layoutIfNeeded()
        self.liftingFooterContentView.layoutIfNeeded()
        self.footerView.layoutIfNeeded()

        var contentSize = self.swipeableScrollView.contentSize
        contentSize.height += self.swipeableScrollView.contentInset.top
        + self.swipeableScrollView.contentInset.bottom
        + self.actualHeaderHeight
        + self.actualLiftingFooterHeight
        + self.actualFooterHeight
        + self.keyboardOffset

        return contentSize
    }

    // MARK: - binding

    private func setBinding() {
        self.headerContentView.publisherHiddenAndHeight()
            .sink { [weak self] _ in
                self?.updateScrollContentTopOffset()
            }
            .store(in: &self.cancellables)

        self.scrollContentView.publisher(for: \.isScrollEnabled)
            .sink { [weak self] isScrollEnabled in
                self?.updateMainContentHeightConstraint(withScrolling: isScrollEnabled)
            }
            .store(in: &self.cancellables)

        Publishers.CombineLatest(
            self.liftingFooterContentView.publisherHiddenAndHeight(),
            self.footerContentView.publisherHiddenAndHeight()
        )
        .sink { [weak self] _ in
            self?.updateScrollBottomOffset()
        }
        .store(in: &self.cancellables)
    }

    // MARK: - utils

    private func getContainerVisiblity(_ container: UIView) -> Bool {
        (container.isHidden || container.subviews.isEmpty) == false
    }

    private func getContainerVisiblity(_ container: UIView, params: BoolCGFloat) -> Bool {
        (params.0 || container.subviews.isEmpty || params.1 == .zero) == false
    }
}

// MARK: - private

private typealias BoolCGFloat = (Bool, CGFloat)

private extension UIView {
    func publisherHiddenAndHeight() -> AnyPublisher<BoolCGFloat, Never> {
        Publishers.CombineLatest(
            self.publisher(for: \.isHidden),
            self.publisher(for: \.bounds)
        )
        .receive(on: DispatchQueue.main)
        .map({ (item: (Bool, CGRect)) -> BoolCGFloat in
            (item.0, item.1.height)
        })
        .removeDuplicates(by: { (lrs: BoolCGFloat, rhs: BoolCGFloat) in
            lrs == rhs
        })
        .eraseToAnyPublisher()
    }
}
