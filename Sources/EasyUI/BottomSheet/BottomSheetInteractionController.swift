//
//  Created on 14.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyFoundation
import UIKit

final class BottomSheetInteractionController: NSObject {

    // MARK: - subtypes

    typealias Config = BottomSheetConfig

    private enum ScrollingState {
        case dragging
        case noDragging
        case interception(initTranslation: CGPoint)
    }

    // MARK: - properties

    private weak var presentedViewController: UIViewController?

    private(set) var interactiveTransition: UIPercentDrivenInteractiveTransition?

    private weak var trackingScrollView: UIScrollView?

    private var scrollingState: ScrollingState = .noDragging

    private let config: Config

    private let dismissCompletion: (() -> Void)?

    // MARK: - init

    init(
        presentedViewController: UIViewController,
        config: Config,
        dismissCompletion: (() -> Void)?
    ) {
        self.presentedViewController = presentedViewController
        self.config = config
        self.dismissCompletion = dismissCompletion

        super.init()

        presentedViewController.loadViewIfNeeded()

        initPanGesture()
        initScrollingTracking()
    }

    private func initPanGesture() {
        guard config.dismissOnPan else { return }

        let panRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture))
        panRecognizer.delegate = self

        presentedViewController?.view.addGestureRecognizer(panRecognizer)
    }

    private func initScrollingTracking() {
        guard config.dismissOnPan else { return }

        if let vc = presentedViewController as? SwipeableViewController {
           trackingScrollView = vc.swipeableScrollView
        }

        trackingScrollView?.panGestureRecognizer.setTarget(self, action: #selector(self.handlePanGestureRecognizer))
    }

    @objc
    private func handlePanGestureRecognizer(_ gesture: UIPanGestureRecognizer) {
        guard let scrollView = trackingScrollView else { return }

        switch gesture.state {
        case .began:
            scrollingState = .dragging

        case .changed:
            updateScrollingState(scrollView: scrollView)

            if case .interception = scrollingState {
                scrollView.bounds.origin.y = -scrollView.adjustedContentInset.top
                let translation = scrollView.panGestureRecognizer.translation()

                beginTransitionIfNeeded()
                changeTransition(translation: translation)
            }

        case .ended:
            if case .interception(let initTranslation) = scrollingState {
                let endTranslation = scrollView.panGestureRecognizer.endTranslation()
                finishTransition(translation: endTranslation - initTranslation)
            } else {
                cancelTransition()
            }
            scrollingState = .noDragging

        default:
            break
        }
    }

    // MARK: - transition

    private func beginTransitionIfNeeded() {
        guard interactiveTransition == nil else { return }

        interactiveTransition = .init()
        presentedViewController?.dismiss(animated: true, completion: dismissCompletion)
    }

    private func changeTransition(translation: CGPoint) {
        guard let presentedView = presentedViewController?.view else { return }

        let percentComplete = self.prepareOffset(translation.y / presentedView.bounds.height)
        interactiveTransition?.update(percentComplete)
    }

    private func finishTransition(translation: CGPoint) {
        guard let presentedView = presentedViewController?.view else { return }

        let progress = self.prepareOffset(translation.y / presentedView.bounds.height)

        if progress < config.progressThreshold {
            cancelTransition()
        } else {
            endTransition()
        }
    }

    private func endTransition() {
        interactiveTransition?.completionSpeed = transitionFinalSpeed()
        interactiveTransition?.finish()
        interactiveTransition = nil
    }

    private func cancelTransition() {
        interactiveTransition?.completionSpeed = transitionFinalSpeed()
        interactiveTransition?.cancel()
        interactiveTransition = nil
    }

    private func transitionFinalSpeed() -> CGFloat {
        // no math excuse
        1 - pow((interactiveTransition?.percentComplete ?? 0), 0.35)
    }

    // MARK: - utils

    private func prepareOffset(_ value: CGFloat) -> CGFloat {
        switch self.config.display {
        case .fromBottom:
            return value
        case .fromTop:
            return -value
        }
    }

}

// MARK: - UIGestureRecognizerDelegate

extension BottomSheetInteractionController: UIGestureRecognizerDelegate {
    @objc private func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        switch panGesture.state {
        case .began:
            beginTransitionIfNeeded()
        case .changed:
            let translation = panGesture.translation()
            changeTransition(translation: translation)
        case .ended:
            let translation = panGesture.endTranslation()
            finishTransition(translation: translation)
        case .cancelled:
            cancelTransition()
        default:
            break
        }
    }
}

// MARK: - UIScrollViewDelegate

extension BottomSheetInteractionController {

    private func updateScrollingState(scrollView: UIScrollView) {
        guard scrollView.isTracking else { return }

        let contentOffsetY = self.prepareOffset(scrollView.contentOffset.y)
        let velocityY = self.prepareOffset(scrollView.panGestureRecognizer.velocity().y)

        let isNoInteraction = interactiveTransition?.percentComplete.isEqual(to: 0) ?? true
        let isOffsetAtStart = contentOffsetY <= -scrollView.adjustedContentInset.top
        let isScrollingDown = velocityY > 0

        switch scrollingState {
        case .dragging where isNoInteraction && isOffsetAtStart && isScrollingDown:
            let translation = scrollView.panGestureRecognizer.translation()
            scrollingState = .interception(initTranslation: translation)
        case .interception where isNoInteraction && !isOffsetAtStart && !isScrollingDown:
            scrollingState = .dragging
        default:
            break
        }
    }
}
