//
//  Created on 13.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public struct BottomSheetSafeAreaOptions: OptionSet {
    public static let `default`: Self = []

    public static let ignoreTop: Self = .init(rawValue: 1 << 0)
    public static let ignoreBottom: Self = .init(rawValue: 1 << 1)
    public static let ignorePullbar: Self = .init(rawValue: 1 << 2)
    public static let ignoreNavbar: Self = .init(rawValue: 1 << 3)

    public private(set) var rawValue: UInt

    public init(rawValue: UInt) {
        self.rawValue = rawValue
    }
}
