//
//  Created on 14.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

final class BottomSheetDismissingAnimator: NSObject {

    // MARK: - properties

    private let transitionDuration: TimeInterval
    private let display: BottomSheetDisplay

    private var interruptibleAnimator: UIViewImplicitlyAnimating?

    // MARK: - init

    init(
        transitionDuration: TimeInterval,
        display: BottomSheetDisplay
    ) {
        self.transitionDuration = transitionDuration
        self.display = display

        super.init()
    }

    // MARK: - utils

    func getFinalTransform(_ presentedFrame: CGRect) -> CGAffineTransform {
        switch self.display {
        case .fromBottom:
            return .identity.translatedBy(x: 0, y: presentedFrame.height)

        case .fromTop:
            return .identity.translatedBy(x: 0, y: -presentedFrame.height)
        }
    }
}

// MARK: - UIViewControllerAnimatedTransitioning

extension BottomSheetDismissingAnimator: UIViewControllerAnimatedTransitioning {
    func transitionDuration(
        using transitionContext: UIViewControllerContextTransitioning?
    ) -> TimeInterval {
        self.transitionDuration
    }

    func animateTransition(
        using transitionContext: UIViewControllerContextTransitioning
    ) {
        self.interruptibleAnimator(using: transitionContext).startAnimation()
    }

    func interruptibleAnimator(
        using transitionContext: UIViewControllerContextTransitioning
    ) -> UIViewImplicitlyAnimating {
        if let interruptibleAnimator { return interruptibleAnimator }

        let presentedView = transitionContext.view(forKey: .from)
        let presentedFrame = presentedView?.frame ?? .zero

        let animator = UIViewPropertyAnimator(
            duration: self.transitionDuration(using: transitionContext),
            dampingRatio: 1.0,
            animations: {
                presentedView?.transform = self.getFinalTransform(presentedFrame)
            }
        )
        animator.addCompletion { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }

        self.interruptibleAnimator = animator

        return animator
    }
}
