//
//  Created on 14.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//

import UIKit

public final class BottomSheetTransitioningDelegate: NSObject {

    // MARK: - properties

    private let config: BottomSheetConfig

    private let dismissCompletion: (() -> Void)?

    private var interactionController: BottomSheetInteractionController?

    // MARK: - initialization

    public init(
        config: BottomSheetConfig,
        dismissCompletion: (() -> Void)?
    ) {
        self.config = config
        self.dismissCompletion = dismissCompletion

        super.init()
    }
}

// MARK: - UIViewControllerTransitioningDelegate

extension BottomSheetTransitioningDelegate: UIViewControllerTransitioningDelegate {
    public func presentationController(
        forPresented presented: UIViewController,
        presenting: UIViewController?,
        source: UIViewController
    ) -> UIPresentationController? {
        let presentationController = BottomSheetPresentationController(
            presentedViewController: presented,
            presentingViewController: presenting ?? source,
            config: self.config,
            dismissCompletion: dismissCompletion
        )
        interactionController = BottomSheetInteractionController(
            presentedViewController: presented,
            config: self.config,
            dismissCompletion: self.dismissCompletion
        )
        return presentationController
    }

    public func animationController(
        forPresented presented: UIViewController,
        presenting: UIViewController,
        source: UIViewController
    ) -> UIViewControllerAnimatedTransitioning? {
        BottomSheetPresentingAnimator(
            transitionDuration: self.config.transitionDuration,
            display: self.config.display
        )
    }

    public func animationController(
        forDismissed dismissed: UIViewController
    ) -> UIViewControllerAnimatedTransitioning? {
        BottomSheetDismissingAnimator(
            transitionDuration: self.config.transitionDuration,
            display: self.config.display
        )
    }

    public func interactionControllerForDismissal(
        using animator: UIViewControllerAnimatedTransitioning
    ) -> UIViewControllerInteractiveTransitioning? {
        self.interactionController?.interactiveTransition
    }
}

