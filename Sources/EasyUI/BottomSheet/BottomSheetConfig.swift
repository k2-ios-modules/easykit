//
//  Created on 14.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//

import UIKit

public struct BottomSheetConfig {
    public let display: BottomSheetDisplay
    public let cornerRadius: CGFloat
    public let pullBarConfig: BottomSheetPullBarConfig
    public let backgroundConfig: BottomSheetBackgroundConfig
    public let dismissOnTap: Bool
    public let dismissOnPan: Bool
    public let progressThreshold: CGFloat
    public let transitionDuration: TimeInterval
    public let contentHeight: BottomSheetContentHeight
    public let safeAreaOptions: BottomSheetSafeAreaOptions

    public init(
        display: BottomSheetDisplay,
        cornerRadius: CGFloat,
        pullBarConfig: BottomSheetPullBarConfig,
        backgroundConfig: BottomSheetBackgroundConfig,
        dismissOnTap: Bool,
        dismissOnPan: Bool,
        progressThreshold: CGFloat,
        transitionDuration: TimeInterval,
        contentHeight: BottomSheetContentHeight,
        safeAreaOptions: BottomSheetSafeAreaOptions
    ) {
        self.display = display
        self.cornerRadius = cornerRadius
        self.pullBarConfig = pullBarConfig
        self.backgroundConfig = backgroundConfig
        self.dismissOnTap = dismissOnTap
        self.dismissOnPan = dismissOnPan
        self.progressThreshold = progressThreshold
        self.transitionDuration = transitionDuration
        self.contentHeight = contentHeight
        self.safeAreaOptions = safeAreaOptions
    }
}

// MARK: - overlay

public extension BottomSheetConfig {
    static let overlay: Self = overlay()

    static func overlay(
        display: BottomSheetDisplay = .fromBottom,
        cornerRadius: CGFloat = 16,
        pullBarConfig: BottomSheetPullBarConfig = .overlay(.fromBottom),
        backgroundConfig: BottomSheetBackgroundConfig = .default,
        progressThreshold: CGFloat = 0.2,
        transitionDuration: CGFloat = 0.3,
        contentHeight: BottomSheetContentHeight = .preferred,
        safeAreaOptions: BottomSheetSafeAreaOptions = .default
    ) -> Self {
        .init(
            display: display,
            cornerRadius: cornerRadius,
            pullBarConfig: pullBarConfig,
            backgroundConfig: backgroundConfig,
            dismissOnTap: true,
            dismissOnPan: true,
            progressThreshold: progressThreshold,
            transitionDuration: transitionDuration,
            contentHeight: contentHeight,
            safeAreaOptions: safeAreaOptions
        )
    }
}

// MARK: - dialogue

public extension BottomSheetConfig {
    static let dialogue: Self = dialogue()

    static func dialogue(
        display: BottomSheetDisplay = .fromBottom,
        cornerRadius: CGFloat = 16,
        pullBarConfig: BottomSheetPullBarConfig = .dialogue,
        backgroundConfig: BottomSheetBackgroundConfig = .default,
        progressThreshold: CGFloat = 0.2,
        transitionDuration: CGFloat = 0.3,
        contentHeight: BottomSheetContentHeight = .preferred,
        safeAreaOptions: BottomSheetSafeAreaOptions = .default
    ) -> Self {
        .init(
            display: display,
            cornerRadius: cornerRadius,
            pullBarConfig: pullBarConfig,
            backgroundConfig: backgroundConfig,
            dismissOnTap: false,
            dismissOnPan: false,
            progressThreshold: progressThreshold,
            transitionDuration: transitionDuration,
            contentHeight: contentHeight,
            safeAreaOptions: safeAreaOptions
        )
    }
}
