//
//  Created on 25.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public enum BottomSheetDisplay: Int {
    case fromBottom
    case fromTop
}
