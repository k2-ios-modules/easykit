//
//  Created on 14.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

final class BottomSheetPullBarView: EasyView {

    // MARK: - variables

    private let config: BottomSheetPullBarConfig

    // MARK: - gui

    private var shapeView: UIView?

    // MARK: - init

    init(config: BottomSheetPullBarConfig) {
        self.config = config
        super.init()

        if let shape = config.shape {
            let shapeView = self.makeShapeView(config: shape)
            self.addSubview(shapeView)
            self.shapeView = shapeView
        }
    }

    required init?(coder: NSCoder) {
        preconditionFailure("init(coder:) has not been implemented")
    }

    // MARK: - layout

    private func makeShapeView(config: BottomSheetPullBarConfig.Shape) -> UIView {
        let view = UIView()
        view.frame = CGRect(origin: .zero, size: config.size)
        view.layer.cornerRadius = config.size.height / 2
        view.backgroundColor = config.color
        return view
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        if let shapeView {
            shapeView.center = CGPoint(
                x: self.bounds.midX,
                y: self.config.topInset
            )
        }
    }
}
