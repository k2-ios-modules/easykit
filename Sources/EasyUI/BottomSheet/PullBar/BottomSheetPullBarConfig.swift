//
//  Created on 14.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

public struct BottomSheetPullBarConfig {
    public let topInset: CGFloat
    public let bottomInset: CGFloat
    public let shape: Shape?

    public var height: CGFloat {
        self.topInset + (self.shape?.size.height ?? 0) + self.bottomInset
    }

    public init(
        topInset: CGFloat,
        bottomInset: CGFloat,
        shape: Shape?
    ) {
        self.topInset = topInset
        self.bottomInset = bottomInset
        self.shape = shape
    }

    public static var zero: Self {
        .init(topInset: 0, bottomInset: 0, shape: nil)
    }
}

extension BottomSheetPullBarConfig {
    public struct Shape {
        public let size: CGSize
        public let color: UIColor

        public init(
            size: CGSize,
            color: UIColor
        ) {
            self.size = size
            self.color = color
        }

        static var overlay: Self {
            .init(
                size: CGSize(width: 46, height: 5),
                color: .systemGray
            )
        }
    }
}

public extension BottomSheetPullBarConfig {
    static func overlay(_ diplay: BottomSheetDisplay) -> Self {
        let topInset: CGFloat
        let bottomInset: CGFloat

        switch diplay {
        case .fromBottom:
            topInset = 16
            bottomInset = 30

        case .fromTop:
            topInset = 30
            bottomInset = 16
        }

        return .init(
            topInset: topInset,
            bottomInset: bottomInset,
            shape: .overlay
        )
    }

    static let dialogue: Self = .init(
        topInset: 26,
        bottomInset: 0,
        shape: nil
    )

    static let clear: Self = .init(
        topInset: 0,
        bottomInset: 0,
        shape: nil
    )
}
