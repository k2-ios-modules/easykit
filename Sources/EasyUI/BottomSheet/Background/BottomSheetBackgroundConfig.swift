//
//  Created on 13.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

public struct BottomSheetBackgroundConfig {
    public let color: UIColor
    public let blurStyle: UIBlurEffect.Style
    public let blurIntensity: CGFloat

    public init(
        color: UIColor,
        blurStyle: UIBlurEffect.Style,
        blurIntensity: CGFloat
    ) {
        self.color = color
        self.blurStyle = blurStyle
        self.blurIntensity = blurIntensity
    }
}


public extension BottomSheetBackgroundConfig {
    static let `default`: Self = .init(
        color: .black.withAlphaComponent(0.5),
        blurStyle: .dark,
        blurIntensity: 0.05
    )

    static let clear: Self = .init(
        color: .clear,
        blurStyle: .dark,
        blurIntensity: 0.00
    )
}
