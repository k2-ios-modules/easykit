//
//  Created on 13.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

final class BottomSheetBackgroundView: EasyView {

    // MARK: - gui

    private let blurView: IntensityVisualEffectView

    // MARK: - init

    init(config: BottomSheetBackgroundConfig) {
        self.blurView = IntensityVisualEffectView(
            blurStyle: config.blurStyle,
            intensity: config.blurIntensity
        )
        super.init()

        self.backgroundColor = config.color
        self.addSubview(self.blurView)
    }

    required init?(coder: NSCoder) {
        preconditionFailure("init(coder:) has not been implemented")
    }

    // MARK: - layout

    override func layoutSubviews() {
        super.layoutSubviews()

        self.blurView.frame = self.bounds
    }
}
