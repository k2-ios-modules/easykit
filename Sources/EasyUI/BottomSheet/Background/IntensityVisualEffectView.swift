//
//  Created on 13.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//

import UIKit

final class IntensityVisualEffectView: UIVisualEffectView {

    // MARK: - variables

    private let customIntensity: CGFloat

    private var animator: UIViewPropertyAnimator?

    // MARK: - gui

    private let visualEffect: UIVisualEffect

    // MARK: - init

    init(
        blurStyle: UIBlurEffect.Style,
        intensity: CGFloat
    ) {
        self.visualEffect = UIBlurEffect(style: blurStyle)
        self.customIntensity = intensity
        super.init(effect: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        preconditionFailure("init(coder:) has not been implemented")
    }

    deinit {
        self.animator?.stopAnimation(true)
    }

    // MARK: - life cycle

    override func didMoveToSuperview() {
        guard let superview else { return }

        self.backgroundColor = .clear
        self.frame = superview.bounds
        self.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }

    // MARK: - core graphics

    override func draw(_ rect: CGRect) {
        super.draw(rect)

        self.effect = nil
        self.animator?.stopAnimation(true)
        self.animator = UIViewPropertyAnimator(duration: 1, curve: .linear) { [weak self] in
            guard let self else { return }
            self.effect = self.visualEffect
        }
        self.animator?.fractionComplete = self.customIntensity
    }
}
