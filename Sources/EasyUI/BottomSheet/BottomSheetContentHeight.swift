//
//  Created on 13.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public enum BottomSheetContentHeight {
    public static let preferred: Self = .preferred(min: .leastNormalMagnitude)

    case preferred(min: CGFloat)
    case maximum
    case custom(CGFloat)
}
