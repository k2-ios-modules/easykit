//
//  Created on 14.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import EasyFoundation
import UIKit

final class BottomSheetPresentationController: UIPresentationController {

    // MARK: - variables

    private var keyboardCancallebles: Set<AnyCancellable> = []

    private var keyboardShown: Bool = false

    // MARK: - gui

    private lazy var pullBarView = BottomSheetPullBarView(config: self.config.pullBarConfig)

    private let backgroundView: BottomSheetBackgroundView

    // MARK: - properties

    private let config: BottomSheetConfig

    private let dismissCompletion: (() -> Void)?

    private var presentedViewHeightConstraint: NSLayoutConstraint?

    // MARK: - initialization

    init(
        presentedViewController: UIViewController,
        presentingViewController: UIViewController?,
        config: BottomSheetConfig,
        dismissCompletion: (() -> Void)?
    ) {
        self.config = config
        self.backgroundView = BottomSheetBackgroundView(config: config.backgroundConfig)
        self.dismissCompletion = dismissCompletion

        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)

        self.setKeyboardNotifications()
    }

    // MARK: - keyboard

    private func setKeyboardNotifications() {
        NotificationCenter.default
            .publisher(for: UIResponder.keyboardWillShowNotification)
            .sink { [weak self] _ in self?.keyboardWillShow() }
            .store(in: &self.keyboardCancallebles)

        NotificationCenter.default
            .publisher(for: UIResponder.keyboardWillHideNotification)
            .sink { [weak self] _ in self?.keyboardWillHide() }
            .store(in: &self.keyboardCancallebles)
    }

    private func keyboardWillShow() {
        guard self.keyboardShown == false else { return }
        self.keyboardShown = true
    }

    private func keyboardWillHide() {
        guard self.keyboardShown else { return }
        self.keyboardShown = false
    }

    // MARK: - presenting

    override func presentationTransitionWillBegin() {
        self.setBackgroundView()
        self.setPullBarView()
        self.setPresentedView()
        self.setDismissOnTap()

        self.presentedViewController.performAlongsideTransition(animation: { [weak self] in
            self?.backgroundView.alpha = 1
        })
    }

    private func setDismissOnTap() {
        guard self.config.dismissOnTap else { return }

        self.backgroundView.addGestureRecognizer(UITapGestureRecognizer(
            target: self,
            action: #selector(self.dismissPresented)
        ))
    }

    private func setBackgroundView() {
        guard let containerView else { return }

        containerView.addSubview(self.backgroundView)

        NSLayoutConstraint.activate([
            self.backgroundView.leftAnchor.constraint(equalTo: containerView.leftAnchor),
            self.backgroundView.topAnchor.constraint(equalTo: containerView.topAnchor),
            self.backgroundView.rightAnchor.constraint(equalTo: containerView.rightAnchor),
            self.backgroundView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
        ])

        self.backgroundView.alpha = 0
    }

    private func setPullBarView() {
        guard let presentedView else { return }

        presentedView.addSubview(self.pullBarView)

        var constrains: [NSLayoutConstraint] = [
            self.pullBarView.leftAnchor.constraint(equalTo: presentedView.leftAnchor),
            self.pullBarView.rightAnchor.constraint(equalTo: presentedView.rightAnchor),
            self.pullBarView.heightAnchor.constraint(equalToConstant: self.config.pullBarConfig.height)
        ]

        switch self.config.display {
        case .fromBottom:
            constrains.append(
                self.pullBarView.topAnchor.constraint(equalTo: presentedView.topAnchor)
            )

        case .fromTop:
            constrains.append(
                self.pullBarView.bottomAnchor.constraint(equalTo: presentedView.bottomAnchor)
            )
        }

        NSLayoutConstraint.activate(constrains)
    }

    private func setPresentedView() {
        guard let containerView, let presentedView else { return }

        presentedView.prepareForAutolayout()
        containerView.addSubview(presentedView)
        presentedView.clipsToBounds = true
        presentedView.layer.cornerRadius = config.cornerRadius

        self.presentedViewHeightConstraint = presentedView.heightAnchor.constraint(equalToConstant: 0)
        var constrains: [NSLayoutConstraint?] = [
            presentedView.leftAnchor.constraint(equalTo: containerView.leftAnchor),
            presentedView.rightAnchor.constraint(equalTo: containerView.rightAnchor)
        ]

        switch self.config.display {
        case .fromBottom:
            constrains.append(
                presentedView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor)
            )
            presentedView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]

        case .fromTop:
            constrains.append(
                presentedView.topAnchor.constraint(equalTo: containerView.topAnchor)
            )
            presentedView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMaxXMaxYCorner]
        }

        NSLayoutConstraint.activate(constrains)

        if !config.safeAreaOptions.contains(.ignorePullbar) {
            switch self.config.display {
            case .fromBottom:
                presentedViewController.additionalSafeAreaInsets.top += config.pullBarConfig.height

            case .fromTop:
                presentedViewController.additionalSafeAreaInsets.bottom += config.pullBarConfig.height
            }
        }
    }

    // MARK: - dismissing

    override func dismissalTransitionWillBegin() {
        self.presentedViewController.performAlongsideTransition(animation: { [weak self] in
            self?.backgroundView.alpha = 0
        })
    }

    @objc
    private func dismissPresented() {
        self.presentedViewController.dismiss(
            animated: true,
            completion: self.dismissCompletion
        )
    }

    // MARK: - dismissed

    override func dismissalTransitionDidEnd(_ completed: Bool) {
        guard completed else { return }

        pullBarView.removeFromSuperview()
        backgroundView.removeFromSuperview()

        switch self.config.display {
        case .fromBottom:
            if !self.config.safeAreaOptions.contains(.ignorePullbar) {
                presentedViewController.additionalSafeAreaInsets.top -= self.config.pullBarConfig.height
            }

        case .fromTop:
            if !self.config.safeAreaOptions.contains(.ignorePullbar) {
                presentedViewController.additionalSafeAreaInsets.bottom -= self.config.pullBarConfig.height
            }
        }
    }

    // MARK: - resizing

    override func preferredContentSizeDidChange(forChildContentContainer container: UIContentContainer) {
        updatePresentedViewHeight()
    }

    override func containerViewDidLayoutSubviews() {
        updatePresentedViewHeight()
    }

    private func updatePresentedViewHeight() {
        let preferredContentHeight = presentedViewController.preferredContentSize.height
        guard let containerView, preferredContentHeight != .zero else { return }

        var maxHeight = containerView.bounds.height
        var presentedHeight: CGFloat

        let display = self.config.display
        let safeAreaOptions = self.config.safeAreaOptions

        switch display {
        case .fromBottom:
            if !safeAreaOptions.contains(.ignoreTop) {
                maxHeight -= containerView.safeAreaInsets.top
            }

        case .fromTop:
            if !safeAreaOptions.contains(.ignoreBottom) {
                maxHeight -= containerView.safeAreaInsets.bottom
            }
        }

        if !safeAreaOptions.contains(.ignoreNavbar) {
            maxHeight -= presentingViewController.navigationBarSize.height
        }

        switch config.contentHeight {
        case .preferred(let minHeight):
            var height = preferredContentHeight
            if !safeAreaOptions.contains(.ignorePullbar) {
                height += config.pullBarConfig.height
            }

            if self.keyboardShown == false {
                switch display {
                case .fromBottom:
                    if !safeAreaOptions.contains(.ignoreBottom) {
                        height += containerView.safeAreaInsets.bottom
                    }

                case .fromTop:
                    if !safeAreaOptions.contains(.ignoreTop) {
                        height += containerView.safeAreaInsets.top
                    }
                }
            }
            presentedHeight = min(max(minHeight, height), maxHeight)

        case .maximum:
            presentedHeight = maxHeight

        case .custom(let height):
            presentedHeight = min(height, maxHeight)
        }

        self.presentedViewHeightConstraint?.constant = presentedHeight
        self.presentedViewHeightConstraint?.isActive = true
        containerView.layoutIfNeeded()
    }
}
