//
//  Created on 03.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

public protocol ViewPlaceholdable: ViewTextable {
    var placeholderDisplay: PlaceholderDisplay { get set }
    var uiPlaceholder: UIText { get set }
    var placeholderValue: PlaceholderValue? { get set }
    func setAttributedPlaceholder(_ attributed: NSAttributedString?)
}

extension ViewPlaceholdable {
    private var placeholderParagraphStyle: NSMutableParagraphStyle {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = self.textAlignment
        return paragraphStyle
    }

    private var placeholderAttridutes: [NSAttributedString.Key: Any] {[
        .foregroundColor: self.uiPlaceholder.color ?? .black,
        .font: self.uiPlaceholder.font ?? .systemFont(ofSize: 20),
        .paragraphStyle: self.placeholderParagraphStyle
    ]}

    private func makePlaceholderMask(
        _ mask: String
    ) -> NSAttributedString? {
        var mask = mask
        guard mask.isEmpty == false else { return .none }

        let text = mask
            .replacingOccurrences(of: "{", with: "")
            .replacingOccurrences(of: "}", with: "")

        let fullString = NSMutableAttributedString(
            string: text,
            attributes: self.placeholderAttridutes
        )

        while let startMaskIndex = mask.firstIndex(of: "{") {
            mask.remove(at: startMaskIndex)

            guard let endMaskIndex = mask[startMaskIndex..<mask.endIndex].firstIndex(of: "}")
            else { continue }
            mask.remove(at: endMaskIndex)

            let maskStartPosition = startMaskIndex.utf16Offset(in: mask)
            let maskLength = mask.distance(from: startMaskIndex, to: endMaskIndex)

            fullString.setAttributes(
                [
                    .font: self.uiText.font ?? .systemFont(ofSize: 20),
                    .foregroundColor: self.uiText.color ?? .black,
                    .paragraphStyle: self.placeholderParagraphStyle
                ],
                range: .init(location: maskStartPosition, length: maskLength)
            )
        }
        return fullString
    }

    public func updatePlaceholder() {
        var needUpdate = self.anyString?.isEmpty == true

        switch self.placeholderDisplay {
        case .whenTextIsEmpty:
            break

        case .whenTextIsEmptyAndNonFocus:
            needUpdate = needUpdate && self.isFirstResponder == false
        }

        guard needUpdate,
              let placeholder = self.placeholderValue
        else {
            self.setAttributedPlaceholder(.none)
            return
        }

        switch placeholder {
        case let .text(text):
            if let text {
                self.setAttributedPlaceholder(.init(
                    string: text,
                    attributes: self.placeholderAttridutes
                ))
            } else {
                self.setAttributedPlaceholder(.none)
            }

        case let .mask(mask):
            self.setAttributedPlaceholder(self.makePlaceholderMask(mask))
        }
    }
}
