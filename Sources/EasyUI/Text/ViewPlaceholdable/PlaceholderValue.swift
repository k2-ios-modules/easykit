//
//  Created on 03.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public enum PlaceholderValue {
    case text(String?)
    /// +7 {987}  654 4321
    case mask(String)

    public var textString: String? {
        switch self {
        case .text(let string):
            return string
        case .mask(let mask):
            return mask
        }
    }
}
