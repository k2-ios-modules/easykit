//
//  Created on 19.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public enum PlaceholderDisplay {
    case whenTextIsEmpty
    case whenTextIsEmptyAndNonFocus
}
