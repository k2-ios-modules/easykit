//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public protocol TextInputProtocol: AnyObject {
    var scrollContentAdditionalBottomInset: CGFloat { get }
}
