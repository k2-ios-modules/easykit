//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

@objc
public protocol TextInputDelegate: AnyObject {
    @objc optional func textDidBeginEditing(_ text: String)
    @objc optional func textDidEndEditing(_ text: String)
    @objc optional func textEditingChanged(_ text: String)
    @objc optional func text(
        _ text: String,
        shouldChangeCharactersIn range: NSRange,
        replacementString string: String
    ) -> Bool
}
