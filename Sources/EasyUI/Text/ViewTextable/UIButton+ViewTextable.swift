//
//  Created on 03.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyFoundation
import UIKit

extension UIButton: ViewTextable {
    public var uiText: UIText {
        get { (font: self.titleLabel?.font, color: self.titleColor(for: self.state)) }
        set {
            self.titleLabel?.font = newValue.font
            self.titleLabel?.textColor = newValue.color
            self.setTitleColor(newValue.color, for: .normal)
            self.setTitleColor(newValue.color, for: .highlighted)
            self.setTitleColor(newValue.color, for: .disabled)
            self.setTitleColor(newValue.color, for: .selected)
        }
    }

    public var textAlignment: NSTextAlignment {
        get { self.titleLabel?.textAlignment ?? .left }
        set { self.titleLabel?.textAlignment = newValue }
    }

    public var lineBreakMode: NSLineBreakMode { self.titleLabel?.lineBreakMode ?? .byTruncatingTail }

    public var textString: String? {
        get { self.titleLabel?.text }
        set {
            self.titleLabel?.text = newValue
            self.setTitle(newValue, for: .normal)
            self.setTitle(newValue, for: .highlighted)
            self.setTitle(newValue, for: .disabled)
            self.setTitle(newValue, for: .selected)
        }
    }

    public var attributedStringValue: NSAttributedString? {
        get { self.titleLabel?.attributedText }
        set {
            self.titleLabel?.attributedText = newValue
            self.setAttributedTitle(newValue, for: .normal)
            self.setAttributedTitle(newValue, for: .highlighted)
            self.setAttributedTitle(newValue, for: .disabled)
            self.setAttributedTitle(newValue, for: .selected)
        }
    }
}
