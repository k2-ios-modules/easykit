//
//  Created on 03.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyFoundation
import UIKit

extension UITextView: ViewTextable {
    public var uiText: UIText {
        get { (font: self.font, color: self.textColor) }
        set {
            self.font = newValue.font
            self.textColor = newValue.color
        }
    }

    @objc
    open var isEnabled: Bool {
        get { self.isEditable }
        set {
            self.isEditable = newValue
            self.isSelectable = newValue
        }
    }

    public var lineBreakMode: NSLineBreakMode { self.textContainer.lineBreakMode }

    public var textString: String? {
        get { self.text }
        set { self.text = newValue }
    }

    public var attributedStringValue: NSAttributedString? {
        get { self.attributedText }
        set { self.attributedText = newValue }
    }
}
