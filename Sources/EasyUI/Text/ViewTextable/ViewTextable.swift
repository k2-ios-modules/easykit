//
//  Created on 03.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import EasyFoundation
import UIKit

/// Описание сущности вью, проксирующей свойства текста и установки текста.
public protocol ViewTextable where Self: UIView {
    typealias UIText = (font: UIFont?, color: UIColor?)

    /// Визуальные параметры текста: `UIFont` и `UIColor`.
    var uiText: UIText { get set }

    /// Булическое значение, указывающее, находится ли элемент управления в включенном состоянии.
    var isEnabled: Bool { get set }

    /// Техника выравнивания текста.
    ///
    /// Применяется глобально для атрибутивного значения текста.
    var textAlignment: NSTextAlignment { get set }

    /// Техника переноса и усечения текста.
    ///
    /// Применяется глобально для атрибутивного значения текста.
    var lineBreakMode: NSLineBreakMode { get }
    
    /// Атрибуты абзаца или линейки для атрибутированной строки.
    ///
    /// При первом обращении `paragraphStyle` будет проициализирован с параметрами `textAlignment` и `lineBreakMode`.
    /// Применяется глобально для атрибутивного значения текста.
    var paragraphStyle: NSMutableParagraphStyle? { get set }
    
    /// Проксируюзее свойствой текствого значения строки.
    var textString: String? { get set }

    /// Проксирующее свойство атрибутивного значения строки.
    var attributedStringValue: NSAttributedString? { get set }
}

// MARK: - paragraphStyle

fileprivate extension UIView {
    static var paragraphStyleKey: UInt8 = 0
}

public extension ViewTextable {
    var paragraphStyle: NSMutableParagraphStyle? {
        get { objc_getAssociatedObject(self, &Self.paragraphStyleKey) as? NSMutableParagraphStyle }
        set {
            objc_setAssociatedObject(self, &Self.paragraphStyleKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
}

// MARK: - setters

public extension ViewTextable {

    func setTextValue(_ value: TextValue?) {
        switch value {
        case .text(let text):
            self.attributedStringValue = nil // сбросить атрибутивную строку
            self.textString = text // установить текст, атрибутивная строка будет проинициализирована

            self.applyParagraphStyle(for: text)

        case .attributed(let attributed):
            self.attributedStringValue = nil // сбросить атрибутивную строку
            self.textString = attributed?.string // установить текст, атрибутивная строка будет проинициализирована

            guard let attributed, let attributedString = self.attributedStringValue else { return }

            let mutable = NSMutableAttributedString(attributedString: attributedString)

            // применить общий paragraphStyle ко всей строке, если это необходимо
            if let paragraphStyle {
                mutable.addAttributes(
                    [
                        .paragraphStyle: paragraphStyle
                    ],
                    range: NSRange(location: 0, length: mutable.length)
                )
            }

            // применить атрибуты attributedString только к указанным участкам в mutable
            attributed.enumerateAttributes(
                in: .init(location: 0, length: attributed.length),
                using: { attributes, range, _ in
                    if attributes.isEmpty == false {
                        mutable.addAttributes(attributes, range: range)
                    }
                }
            )

            self.attributedStringValue = mutable

        case .none:
            self.attributedStringValue = nil
            self.textString = nil
        }
    }
    
    /// Применить общий `paragraphStyle` ко всей строке, если это необходимо.
    ///
    /// Общий `paragraphStyle` будет применет к строке, если какое-либо из свойств `NSMutableParagraphStyle` было изменено.
    /// - Parameter text: Целевое значение текста.
    func applyParagraphStyle(for text: String?) {
        guard let paragraphStyle,
              let attributedString = self.attributedStringValue
        else { return }

        paragraphStyle.alignment = self.textAlignment
        paragraphStyle.lineBreakMode = self.lineBreakMode

        let mutable = NSMutableAttributedString(attributedString: attributedString)
        mutable.addAttributes(
            [
                .paragraphStyle: paragraphStyle
            ],
            range: NSRange(location: 0, length: mutable.length)
        )

        self.attributedStringValue = mutable
    }
}

// MARK: - computed properties

public extension ViewTextable {

    /// Фактическая текствая строка в компоненте.
    var anyString: String? {
        self.attributedStringValue?.string ?? self.textString
    }
    
    /// Фактический размер текста.
    var textSize: CGSize {
        let font = self.uiText.font
        var attributes: [NSAttributedString.Key: Any] = [
            .font: font as Any
        ]
        if let paragraphStyle {
            attributes[.paragraphStyle] = paragraphStyle
        }
        return (self.anyString ?? "").size(withAttributes: attributes)
    }
    
    /// Фактическое количество строк текущего текста.
    var currentNumberOfLines: Int {
        guard let font = self.uiText.font else { return 0 }
        let maxSize = CGSize(
            width: self.frame.size.width,
            height: CGFloat(Float.infinity)
        )
        let charSize = font.lineHeight
        let text = (self.anyString ?? "") as NSString

        var attributes: [NSAttributedString.Key: Any] = [
            .font: font as Any
        ]
        if let paragraphStyle {
            attributes[.paragraphStyle] = paragraphStyle
        }

        let textSize = text.boundingRect(
            with: maxSize,
            options: .usesLineFragmentOrigin,
            attributes: attributes,
            context: nil
        )
        let linesRoundedUp = Int(ceil(textSize.height / charSize))
        return linesRoundedUp
    }
}
