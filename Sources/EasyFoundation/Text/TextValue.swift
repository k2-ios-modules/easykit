//
//  Created on 03.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public enum TextValue: ExpressibleByStringLiteral,
                       ExpressibleByNilLiteral {
    case text(String?)
    case attributed(NSAttributedString?)

    public var textString: String? {
        switch self {
        case .text(let string):
            return string
        case .attributed(let attributedString):
            return attributedString?.string
        }
    }

    public var isEmpty: Bool {
        self.textString?.isEmpty ?? true
    }

    public init(stringLiteral value: StringLiteralType) {
        self = .text(value)
    }

    public init(nilLiteral: ()) {
        self = .text(nil)
    }
}

// MARK: - Equatable

extension TextValue: Equatable {
    public static func == (lhs: Self, rhs: Self) -> Bool {
        switch (lhs, rhs) {
        case (.text(let lhs), .text(let rhs)):
            return lhs == rhs
        case (.attributed(let lhs), .attributed(let rhs)):
            return lhs == rhs
        default:
            return false
        }
    }
}
