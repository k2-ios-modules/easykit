//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

public extension UIViewController {
    var navigationBarSize: CGSize {
        if let tbc = self as? UITabBarController {
            return tbc.selectedViewController?.navigationBarSize ?? .zero
        } else if let nc = self as? UINavigationController {
            return nc.navigationBar.frame.size
        } else {
            return self.navigationController?.navigationBar.frame.size ?? .zero
        }
    }

    var isModal: Bool {
        if let nc = self.navigationController {
            if nc.viewControllers.first === self {
                return nc.presentingViewController?.presentedViewController === nc
            } else {
                return false
            }
        } else if self.presentingViewController != nil {
            return true
        } else if self.tabBarController?.presentingViewController is UITabBarController {
            return true
        } else {
            return false
        }
    }

    var isChild: Bool {
        guard let parent = self.parent else { return false }
        return (parent is UINavigationController) == false
    }

    var actualPreferredContentSize: CGSize {
        get { (self.navigationController ?? self).preferredContentSize }
        set { (self.navigationController ?? self).preferredContentSize = newValue }
    }

    func removeFromParentVC() {
        view.removeFromSuperview()
        didMove(toParent: nil)
        removeFromParent()
    }

    func removeChild(vc: UIViewController) {
        guard self.children.contains(vc) else { return }
        vc.removeFromParentVC()
    }

    func removeAllChild() {
        self.children.forEach(self.removeChild)
    }

    func addChild(
        _ vc: UIViewController,
        to containerView: UIView
    ) {
        guard containerView.isDescendant(of: view) else { return }

        self.addChild(vc)
        containerView.addSubview(vc.view)
        vc.didMove(toParent: self)
    }
}
