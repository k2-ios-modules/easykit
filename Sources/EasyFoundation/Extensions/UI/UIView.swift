//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

extension UIView {

    public var layoutSizeFitting: CGSize {
        self.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
    }

    public var firstResponder: UIView? {
        if self.isFirstResponder {
            return self
        }
        for view in self.subviews {
            if let firstResponder = view.firstResponder {
                return firstResponder
            }
        }
        return nil
    }

    @objc
    open var cornerRadius: CGFloat {
        get {
            self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            self.layer.masksToBounds = newValue > 0
        }
    }

    @discardableResult
    public func prepareForAutolayout() -> Self {
        self.translatesAutoresizingMaskIntoConstraints = false
        return self
    }

    public func addSubviews(_ views: [UIView]) {
        views.forEach(self.addSubview)
    }

    public func removeSubviews() {
        self.subviews.forEach({
            $0.removeFromSuperview()
        })
    }

    public func addGestureRecognizerIfNeeded(_ gestureRecognizer: UIGestureRecognizer) {
        guard (self.gestureRecognizers ?? []).contains(gestureRecognizer) == false else { return }
        self.addGestureRecognizer(gestureRecognizer)
    }

    public func removeConstraint(_ constraint: NSLayoutConstraint?) {
        guard let constraint else { return }
        constraint.isActive = false
        self.removeConstraint(constraint)
    }

    public func removeConstraints(_ constrainst: [NSLayoutConstraint?]) {
        constrainst.forEach({
            $0?.isActive = false
        })
        self.removeConstraints(constrainst.compactMap({ $0 }))
    }

    public func removeConstraint(_ attribute: NSLayoutConstraint.Attribute) {
        let constraints = self.constraints.filter({
            $0.firstAttribute == attribute || $0.secondAttribute == attribute
        })
        guard constraints.isEmpty == false else { return }

        self.removeConstraints(constraints)
    }
}
