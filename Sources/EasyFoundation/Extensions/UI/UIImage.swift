//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

public extension UIImage {

    enum Options {
        case backgroundColor(UIColor?)
        case backgroundLayer(CALayer)

        case tint(UIColor?)
        case tintGradient(CAGradientLayer)
        case alpha(CGFloat)

        case size(CGSize)
        public static func size(width: CGFloat, height: CGFloat) -> Self {
            .size(CGSize(width: width, height: height))
        }

        case insets(UIEdgeInsets)
        public static func insets(top: CGFloat = 0, left: CGFloat = 0, bottom: CGFloat = 0, right: CGFloat = 0) -> Self {
            .insets(UIEdgeInsets(top: top, left: left, bottom: bottom, right: right))
        }
        case centeringForTargetSize(CGSize)
        public static func centeringForTargetSize(width: CGFloat, height: CGFloat) -> Self {
            .centeringForTargetSize(CGSize(width: width, height: height))
        }

        case roundedCorners(radius: CGFloat, corners: UIRectCorner)
        case cicleCorners

        case rotate(angle: CGFloat)

        case rendered
    }

    func applyOptions(_ options: [Options]) -> UIImage {
        return options.reduce(into: self) { result, option in
            switch option {
            case let .backgroundColor(color):
                result = result.withBackgroundColor(color)
            case let .backgroundLayer(layer):
                result = result.withBackgroundLayer(layer)

            case let .tint(color):
                result = result.withTintColor(color)
            case let .tintGradient(layer):
                result = result.withTintGradient(layer)
            case let .alpha(alpha):
                result = result.withAlpha(alpha)

            case let .size(size):
                result = result.withSize(size)

            case let .insets(insets):
                result = result.withInsets(insets)
            case let .centeringForTargetSize(size):
                result = result.withCenteringForTargetSize(size)

            case let .roundedCorners(radius, corners):
                result = result.withRoundedCorners(radius: radius, corners: corners)
            case .cicleCorners:
                result = result.withCicleCorners()

            case let .rotate(angle):
                result = result.rotate(angle: angle)

            case .rendered:
                result = result.rendered()
            }
        }
    }

    // MARK: - helpers

    private var bounds: CGRect {
        CGRect(origin: .zero, size: self.size)
    }

    private func makeRenderer(size: CGSize) -> UIGraphicsImageRenderer {
        let format = UIGraphicsImageRendererFormat()
        format.scale = self.scale
        return UIGraphicsImageRenderer(size: size, format: format)
    }

    // MARK: - background

    /// Возвращает новую версию текущего изображения с указанным цветом фона.
    /// https://stackoverflow.com/questions/28299886/how-to-set-a-background-color-in-uiimage-in-swift-programming
    /// - Parameter color: Цвет фона `UIColor`.
    /// - Returns: Новая версия изображения, которая включает в себя указанный цвет фона.
    func withBackgroundColor(_ color: UIColor?) -> UIImage {
        guard let color else { return self }

        let bounds = self.bounds
        return self.makeRenderer(size: self.size).image { rendererContext in
            let cgContext = rendererContext.cgContext
            cgContext.setFillColor(color.cgColor)
            cgContext.fill(bounds)
            self.draw(in: bounds)
            cgContext.saveGState()
        }
    }

    /// Возвращает новую версию текущего изображения с указанным цветом фона.
    /// - Parameter layer: Цвет фона `CALayer`.
    /// - Returns: Новая версия изображения, которая включает в себя указанный цвет фона.
    func withBackgroundLayer(_ layer: CALayer) -> UIImage {
        let bounds = self.bounds
        layer.bounds = bounds
        return self.makeRenderer(size: self.size).image { rendererContext in
            let cgContext = rendererContext.cgContext
            layer.render(in: cgContext)
            self.draw(in: bounds)
            cgContext.saveGState()
        }
    }

    // MARK: - color
    
    /// Возвращает новую версию текущего изображения с указанным цветом оттенка.
    /// - Parameter color: Цвет оттенка.
    /// - Returns: Новая версия изображения, которая включает в себя указанный оттенок цвета.
    func withTintColor(_ color: UIColor?) -> UIImage {
        guard let color else { return self }
        return self.withTintColor(color)
    }

    /// Возвращает новую версию текущего изображения с указанным цветом градиента.
    /// - Parameter gradientLayer: Слой градиента `CAGradientLayer`.
    /// - Returns: Новая версия изображения, которая включает в себя указанный градиент цвета.
    func withTintGradient(
        _ gradientLayer: CAGradientLayer
    ) -> UIImage {
        let colors = (gradientLayer.colors ?? []) as CFArray
        let locations = (gradientLayer.locations ?? []).map({ CGFloat($0.doubleValue) })
        let space = CGColorSpaceCreateDeviceRGB()
        let gradient = CGGradient(colorsSpace: space, colors: colors, locations: locations)

        guard let cgImage, let gradient else { return self }

        let rect = CGRect(origin: .zero, size: self.size)

        return self.makeRenderer(size: self.size).image { rendererContext in
            let cgContext = rendererContext.cgContext
            cgContext.translateBy(x: 0, y: self.size.height)
            cgContext.scaleBy(x: 1, y: -1)
            cgContext.setBlendMode(.normal)

            var startPoint = gradientLayer.startPoint
            var endPoint = gradientLayer.endPoint
            startPoint.multiply(by: self.size)
            endPoint.multiply(by: self.size)

            cgContext.clip(to: rect, mask: cgImage)
            cgContext.drawLinearGradient(
                gradient,
                start: startPoint,
                end: endPoint,
                options: .drawsAfterEndLocation
            )

            cgContext.saveGState()
        }
    }

    /// Возвращает новую версию текущего изображения с указанной прозрачностью оттенка.
    /// - Parameter alpha: Значение прозрачности.
    /// - Returns: Новая версия изображения, которая включает в себя указанную прозрачность оттенка.
    func withAlpha(_ alpha: CGFloat) -> UIImage {
        let bounds = self.bounds
        return self.makeRenderer(size: self.size).image { rendererContext in
            let cgContext = rendererContext.cgContext
            self.draw(in: bounds, blendMode: .screen, alpha: alpha)
            cgContext.saveGState()
        }
    }

    // MARK: - size
    
    /// Возвращает новую версию текущего изображения с указанным размером.
    /// - Parameter size: Размер `UIImage`.
    /// - Returns: Новая версия изображения, которая включает в себя указанный размер.
    func withSize(_ size: CGSize) -> UIImage {
        let bounds = CGRect(origin: .zero, size: size)
        return self.makeRenderer(size: size).image { rendererContext in
            let cgContext = rendererContext.cgContext
            self.draw(in: bounds)
            cgContext.saveGState()
        }
    }

    /// Возвращает новую версию текущего изображения с указанным размером.
    /// - Returns: Новая версия изображения, которая включает в себя указанный размер.
    /// - Parameters:
    ///   - width: Значение требуемой ширины.
    ///   - height: Значение требуемой высоты.
    func withSize(width: CGFloat, height: CGFloat) -> UIImage {
        self.withSize(CGSize(width: width, height: height))
    }

    // MARK: - insets

    /// Возвращает новую версию текущего изображения с указанными внешними отступами.
    /// - Parameter insets: Значение внешних отступов.
    /// - Returns: Новая версия изображения, которая включает в себя указанные внешние отступы.
    func withInsets(_ insets: UIEdgeInsets) -> UIImage {
        let origin = CGPoint(x: insets.left, y: insets.top)
        let sizeWithInsets = CGSize(
            width: self.size.width + insets.left + insets.right,
            height: self.size.height + insets.top + insets.bottom
        )
        return self.makeRenderer(size: sizeWithInsets).image { rendererContext in
            let cgContext = rendererContext.cgContext
            self.draw(at: origin)
            cgContext.saveGState()
        }
    }

    /// Возвращает новую версию текущего изображения с указанными внешними отступами.
    /// - Parameters:
    ///   - top: Значение внешнего отступа сверху.
    ///   - left: Значение внешнего отступа слева.
    ///   - bottom: Значение внешнего отступа справа.
    ///   - right: Значение внешнего отступа снизу.
    /// - Returns: Новая версия изображения, которая включает в себя указанные внешние отступы.
    func withInsets(
        top: CGFloat = 0,
        left: CGFloat = 0,
        bottom: CGFloat = 0,
        right: CGFloat = 0
    ) -> UIImage {
        let insets = UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
        return insets != .zero ? self.withInsets(insets) : self
    }

    /// Возвращает новую версию текущего изображения с указанными целевым размером, размещенным по центру целевого размера.
    /// - Parameter targetSize: Целевой размер.
    /// - Returns: Новая версия изображения, которая включает в себя указанный целевой размер, размещенным по центру целевого размера.
    func withCenteringForTargetSize(_ targetSize: CGSize) -> UIImage {
        let verticalInset: CGFloat = max(
            (targetSize.height - self.size.height) / 2.0,
            0
        )
        let horizontalInset: CGFloat = max(
            (targetSize.width - self.size.width) / 2.0,
            0
        )
        return self.withInsets(UIEdgeInsets(
            top: verticalInset,
            left: horizontalInset,
            bottom: verticalInset,
            right: horizontalInset
        ))
    }

    /// Возвращает новую версию текущего изображения с указанными целевым размером, размещенным по центру целевого размера.
    /// - Parameters:
    ///   - width: Целевая ширина.
    ///   - height: Целевая высота.
    /// - Returns: Новая версия изображения, которая включает в себя указанный целевой размер, размещенным по центру целевого размера.
    func withCenteringForTargetSize(width: CGFloat, height: CGFloat) -> UIImage {
        self.withCenteringForTargetSize(CGSize(width: width, height: height))
    }

    // MARK: - corners
    
    /// Возвращает новую версию текущего изображения с указанным радиусом и углами закругления.
    /// - Parameters:
    ///   - radius: Радиус закругления
    ///   - corners: Углы закругления
    /// - Returns: Новая версия изображения, которая включает в себя указанный радиус и углы загругления.
    func withRoundedCorners(
        radius: CGFloat,
        corners: UIRectCorner
    ) -> UIImage {
        let bounds = self.bounds
        return self.makeRenderer(size: self.size).image { rendererContext in
            let path = UIBezierPath(
                roundedRect: bounds,
                byRoundingCorners: corners,
                cornerRadii: CGSize(width: radius, height: radius)
            )
            path.close()

            let cgContext = rendererContext.cgContext
            cgContext.saveGState()
            path.addClip()
            self.draw(in: bounds)
            cgContext.saveGState()
        }
    }
    
    /// Возвращает новую версию текущего изображения с закругленными углами.
    /// - Returns: Новая версия изображения, которая включает в себя закругленные углы.
    func withCicleCorners() -> UIImage {
        let radius = self.size.height / 2
        return self.withRoundedCorners(radius: radius, corners: .allCorners)
    }

    // MARK: - rotate
    
    /// Возвращает новую версию текущего изображения с указанным угол (в градусах) поворота.
    /// - Parameter angle: Угол поворота.
    /// - Returns: Новая версия изображения, которая включает в себя указанный угол поворота.
    func rotate(angle: CGFloat) -> UIImage {
        var newSize = self.bounds
            .applying(CGAffineTransform(rotationAngle: angle.toRadians()))
            .size

        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)

        return self.makeRenderer(size: newSize).image { rendererContext in
            let cgContext = rendererContext.cgContext
            cgContext.saveGState()
            cgContext.translateBy(x: newSize.width / 2.0, y: newSize.height / 2.0)
            cgContext.rotate(by: angle.toRadians())
            self.draw(in: CGRect(
                origin: CGPoint(x: -self.size.width / 2.0, y: -self.size.height / 2.0),
                size: self.size
            ))
            cgContext.saveGState()
        }
    }

    func rendered() -> UIImage {
        guard let cgImage = self.cgImage else {
            return self
        }

        let width = cgImage.width
        let height = cgImage.height
        let rect = CGRect(origin: .zero, size: CGSize(width: width, height: height))

        let space = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo: UInt32 =
        CGImageAlphaInfo.premultipliedFirst.rawValue |
        CGBitmapInfo.byteOrder32Little.rawValue

        guard let imageContext = CGContext(
            data: nil,
            width: width,
            height: height,
            bitsPerComponent: 8,
            bytesPerRow: width * 4,
            space: space,
            bitmapInfo: bitmapInfo
        ) else {
            return self
        }

        imageContext.draw(cgImage, in: rect)

        guard let outputImage = imageContext.makeImage() else {
            return self
        }

        return UIImage(cgImage: outputImage)
    }
}

// MARK: - private

fileprivate extension CGFloat {
    func toRadians() -> CGFloat {
        return self * (.pi / 180)
    }
}

fileprivate extension CGPoint {
    mutating func multiply(by size: CGSize) {
        self.x *= size.width
        self.y *= size.height
    }
}
