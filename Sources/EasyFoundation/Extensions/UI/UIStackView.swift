//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

public extension UIStackView {

    // MARK: - subtypes

    typealias ViewSpacing = (UIView, spacing: CGFloat)

    // MARK: - add methods

    func addArrangedSubview(_ config: ViewSpacing) {
        self.addArrangedSubview(config.0)
        self.setCustomSpacing(config.spacing, after: config.0)
    }

    func addArrangedSubviews(
        _ views: [UIView],
        applyWidthFitting: Bool = false
    ) {
        views.forEach(self.addArrangedSubview)
        if applyWidthFitting {
            self.applyWidthFitting()
        }
    }

    func addArrangedSubviews(
        _ configs: [ViewSpacing],
        applyWidthFitting: Bool = false
    ) {
        configs.forEach(self.addArrangedSubview)
        if applyWidthFitting {
            self.applyWidthFitting()
        }
    }

    // MARK: - insert methods

    func insertArrangedSubview(
        _ view: UIView,
        at index: Int,
        applyWidthFitting: Bool
    ) {
        self.insertArrangedSubview(view, at: index)
        if applyWidthFitting {
            self.applyWidthFitting()
        }
    }

    func insertArrangedSubview(
        _ config: ViewSpacing,
        at index: Int,
        applyWidthFitting: Bool = false
    ) {
        self.insertArrangedSubview(config.0, at: index)
        self.setCustomSpacing(config.spacing, after: config.0)
        if applyWidthFitting {
            self.applyWidthFitting()
        }
    }

    // MARK: - remove methods

    func clearArrangedSubviews() {
        self.arrangedSubviews.forEach(self.completeRemoveArrangedSubview)
    }

    func completeRemoveArrangedSubview(_ view: UIView) {
        self.removeArrangedSubview(view)
        view.removeFromSuperview()
    }

    func removeArrangedSubview(at index: Int) {
        let view = self.arrangedSubviews[index]
        self.completeRemoveArrangedSubview(view)
    }

    // MARK: - utils

    @discardableResult
    func applyHorizontalCenter() -> Self {
        self.axis = .horizontal
        self.alignment = .center
        self.distribution = .fill
        return self
    }

    func applyWidthFitting() {
        let visibleItems = self.arrangedSubviews.filter({ $0.isHidden == false })
        let width = visibleItems.reduce(0, {
            $0 + $1.layoutSizeFitting.width + min(self.customSpacing(after: $1), 0.0)
        })

        self.isHidden = width <= 0

        self.removeConstraint(.width)
        NSLayoutConstraint.activate([
            self.widthAnchor.constraint(equalToConstant: max(width, 0))
        ])
    }
}
