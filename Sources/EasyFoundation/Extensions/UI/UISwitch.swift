//
//  Created on 08.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

extension UISwitch {
    public func setSize(_ size: CGSize) {
        let oridinalSize = CGSize(width: 51, height: 31)
        self.transform = .init(
            scaleX: size.width / oridinalSize.width,
            y: size.height / oridinalSize.height
        )

        self.removeConstraint(.width)
        self.removeConstraint(.height)

        NSLayoutConstraint.activate([
            self.widthAnchor.constraint(equalToConstant: size.width),
            self.heightAnchor.constraint(equalToConstant: size.height)
        ])
    }
}
