//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

public extension UIBarButtonItem {
    var button: UIButton? {
        self.customView as? UIButton
    }

    func setTarget(_ target: AnyObject?, action: Selector) {
        if let button = self.button {
            button.removeTarget(nil, action: nil, for: .allEvents)
            button.addTarget(target, action: action, for: .touchUpInside)
        } else {
            self.target = target
            self.action = action
        }
    }
}
