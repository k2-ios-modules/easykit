//
//  Created on 17.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

public extension UICollectionView {

    // MARK: - Cell

    func register<T: UICollectionViewCell>(_ type: T.Type) {
        self.register(type, forCellWithReuseIdentifier: type.className)
    }

    func dequeueReusableCell<T: UICollectionViewCell>(_ type: T.Type, for indexPath: IndexPath) -> T? {
        self.dequeueReusableCell(withReuseIdentifier: type.className, for: indexPath) as? T
    }
}
