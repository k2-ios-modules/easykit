//
//  Created on 08.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//

import UIKit

extension UIGestureRecognizer {
    public func setTarget(_ target: Any, action selector: Selector) {
        self.removeTarget(target, action: selector)
        self.addTarget(target, action: selector)
    }
}
