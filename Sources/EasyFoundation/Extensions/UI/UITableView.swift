//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

public extension UITableView {

    // MARK: - TableHeaderView

    func layoutTableHeaderIfNeeded() {
        guard let view = self.tableHeaderView else { return }

        view.updateConstraintsIfNeeded()

        let height = view
            .systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
            .height

        if view.frame.height != height {
           view.frame.size.height = height
           self.tableHeaderView = view
        }
    }

    // MARK: - Cell

    func register<T: UITableViewCell>(_ type: T.Type) {
        self.register(T.self, forCellReuseIdentifier: type.className)
    }

    func dequeueReusableCell<T: UITableViewCell>(_ type: T.Type) -> T? {
        self.dequeueReusableCell(withIdentifier: type.className) as? T
    }

    func dequeueReusableCell<T: UITableViewCell>(_ type: T.Type, for indexPath: IndexPath) -> T? {
        self.dequeueReusableCell(withIdentifier: type.className, for: indexPath) as? T
    }

    // MARK: - HeaderFooter

    func register<T: UITableViewHeaderFooterView>(_ type: T.Type) {
        self.register(T.self, forHeaderFooterViewReuseIdentifier: type.className)
    }

    func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>(_ type: T.Type) -> T? {
        self.dequeueReusableHeaderFooterView(withIdentifier: type.className) as? T
    }
}
