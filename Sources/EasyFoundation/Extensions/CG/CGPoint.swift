//
//  Created on 15.02.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import QuartzCore

public extension CGPoint {
    static func + (lhs: CGPoint, rhs: CGPoint) -> CGPoint {
        .init(
            x: lhs.x + rhs.x,
            y: lhs.y + rhs.y
        )
    }

    static func - (lhs: CGPoint, rhs: CGPoint) -> CGPoint {
        .init(
            x: lhs.x - rhs.x,
            y: lhs.y - rhs.y
        )
    }
}
