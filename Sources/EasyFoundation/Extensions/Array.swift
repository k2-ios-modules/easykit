//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public extension Array {
    subscript(safe index: Index) -> Element? {
        self.indices.contains(index) ? self[index] : nil
    }
}

// MARK: - Equatable

public extension Array where Element: Equatable {
    mutating func removeFirst(where predicate: (Element) throws -> Bool) rethrows {
        guard let index = try self.firstIndex(where: predicate) else { return }
        self.remove(at: index)
    }

    mutating func remove(element: Element) {
        guard let index = self.firstIndex(of: element) else { return }
        self.remove(at: index)
    }
}

// MARK: - Identifiable

public extension Array where Element: Identifiable {
    mutating func removeById(_ id: Element.ID) {
        guard let index = self.firstIndex(where: { $0.id == id }) else { return }
        self.remove(at: index)
    }
}
