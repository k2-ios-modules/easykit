//
//  Created on 16.03.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public extension String {
    subscript(bounds: CountableClosedRange<Int>) -> String {
        let start = self.index(startIndex, offsetBy: bounds.lowerBound)
        let end = self.index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start...end])
    }

    subscript(bounds: CountableRange<Int>) -> String {
        let start = self.index(startIndex, offsetBy: bounds.lowerBound)
        let end = self.index(startIndex, offsetBy: bounds.upperBound)
        return String(self[start..<end])
    }

    subscript(offset: Int) -> String {
        String(self[index(self.startIndex, offsetBy: offset)])
    }
}
