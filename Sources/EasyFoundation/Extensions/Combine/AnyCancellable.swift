//
//  Created on 10.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Combine
import Foundation

extension AnyCancellable {
    private static var identifierKey: UInt8 = 0

    public var identifier: String? {
        get {
            objc_getAssociatedObject(self, &Self.identifierKey) as? String
        }
        set {
            objc_setAssociatedObject(self, &Self.identifierKey, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    public func setIdentifier(_ id: String?) -> Self {
        self.identifier = id
        return self
    }
}

// MARK: - Set

extension Set where Element == AnyCancellable {
    public mutating func removeByIdentifier(_ id: String) {
        guard let index = self.firstIndex(where: { $0.identifier == id })
        else { return }
        self.remove(at: index)
    }
}
