//
//  Created on 31.01.2025.
//  Copyright © 2025 Коба Самхарадзе. All rights reserved.
//  

import Foundation
import Combine

extension Publisher where Self.Failure == Never {
    public func sinkOnMain(receiveValue: @escaping ((Self.Output) -> Void)) -> AnyCancellable {
        self.sink(receiveValue: { output in
            if Thread.isMainThread {
                receiveValue(output)
            } else {
                Task { @MainActor in
                    receiveValue(output)
                }
            }
        })
    }
}
