//
//  Created on 08.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

public extension NSLayoutConstraint {
    static func activate(_ constraints: [NSLayoutConstraint?]) {
        self.activate(constraints.compactMap({ $0 }))
    }

    static func deactivate(_ constraints: [NSLayoutConstraint?]) {
        self.deactivate(constraints.compactMap({ $0 }))
    }
}
