//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

extension NSObject {
    static var className: String {
        String(describing: self)
    }
}
