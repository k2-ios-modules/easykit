//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

public extension String {
    typealias HTMLTag = String
    typealias HTMLStyle = [HTMLTag: [CSSProperty]]

    var htmlAttributedString: NSAttributedString? {
        guard let data = self.data(using: .utf8) else { return nil }
        let options: [NSAttributedString.DocumentReadingOptionKey: Any] = [
            .documentType: NSAttributedString.DocumentType.html,
            .characterEncoding: String.Encoding.utf8.rawValue
        ]
        do {
            return try NSAttributedString(
                data: data,
                options: options,
                documentAttributes: nil
            )
        } catch {
            return nil
        }
    }

    func embedToHTML(style: HTMLStyle) -> Self {
        let styleValue = style
            .map({
                let cssProperties = $0.value
                    .map(\.cssProperty)
                    .joined(separator: "\n")
                return "\($0.key) { \(cssProperties) }"
            })
            .joined(separator: "\n")
        return """
    <html>
        <head>
            <style type=\"text/css\">
                \(styleValue)
            </style>
        </head>
        <body>
        \(self)
        </body>
    </html>
    """
    }
}
