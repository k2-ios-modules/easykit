//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

extension UIColor: CSSProperty {
    /// exapmple: `UIColor.black.cssValue -> "rgb(0 0 0)"`
    public var cssValue: String {
        let components = [
            CIColor(color: self).red,
            CIColor(color: self).green,
            CIColor(color: self).blue
        ].map({ "\($0 * 255)" })
        return "rgb(\(components.joined(separator: ", ")))"
    }

    /// `color: rgb(0 0 0);`
    public var cssProperty: String {
        "color: \(self.cssValue);"
    }
}
