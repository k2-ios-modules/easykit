//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

extension NSTextAlignment: CSSProperty {
    /// exapmple: `NSTextAlignment.left.cssValue -> "text-align: left;"`
    public var cssProperty: String {
        let key: String = "text-align"
        let value: String

        switch self {
        case .left: value = "left"
        case .center: value = "center"
        case .right: value = "right"
        case .justified: value = "justify"
        case .natural: value = "inherit"
        @unknown default:
            assertionFailure()
            value = ""
        }

        return "\(key): \(value);"
    }
}
