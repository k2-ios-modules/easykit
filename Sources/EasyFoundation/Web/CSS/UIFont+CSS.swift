//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

extension UIFont: CSSProperty {
    public var cssProperty: String {
        guard let url = Bundle.main.url(forResource: self.fontName,
                                        withExtension: "ttf"),
              let data = try? Data(contentsOf: url)
        else { return "" }

        var fontAttrs: [String] = [
            self.cssFamily,
            "font-size: \(self.pointSize);"
        ]

        let cssWeight = self.cssWeight
        if cssWeight > 0 {
            fontAttrs.append("font-weight: \(cssWeight);")
        }

        return """
    @font-face {
       \(self.cssFamily)
        src: local('\(self.familyName)'), url(data: font/truetype;charset=utf-8;base64,\(data.base64EncodedString(options: []))) format('truetype');
    }
    \(fontAttrs.joined(separator: "\n"))
    """
    }

    public var cssFamily: String {
        "font-family: '\(self.familyName)';"
    }

    public var cssWeight: Int {
        let weight = self.weight
        let weights: [UIFont.Weight: Int] = [
            .ultraLight: 100,
            .thin: 200,
            .light: 300,
            .regular: 400,
            .medium: 500,
            .semibold: 600,
            .bold: 700,
            .heavy: 800,
            .black: 900
        ]

        return weights.min(by: {
            abs(weight.rawValue - $0.key.rawValue) < abs(weight.rawValue - $1.key.rawValue)
        })?.value ?? 0
    }
}

// https://stackoverflow.com/questions/48686358/how-can-i-get-weight-of-a-uilabel
extension UIFont {
    private var traits: [UIFontDescriptor.TraitKey: Any] {
        return self.fontDescriptor.object(forKey: .traits) as? [UIFontDescriptor.TraitKey: Any]
        ?? [:]
    }

    public var weight: UIFont.Weight {
        guard let weightNumber = self.traits[.weight] as? NSNumber else { return .regular }
        let weightRawValue = CGFloat(weightNumber.doubleValue)
        let weight = UIFont.Weight(rawValue: weightRawValue)
        return weight
    }
}
