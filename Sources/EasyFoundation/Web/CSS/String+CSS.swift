//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import Foundation

extension String: CSSProperty {
    public var cssProperty: String { self }
}
