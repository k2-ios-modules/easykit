//
//  Created on 02.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

public protocol GenericContentViewable: AnyObject {
    associatedtype MainView: UIView

    var mainView: MainView { get }

    var contentView: UIView { get }
}

public extension GenericContentViewable {
    func setupLayout() {
        self.mainView.prepareForAutolayout()

        self.setInsets(.zero)
    }

    func setInsets(top: CGFloat = 0, left: CGFloat = 0, bottom: CGFloat = 0, right: CGFloat = 0) {
        self.setInsets(UIEdgeInsets(top: top, left: left, bottom: bottom, right: right))
    }

    func setInsets(_ insets: UIEdgeInsets) {
        self.contentView.removeSubviews()
        self.contentView.addSubview(self.mainView)

        NSLayoutConstraint.activate([
            self.mainView.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: insets.left),
            self.mainView.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: insets.top),
            self.mainView.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: -insets.right),
            self.mainView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -insets.bottom)
        ])
    }
}
