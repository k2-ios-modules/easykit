//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

public final class TapGestureRecognizer: UITapGestureRecognizer {
    public typealias Completion = () -> Void

    public var completion: Completion?

    public init(completion: Completion? = nil) {
        self.completion = completion
        super.init(target: nil, action: nil)
        self.addTarget(self, action: #selector(self.tapAction))
    }

    @objc
    private func tapAction() {
        self.completion?()
    }
}
