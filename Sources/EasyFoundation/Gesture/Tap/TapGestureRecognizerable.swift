//
//  Created on 04.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

public protocol TapGestureRecognizerable where Self: UIView {
    var tapGestureRecognizer: TapGestureRecognizer { get }
    var tappableView: UIView { get }
}

public extension TapGestureRecognizerable {
    var tappableView: UIView { self }

    func setTapCompletion(
        _ completion: (() -> Void)?,
        checkUserInteraction: Bool = false
    ) {
        self.tappableView.addGestureRecognizerIfNeeded(self.tapGestureRecognizer)
        self.tapGestureRecognizer.completion = completion
        self.tapGestureRecognizer.isEnabled = completion != nil

        if checkUserInteraction {
            self.isUserInteractionEnabled = completion != nil
        }
    }
}
