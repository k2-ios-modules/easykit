//
//  Created on 11.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

extension CALayer {
    private static var idKey: UInt8 = 0

    public var id: String? {
        objc_getAssociatedObject(self, &Self.idKey) as? String
    }

    @discardableResult
    public func setId(_ id: String) -> Self {
        objc_setAssociatedObject(self, &Self.idKey, id, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        return self
    }

    /// insert layer to back with background color
    @discardableResult
    public func insertSublayerToBack(_ color: UIColor) -> Self {
        let layer = CALayer()
        layer.backgroundColor = color.cgColor
        return self.insertSublayerToBack(layer)
    }

    @discardableResult
    public func insertSublayerToBack(_ layer: CALayer) -> Self {
        self.insertSublayer(layer, at: 0)
        return self
    }
}

// MARK: - UIView

public extension UIView {
    func removeIdentifierLayers() {
        self.layer.sublayers?.forEach({
            if $0.id != nil {
                $0.removeFromSuperlayer()
            }
        })
    }

    func removeIdentifierLayer(_ layer: CALayer) {
        self.layer.sublayers?
            .first(where: { .some($0.id) == .some(layer.id) })?
            .removeFromSuperlayer()
    }

    func setIdentifierLayer(_ layer: CALayer, rect: CGRect? = nil) {
        let frame = rect ?? self.bounds
        layer.frame = frame
        layer.sublayers?.forEach({ $0.frame = frame })
        self.removeIdentifierLayer(layer)
        self.layer.insertSublayer(layer, at: 0)
    }
}
