//
//  Created on 11.01.2024.
//  Copyright © 2024 Коба Самхарадзе. All rights reserved.
//  

import UIKit

public final class GradientLayer: CAGradientLayer {
    public init(id: String? = nil, angle: CGFloat, colors: [Color]) {
        super.init(layer: CAGradientLayer())
        if let id {
            self.setId(id)
        }

        self.type = .axial
        self.colors = colors.map(\.color.cgColor)
        self.locations = colors.map({ NSNumber(value: $0.location) })
        self.calculatePoints(for: angle)
    }

    public override init(layer: Any) {
        super.init(layer: layer)
    }

    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /// https://stackoverflow.com/questions/37038055/angled-gradient-layer
    /// Sets the start and end points on a gradient layer for a given angle.
    ///
    /// - Important:
    /// *0°* is a horizontal gradient from left to right.
    ///
    /// With a positive input, the rotational direction is clockwise.
    ///
    ///    * An input of *400°* will have the same output as an input of *40°*
    ///
    /// With a negative input, the rotational direction is clockwise.
    ///
    ///    * An input of *-15°* will have the same output as *345°*
    ///
    /// - Parameters:
    ///     - angle: The angle of the gradient.
    ///
    private func calculatePoints(for angle: CGFloat) {
        var ang = (-angle).truncatingRemainder(dividingBy: 360) + 90
        if ang < 0 { ang = 360 + ang }
        let n: CGFloat = 0.5
        switch ang {
        case 0...45, 315...360:
            let a = CGPoint(x: 0, y: n * tanx(ang) + n)
            let b = CGPoint(x: 1, y: n * tanx(-ang) + n)
            startPoint = a
            endPoint = b

        case 45...135:
            let a = CGPoint(x: n * tanx(ang - 90) + n, y: 1)
            let b = CGPoint(x: n * tanx(-ang - 90) + n, y: 0)
            startPoint = a
            endPoint = b

        case 135...225:
            let a = CGPoint(x: 1, y: n * tanx(-ang) + n)
            let b = CGPoint(x: 0, y: n * tanx(ang) + n)
            startPoint = a
            endPoint = b

        case 225...315:
            let a = CGPoint(x: n * tanx(-ang - 90) + n, y: 0)
            let b = CGPoint(x: n * tanx(ang - 90) + n, y: 1)
            startPoint = a
            endPoint = b

        default:
            let a = CGPoint(x: 0, y: n)
            let b = CGPoint(x: 1, y: n)
            startPoint = a
            endPoint = b
        }
    }

    /// Private function to aid with the math when calculating the gradient angle
    private func tanx(_ ang: CGFloat) -> CGFloat {
        return tan(ang * CGFloat.pi / 180)
    }
}

extension GradientLayer {
    public struct Color {
        public let color: UIColor
        public let location: Double

        public init(
            color: UIColor,
            location: Double
        ) {
            self.color = color
            self.location = location
        }
    }
}
